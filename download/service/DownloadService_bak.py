import hashlib
import json
import logging
from PyQt5.QtCore import QObject, pyqtSignal
import sys
import shutil
import ClientTools
import requests
import os
import time
import download.repository.DownloadDao
from network import HttpClient
__author__ = 'victor.choi'
_LOG_ = logging.getLogger()
NOT_INTERNET_ = {'statusCode': -1, 'statusMessage': 'Not Internet'}
default_encoding = 'utf-8'

class DownloadSignalHandler(QObject):
    ad_download_result_signal = pyqtSignal(str)
    delete_result_signal = pyqtSignal(str)


download_signal_handler = DownloadSignalHandler()
download_info = ''
dst_dir = ''

def start_delete_source(message_result):
    ClientTools.get_global_pool().apply_async(delete_source, (message_result,))


def delete_source(message_result):
    try:
        if message_result['taskType'] == 'DELETE_ADVERT':
            if not os.path.exists(sys.path[0] + '/advertisement/source'):
                delete_post_message(message_result['id'], 'path is not exist', 'ERROR')
            path_dir = os.path.join(os.getcwd(), 'advertisement', 'source')
            position = str(message_result['advert']['position'])
            for file_name in os.listdir(path_dir):
                if os.path.isfile(os.path.join(path_dir, file_name)) is True:
                    _file_name = file_name.split('.')[0]
                    if _file_name == position:
                        os.remove(os.path.join(path_dir, file_name))
                        download_signal_handler.delete_result_signal.emit('ok')
                        delete_post_message(message_result['id'], 'delete source finish', 'SUCCESS')
                    else:
                        delete_post_message(message_result['id'], 'the position is not exist', 'ERROR')
                        continue

        if message_result['taskType'] == 'system':
            if not os.path.exists(os.path.join(os.path.abspath(os.path.join(os.path.dirname('PakpoboxClient.py'), os.path.pardir)), 'system', 'source')):
                delete_post_message(message_result['id'], 'path is not exist', 'ERROR')
            path_dir = os.path.join(os.path.abspath(os.path.join(os.path.dirname('PakpoboxClient.py'), os.path.pardir)), 'system', 'source')
        if message_result['taskType'] == 'music':
            if not os.path.exists(sys.path[0] + '/music/source'):
                delete_post_message(message_result['id'], 'path is not exist', 'ERROR')
            path_dir = os.path.join(os.getcwd(), 'music', 'source')
    except Exception as e:
        _LOG_.error(('pre_download_source ERROR :', e))
        delete_post_message(message_result['id'], 'delete_source except error', 'ERROR')


def start_download_source(message_result):
    ClientTools.get_global_pool().apply_async(pre_download_source, (message_result,))


def pre_download_source(message_result):
    global download_info
    try:
        download_info = message_result
        flag_time = ClientTools.now()
        param = {'id': message_result['advert']['doc']['id'], 'url': message_result['advert']['doc']['id'], 'filename': message_result['advert']['doc']['displayName'],
         'status': 'Not downloaded', 'type': message_result['taskType'],
         'MD5': message_result['advert']['doc']['md5'], 'flagTime': flag_time,
         'position': message_result['advert']['position']
         }
        check_result = download.repository.DownloadDao.check_download_by_id({'id': message_result['advert']['doc']['id']})
        if len(check_result) == 0:
            download.repository.DownloadDao.insert_download_info(param)
        else:
            flag_time = check_result[0]['flagTime']
        download_file_patch = {'id': message_result['advert']['doc']['id'], 'url': message_result['advert']['doc']['id'], 'filename': message_result['advert']['doc']['displayName'],
         'type': message_result['taskType'],
         'flagTime': flag_time, 'position': message_result['advert']['position']
         }
        download_mk_dir(download_file_patch)
    except Exception as e:
        _LOG_.error(('pre_download_source ERROR :', e))
        download_post_message(download_info['id'], 'update databases error', 'ERROR')


def download_mk_dir(download_file_patch):
    global dst_dir
    try:
        if download_file_patch['type'] == 'UPDATE_ADVERT':
            if not os.path.exists(sys.path[0] + '/advertisement/source_temp'):
                os.makedirs(sys.path[0] + '/advertisement/source_temp')
            if not os.path.exists(sys.path[0] + '/advertisement/source'):
                os.makedirs(sys.path[0] + '/advertisement/source')
            dst_dir = os.path.join(os.getcwd(), 'advertisement', 'source_temp')
        if download_file_patch['type'] == 'system':
            if not os.path.exists(os.path.join(os.path.abspath(os.path.join(os.path.dirname('PakpoboxClient.py'), os.path.pardir)), 'system', 'source_temp')):
                os.makedirs(os.path.join(os.path.abspath(os.path.join(os.path.dirname('PakpoboxClient.py'), os.path.pardir)), 'system', 'source_temp'))
            if not os.path.exists(os.path.join(os.path.abspath(os.path.join(os.path.dirname('PakpoboxClient.py'), os.path.pardir)), 'system', 'source')):
                os.makedirs(os.path.join(os.path.abspath(os.path.join(os.path.dirname('PakpoboxClient.py'), os.path.pardir)), 'system', 'source'))
            dst_dir = os.path.join(os.path.abspath(os.path.join(os.path.dirname('PakpoboxClient.py'), os.path.pardir)), 'system', 'source_temp')
        if download_file_patch['type'] == 'music':
            if not os.path.exists(sys.path[0] + '/music/source_temp'):
                os.makedirs(sys.path[0] + '/music/source_temp')
            if not os.path.exists(sys.path[0] + '/music/source'):
                os.makedirs(sys.path[0] + '/music/source')
            dst_dir = os.path.join(os.getcwd(), 'music', 'source_temp')
        main_download_file(download_file_patch)
        download_post_message(download_info['id'], 'download file finish', 'SUCCESS')
        remove_file(download_file_patch)
    except Exception as e:
        _LOG_.error(('download_mk_dir ERROR :', e))
        download_post_message(download_info['id'], 'download_mk_dir except error', 'ERROR')


def main_download_file(download_file_patch):
    try:
        while True:
            downloaded_filename = download_file(download_file_patch)
            md5 = md5sum(os.path.join(dst_dir, downloaded_filename))
            correct_md5 = download.repository.DownloadDao.get_md5_by_id({'id': download_file_patch['id']})[0]
            if md5 != correct_md5['MD5']:
                download.repository.DownloadDao.update_download_info_status({'status': 'MD5 error', 'id': download_file_patch['id']})
                os.remove(os.path.join(dst_dir, download_file_patch['filename']))
                time.sleep(3)
            else:
                download.repository.DownloadDao.update_download_info_end({'status': 'finish', 'endTime': ClientTools.now(), 'id': download_file_patch['id']})
                return

    except Exception as e:
        _LOG_.error(('main_download_file ERROR :', e))
        download_post_message(download_info['id'], 'main_download_file except error', 'ERROR')


def download_file(file_path):
    try:
        download.repository.DownloadDao.update_download_info_start({'status': 'start', 'startTime': ClientTools.now(), 'id': file_path['id']})
        url = HttpClient.url + 'file/clientDownload/' + file_path['url']
        header = HttpClient.get_header()
        try:
            r = requests.get(url, stream=True, headers=header, timeout=50)
            with open(os.path.join(dst_dir, file_path['filename']), 'wb') as f:
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:
                        f.write(chunk)
                        f.flush()
                        continue

            return file_path['filename']
        except requests.RequestException:
            _LOG_.warning((NOT_INTERNET_, -1))

    except Exception as e:
        download.repository.DownloadDao.update_download_info_status({'status': 'download error', 'id': file_path['id']})
        _LOG_.error(('download_file ERROR :', e))
        download_post_message(download_info['id'], 'download_file except error', 'ERROR')


def md5sum(filename):
    try:
        fd = open(filename, 'rb')
        f_cont = fd.read()
        fd.close()
        fmd5 = hashlib.md5(f_cont)
        return fmd5.hexdigest()
    except Exception as e:
        _LOG_.error(('md5sum ERROR :', e))


def remove_file--- This code section failed: ---

 199       0  SETUP_EXCEPT        585  'to 585'

 200       3  SETUP_LOOP          581  'to 581'

 201       6  LOAD_GLOBAL              'dst_dir'
           9  STORE_FAST               'old_path'

 202      12  LOAD_GLOBAL              'str'
          15  LOAD_FAST                'file_list'
          18  LOAD_CONST               'position'
          21  BINARY_SUBSCR    
          22  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
          25  LOAD_CONST               '.'
          28  BINARY_ADD       
          29  LOAD_FAST                'file_list'
          32  LOAD_CONST               'filename'
          35  BINARY_SUBSCR    
          36  LOAD_ATTR                'split'
          39  LOAD_CONST               '.'
          42  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
          45  LOAD_CONST           -1  -1
          48  BINARY_SUBSCR    
          49  BINARY_ADD       
          50  STORE_FAST               'new_file_name'

 203      53  LOAD_GLOBAL              'print'
          56  LOAD_CONST               'new_file_name is :'
          59  LOAD_FAST                'new_file_name'
          62  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
          65  POP_TOP          

 204      66  LOAD_GLOBAL              'os'
          69  LOAD_ATTR                'rename'
          72  LOAD_GLOBAL              'os'
          75  LOAD_ATTR                'path'
          78  LOAD_ATTR                'join'
          81  LOAD_FAST                'old_path'
          84  LOAD_FAST                'file_list'
          87  LOAD_CONST               'filename'
          90  BINARY_SUBSCR    
          91  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
          94  LOAD_GLOBAL              'os'
          97  LOAD_ATTR                'path'
         100  LOAD_ATTR                'join'
         103  LOAD_FAST                'old_path'
         106  LOAD_FAST                'new_file_name'
         109  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         112  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         115  POP_TOP          

 205     116  LOAD_GLOBAL              'os'
         119  LOAD_ATTR                'path'
         122  LOAD_ATTR                'join'
         125  LOAD_GLOBAL              'dst_dir'
         128  LOAD_CONST            0  ''
         131  LOAD_GLOBAL              'dst_dir'
         134  LOAD_ATTR                'find'
         137  LOAD_CONST               'source_temp'
         140  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         143  LOAD_CONST            1  1
         146  BINARY_SUBTRACT  
         147  BUILD_SLICE_2         2 
         150  BINARY_SUBSCR    
         151  LOAD_CONST               'source'
         154  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         157  STORE_FAST               'new_path'

 206     160  LOAD_GLOBAL              'len'
         163  LOAD_GLOBAL              'os'
         166  LOAD_ATTR                'listdir'
         169  LOAD_FAST                'new_path'
         172  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         175  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         178  LOAD_CONST            0  ''
         181  COMPARE_OP               '!='
         184  POP_JUMP_IF_FALSE   435  'to 435'

 207     187  SETUP_LOOP          481  'to 481'
         190  LOAD_GLOBAL              'os'
         193  LOAD_ATTR                'listdir'
         196  LOAD_FAST                'new_path'
         199  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         202  GET_ITER         
         203  FOR_ITER            431  'to 431'
         206  STORE_FAST               'file_name'

 208     209  LOAD_GLOBAL              'os'
         212  LOAD_ATTR                'path'
         215  LOAD_ATTR                'isfile'
         218  LOAD_GLOBAL              'os'
         221  LOAD_ATTR                'path'
         224  LOAD_ATTR                'join'
         227  LOAD_FAST                'new_path'
         230  LOAD_FAST                'file_name'
         233  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         236  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         239  LOAD_CONST            1  True
         242  COMPARE_OP               'is'
         245  POP_JUMP_IF_FALSE   203  'to 203'

 209     248  LOAD_FAST                'file_name'
         251  LOAD_ATTR                'split'
         254  LOAD_CONST               '.'
         257  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         260  LOAD_CONST            0  ''
         263  BINARY_SUBSCR    
         264  STORE_FAST               '_file_name'

 210     267  LOAD_GLOBAL              'print'
         270  LOAD_CONST               '_file_name is :'
         273  LOAD_FAST                '_file_name'
         276  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         279  POP_TOP          

 211     280  LOAD_FAST                '_file_name'
         283  LOAD_GLOBAL              'str'
         286  LOAD_FAST                'file_list'
         289  LOAD_CONST               'position'
         292  BINARY_SUBSCR    
         293  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         296  COMPARE_OP               '=='
         299  POP_JUMP_IF_FALSE   379  'to 379'

 212     302  LOAD_GLOBAL              'os'
         305  LOAD_ATTR                'remove'
         308  LOAD_GLOBAL              'os'
         311  LOAD_ATTR                'path'
         314  LOAD_ATTR                'join'
         317  LOAD_FAST                'new_path'
         320  LOAD_FAST                'file_name'
         323  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         326  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         329  POP_TOP          

 213     330  LOAD_GLOBAL              'shutil'
         333  LOAD_ATTR                'copy'
         336  LOAD_GLOBAL              'os'
         339  LOAD_ATTR                'path'
         342  LOAD_ATTR                'join'
         345  LOAD_FAST                'old_path'
         348  LOAD_FAST                'new_file_name'
         351  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         354  LOAD_GLOBAL              'os'
         357  LOAD_ATTR                'path'
         360  LOAD_ATTR                'join'
         363  LOAD_FAST                'new_path'
         366  LOAD_FAST                'new_file_name'
         369  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         372  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         375  POP_TOP          
         376  JUMP_ABSOLUTE       428  'to 428'
         379  ELSE                     '425'

 215     379  LOAD_GLOBAL              'shutil'
         382  LOAD_ATTR                'copy'
         385  LOAD_GLOBAL              'os'
         388  LOAD_ATTR                'path'
         391  LOAD_ATTR                'join'
         394  LOAD_FAST                'old_path'
         397  LOAD_FAST                'new_file_name'
         400  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         403  LOAD_GLOBAL              'os'
         406  LOAD_ATTR                'path'
         409  LOAD_ATTR                'join'
         412  LOAD_FAST                'new_path'
         415  LOAD_FAST                'new_file_name'
         418  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         421  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         424  POP_TOP          
         425  CONTINUE            203  'to 203'
         428  JUMP_BACK           203  'to 203'
         431  POP_BLOCK        
       432_0  COME_FROM_LOOP           '187'
         432  JUMP_FORWARD        481  'to 481'
         435  ELSE                     '481'

 217     435  LOAD_GLOBAL              'shutil'
         438  LOAD_ATTR                'copy'
         441  LOAD_GLOBAL              'os'
         444  LOAD_ATTR                'path'
         447  LOAD_ATTR                'join'
         450  LOAD_FAST                'old_path'
         453  LOAD_FAST                'new_file_name'
         456  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         459  LOAD_GLOBAL              'os'
         462  LOAD_ATTR                'path'
         465  LOAD_ATTR                'join'
         468  LOAD_FAST                'new_path'
         471  LOAD_FAST                'new_file_name'
         474  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         477  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         480  POP_TOP          
       481_0  COME_FROM                '432'

 218     481  LOAD_GLOBAL              'os'
         484  LOAD_ATTR                'remove'
         487  LOAD_GLOBAL              'os'
         490  LOAD_ATTR                'path'
         493  LOAD_ATTR                'join'
         496  LOAD_FAST                'old_path'
         499  LOAD_FAST                'new_file_name'
         502  CALL_FUNCTION_2       2  '2 positional, 0 keyword pair'
         505  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         508  POP_TOP          

 220     509  LOAD_FAST                'file_list'
         512  LOAD_CONST               'type'
         515  BINARY_SUBSCR    
         516  LOAD_CONST               'UPDATE_ADVERT'
         519  COMPARE_OP               '=='
         522  POP_JUMP_IF_FALSE   544  'to 544'

 221     525  LOAD_GLOBAL              'download_signal_handler'
         528  LOAD_ATTR                'ad_download_result_signal'
         531  LOAD_ATTR                'emit'
         534  LOAD_CONST               'ok'
         537  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         540  POP_TOP          
         541  JUMP_FORWARD        544  'to 544'
       544_0  COME_FROM                '541'

 222     544  LOAD_FAST                'file_list'
         547  LOAD_CONST               'type'
         550  BINARY_SUBSCR    
         551  LOAD_CONST               'system'
         554  COMPARE_OP               '=='
         557  POP_JUMP_IF_FALSE   576  'to 576'

 223     560  LOAD_GLOBAL              'os'
         563  LOAD_ATTR                'system'
         566  LOAD_CONST               'update.vbs'
         569  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         572  POP_TOP          
         573  JUMP_FORWARD        576  'to 576'
       576_0  COME_FROM                '573'

 224     576  LOAD_CONST               ''
         579  RETURN_VALUE     
         580  POP_BLOCK        
       581_0  COME_FROM_LOOP           '3'
         581  POP_BLOCK        
         582  JUMP_FORWARD        641  'to 641'
       585_0  COME_FROM_EXCEPT         '0'

 225     585  DUP_TOP          
         586  LOAD_GLOBAL              'Exception'
         589  COMPARE_OP               'exception-match'
         592  POP_JUMP_IF_FALSE   640  'to 640'
         595  POP_TOP          
         596  STORE_FAST               'e'
         599  POP_TOP          
         600  SETUP_FINALLY       627  'to 627'

 226     603  LOAD_GLOBAL              'logger'
         606  LOAD_ATTR                'error'
         609  LOAD_CONST               'remove_file ERROR :'
         612  LOAD_FAST                'e'
         615  BUILD_TUPLE_2         2 
         618  CALL_FUNCTION_1       1  '1 positional, 0 keyword pair'
         621  POP_TOP          
         622  POP_BLOCK        
         623  POP_EXCEPT       
         624  LOAD_CONST               ''
       627_0  COME_FROM_FINALLY        '600'
         627  LOAD_CONST               ''
         630  STORE_FAST               'e'
         633  DELETE_FAST              'e'
         636  END_FINALLY      
         637  JUMP_FORWARD        641  'to 641'
         640  END_FINALLY      
       641_0  COME_FROM                '637'
       641_1  COME_FROM                '582'

Parse error at or near `POP_BLOCK' instruction at offset 580


def download_post_message(task_id, result, reset_status):
    reset_express_result = {'id': task_id, 'result': result, 'statusType': reset_status}
    HttpClient.post_message('task/finish', reset_express_result)


def delete_post_message(task_id, result, reset_status):
    reset_express_result = {'id': task_id, 'result': result, 'statusType': reset_status}
    HttpClient.post_message('task/finish', reset_express_result)


def check_position_file_name(path, position):
    print(path)
    print(position)
    print(os.listdir(path))
    for file_name in os.listdir(path):
        if os.path.isfile(os.path.join(path, file_name)) is True:
            _file_name = file_name.split('.')[0]
            print('_file_name is :', _file_name)
            if _file_name == position:
                os.remove(os.path.join(path, file_name))
            else:
                _LOG_.debug('not file')
                continue