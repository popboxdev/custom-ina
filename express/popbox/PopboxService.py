import requests
import json
import logging
import os
import datetime
import time
import base64
import glob
import random
import sys
import ClientTools
import Configurator
import box.service.BoxService as BoxService
import express.repository.ExpressDao as ExpressDao
import company.service.CompanyService as CompanyService
import express.service.ExpressService as ExpressService
from PyQt5.QtCore import QObject, pyqtSignal
from network import HttpClient
from device import QP3000S
from device import Scanner
# from device import Detector
from database import ClientDatabase
from urllib.request import urlopen
import schedule
from win32 import win32api 
import subprocess
import ntplib
import sys, os
# import paho.mqtt.client as mqtt #MQTT Modul
##
#import sentry_sdk
#from sentry_sdk import capture_message

__author__ = 'wahyudi@popbox.asia'

class PopSignalHandler(QObject):
    __qualname__ = 'PopSignalHandler'
    start_get_express_info_result_signal = pyqtSignal(str)
    start_get_popbox_express_info_signal = pyqtSignal(str)
    start_get_customer_info_by_card_signal = pyqtSignal(str)
    start_payment_by_card_signal = pyqtSignal(str)
    start_get_cod_status_signal = pyqtSignal(str)
    start_reset_partial_transaction_signal = pyqtSignal(str)
    start_post_capture_file_signal = pyqtSignal(str)
    start_get_capture_file_location_signal = pyqtSignal(str)
    start_get_locker_name_signal = pyqtSignal(str)
    start_global_payment_emoney_signal = pyqtSignal(str)
    start_get_sepulsa_product_signal = pyqtSignal(str)
    start_sepulsa_transaction_signal = pyqtSignal(str)
    start_get_gui_version_signal = pyqtSignal(str)
    start_customer_scan_qr_code_signal = pyqtSignal(str)
    stop_customer_scan_qr_code_signal = pyqtSignal(str)
    get_popshop_product_signal = pyqtSignal(str)
    start_push_data_settlement_signal = pyqtSignal(str)
    get_query_member_popsend_signal = pyqtSignal(str)
    start_popsend_topup_signal = pyqtSignal(str)
    get_locker_data_signal = pyqtSignal(str)
    start_popshop_transaction_signal = pyqtSignal(str)
    get_popsend_button_signal = pyqtSignal(str)
    sepulsa_trx_check_signal = pyqtSignal(str)
    box_start_migrate_signal = pyqtSignal(str)
    start_get_ads_images_signal = pyqtSignal(str)
    start_delete_express_by_range_signal = pyqtSignal(str)
    start_post_gui_info_signal = pyqtSignal(str)
    start_post_subscribe_data_signal = pyqtSignal(str)
    start_get_file_dir_signal = pyqtSignal(str)
    start_get_detection_signal = pyqtSignal(str)
    start_create_payment_signal = pyqtSignal(str)
    start_check_trans_global_signal = pyqtSignal(str)
    start_time_overdue_result_signal = pyqtSignal(str)
    start_hour_overdue_result_signal = pyqtSignal(str)
    

start_flag = True
comp_stats = ""
_POP_ = PopSignalHandler()
_LOG_ = logging.getLogger()
img_path = sys.path[0] + '/video_capture/'
NOT_INTERNET_ = {
    'statusCode': -1,
    'statusMessage': 'Not Internet'}
customer_scanner_signal_connect_flag = False
pin_code = ''

# Initial Version Information
version_path = os.path.join(os.getcwd(), 'version.txt')
version = open(version_path, 'r').read().strip()
Configurator.set_value('version', 'version', version)
server = Configurator.get_or_set_value('ClientInfo', 'prox^prefix', 'pr0x')

# print('version : ', version_path, version)


##################################################################
# BoxService = box.service.BoxService
# ExpressDao = express.repository.ExpressDao
# CompanyService = company.service.CompanyService
# ExpressService = express.service.ExpressService
##################################################################

#######################TOP SUPPORTING FUNCTION####################
def development_progress():
    if Configurator.get_value('panel', 'setserver') == "dev":
        return True
    else:
        _LOG_.info('Public live GUI is Running')
        return False


##################################################################
def uat_mode():
    uat_link = ""
    try:
        if Configurator.get_value('panel', 'uat') == "yes":
            uat_link = "UAT"
    except:
        pass
    finally:
        return uat_link
##################################################################
is_UAT = uat_mode()

# Get Status of Development
is_dev = development_progress()
if is_dev:
    global_url = "http://api-dev.popbox.asia/"
    global_token = "767ae89fec88a53dbb465adb4d0c76c294a8a28e"
else:
    global_url = Configurator.get_value('popbox', 'popserver')
    global_token = Configurator.get_value('popbox', 'poptoken')

dev_token = "767ae89fec88a53dbb465adb4d0c76c294a8a28e"
dev_url = "http://api-dev.popbox.asia/"
##################################################################
# Get Locker Information
locker_info = BoxService.get_box()
gui_version = version
BoxService.version = version
# SSL_VERIFY = True if Configurator.get_or_set_value('panel', 'ssl^verify', '1') == '1' else False
SSL_VERIFY = True


##################################################################
###SUPPORTING_FUNCTIONS###
##################################################################
def take_capture_file():
    global latest_img
    img_name = None
    img_b64 = None
    xchars = ['D:', '\\', '.jpg', 'video_capture', '/', 'popboxgui_webkit2']
    try:
        latest_img = max(glob.iglob(img_path + '*.jpg'), key=os.path.getctime)
        img_name = rename_file(latest_img, xchars, '')
        with open(latest_img, 'rb') as imageFile:
            img_b64 = base64.b64encode(imageFile.read())
    finally:
        return img_name, img_b64


##################################################################
def capture_photo_post(url_post, param):
    _LOG_.info('url:' + str(url_post) + '; json: ' + str(param))
    try:
        r = requests.post(url_post, json=param, timeout=50)
    except requests.RequestException:
        _LOG_.debug((NOT_INTERNET_, url_post, -1))
        return NOT_INTERNET_, -1
    try:
        r_response = r.json()
        r_status = r_response['response']['code']
    except ValueError:
        _LOG_.debug(('ValueError on : ', url_post))
        return NOT_INTERNET_, -1
    _LOG_.info((r_status, r_response))
    return r_status, r_response


##################################################################
def rename_file(filename, list, x):
    for char in list:
        filename = filename.replace(char, x)
    return filename


##################################################################
def get_list_popshop(url_param, param=None):
    _LOG_.info('url:' + str(url_param) + '; json: ' + str(param))
    headers = {'content-type': "application/json"}
    try:
        r = requests.post(url_param, json=param, timeout=50, headers=headers)
    except requests.RequestException:
        _LOG_.debug((NOT_INTERNET_, -1))
        return NOT_INTERNET_, -1
    try:
        r_json = r.json()
        r_status = 0
        if "data" in r_json:
            r_status = 200
        elif "response" in r_json:
            r_status = 400
    except ValueError:
        _LOG_.debug(('ValueError on : ', url_param))
        return NOT_INTERNET_
    _LOG_.info((r_json, r_status))
    return r_json, r_status


##################################################################
def global_post(url_param, param=None):
    global SSL_VERIFY
    headers = {'content-type': "application/json"}
    r_status = '404'
    # _LOG_.debug('---<URL> : ' + str(url_param) + ', <POST> : ' + str(param))
    try:
        if 'https' in url_param:
            r = requests.post(url_param, json=param, timeout=60, headers=headers, verify=SSL_VERIFY)
        else:
            r = requests.post(url_param, json=param, timeout=60, headers=headers)
    except requests.RequestException:
        _LOG_.debug((NOT_INTERNET_, -1))
        return NOT_INTERNET_, -1

    try:
        r_json = r.json()
        r_status = r_json["response"]["code"]
    except ValueError:
        _LOG_.warning(('ValueError on ', url_param, str(r.content)))
        return NOT_INTERNET_, r_status
    _LOG_.debug('***<URL> : ' + str(url_param) + ' | <STATUS> : ' + str(r_status) + ' | <POST> : ' + str(param) +
                ' | <RESP> : ' + str(r_json))
    return r_json, r_status


##################################################################
def get_return_value(value):
    new_value = str(value)
    return new_value


##################################################################
def random_chars(type_of, length_of=6):
    while True:
        if type_of == 'LETTER_AND_NUMBER_VALIDATE_CODE':
            chars = 'ABCDEFGHJKMNPQRSTUVWXYZ23456789'
            validate_code = ''
            i = 0
            while i < length_of:
                validate_code += random.choice(chars)
                i += 1

            if len(ExpressDao.get_express_by_validate({'validateCode': validate_code,
                                                       'status': 'IN_STORE'})) == 0:
                return validate_code
        else:
            return ''


##################################################################
def resync_table(sql, param):
    ClientDatabase.insert_or_update_database(sql=sql, parameter=param)
    _LOG_.debug(("resync_table (sql => param) : " + sql + " => " + param))


##################################################################
def force_rename(file1, file2):
    from shutil import move
    try:
        move(file1, file2)
        return True
    except:
        return False


##################################################################
def get_overdue_hours(h):
    try:
        start_time = datetime.datetime.now()
        end_time = start_time + datetime.timedelta(hours=int(h))
        return int(time.mktime(time.strptime(end_time.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')) * 1000)
    except Exception as e:
        _LOG_.debug(('get_overdue_hours', str(e)))


##################################################################

###CALLABLE_FUNCTIONS###
def start_get_express_info(express_text):
    ClientTools.get_global_pool().apply_async(get_express_info, (express_text,))


def get_express_info(text):
    global popbox_express_info
    global cop_order_number
    try:
        popbox_express_info = ''
        cop_order_number = text
        popbox_info = dict()
        popbox_info["order_number"] = text
        popbox_info["token"] = global_token
        query_url = global_url + 'cop/detail'
        _LOG_.debug(('url_query_cop : ', query_url))
        express_message, status_code = HttpClient.pakpobox_get_message(query_url, popbox_info)
        if status_code == 200:
            if express_message["response"]["message"] == 'AVAILABLE':
                if len(express_message["data"][0]) != 0:
                    _LOG_.debug(('get_express_info express_message: ', express_message['data']))
                    popbox_express_info = express_message['data'][0]
                    QP3000S.price = express_message['data'][0]['order_amount']
                    _POP_.start_get_express_info_result_signal.emit('SUCCESS')
                else:
                    _POP_.start_get_express_info_result_signal.emit('ERROR')
            elif express_message["response"]["message"] == 'NOT AVAILABLE':
                _POP_.start_get_express_info_result_signal.emit('ERROR')
            elif express_message["response"]["message"] == 'PAID':
                if len(express_message["data"][0]) != 0:
                    _LOG_.debug(('get_express_info express_message: ', express_message['data']))
                    popbox_express_info = express_message['data'][0]
                    _POP_.start_get_express_info_result_signal.emit('PAID')
                else:
                    _POP_.start_get_express_info_result_signal.emit('ERROR')
        else:
            _POP_.start_get_express_info_result_signal.emit('ERROR')
    except Exception as e:
        _POP_.start_get_express_info_result_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] get_express_info : ', str(e)))


def start_get_popbox_express_info():
    ClientTools.get_global_pool().apply_async(get_popbox_express_info)


def get_popbox_express_info():
    _POP_.start_get_popbox_express_info_signal.emit(json.dumps(popbox_express_info))


def start_get_customer_info_by_card():
    ClientTools.get_global_pool().apply_async(get_customer_info_by_card)


def get_customer_info_by_card():
    global card_info
    try:
        flag = QP3000S.init_serial(x=1)
        _LOG_.debug(('start_get_customer_info_by_card flag: ', flag))
        if flag:
            card_info = QP3000S.balanceInfo()
            _LOG_.debug(('start_get_customer_info_by_card flag: ', card_info))
            if card_info == 'ERROR':
                _POP_.start_get_customer_info_by_card_signal.emit('ERROR')
            elif card_info == 'EMPTY':
                _POP_.start_get_customer_info_by_card_signal.emit('EMPTY')
            elif card_info == 'WRONG-CARD':
                _POP_.start_get_customer_info_by_card_signal.emit('WRONG-CARD')
            else:
                _POP_.start_get_customer_info_by_card_signal.emit(card_info)
        else:
            _POP_.start_get_customer_info_by_card_signal.emit('ERROR')
    except Exception as e:
        _POP_.start_get_customer_info_by_card_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] get_customer_info_by_card : ', str(e)))


UNFINISHED_CODE = ['6F00', '1004', '6700', '1015']
WRONGCARD_CODE = ['6A82', 'FFFE', '6999', '1024']


def start_payment_by_card():
    ClientTools.get_global_pool().apply_async(payment_by_card)


def payment_by_card():
    global time_stamp
    try:
        payment_result_info = dict()
        flag = QP3000S.init_serial(x=1)
        _LOG_.debug(('start_get_card_info flag: ', flag))
        response_purcdeb = ""
        if flag:
            response_purcdeb = QP3000S.purcDeb()
            _LOG_.debug(('init_status_payment : ', response_purcdeb))
            if response_purcdeb == 'SUCCESS':
                payment_info = QP3000S.paymentSuccess()
                _LOG_.debug(('start_get_card_info getReport result : ', payment_info))
                payment_result_info["card_no"] = payment_info[4:20]
                payment_result_info["last_balance"] = payment_info[32:40].lstrip('0')
                time_stamp = int(time.time())
                payment_result_info["show_date"] = time_stamp * 1000
                payment_result_info["terminal_id"] = Configurator.get_value('popbox', 'terminalID')
                payment_result_info["locker"] = locker_info["name"]
                _POP_.start_payment_by_card_signal.emit(json.dumps(payment_result_info))
                _LOG_.info(('status_payment: ', response_purcdeb, ' get signal : SUCCESS'))
            elif response_purcdeb in UNFINISHED_CODE:
                _POP_.start_payment_by_card_signal.emit('UNFINISHED')
                _LOG_.debug(('status_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
            elif response_purcdeb in WRONGCARD_CODE:
                _POP_.start_payment_by_card_signal.emit('WRONG-CARD')
                _LOG_.debug(('status_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
            else:
                _POP_.start_payment_by_card_signal.emit('ERROR')
                _LOG_.warning(('status_payment: ', response_purcdeb, ' get signal : ERROR'))
        else:
            _POP_.start_payment_by_card_signal.emit('FAILED')
            _LOG_.warning(('status_payment: ', response_purcdeb, ' get signal : FAILED'))
    except Exception as e:
        _POP_.start_payment_by_card_signal.emit('FAILED')
        _LOG_.warning(('[ERROR] payment_by_card: ', str(e)))


def start_get_cod_status():
    ClientTools.get_global_pool().apply_async(get_cod_status)


def get_cod_status():
    cod_status = Configurator.get_value('panel', 'emoney')
    _POP_.start_get_cod_status_signal.emit(cod_status)


def start_get_gui_version():
    ClientTools.get_global_pool().apply_async(get_gui_version)


def get_gui_version():
    global version, gui_version
    version_path = os.path.join(os.getcwd(), 'version.txt')
    version = open(version_path, 'r').read().strip()
    gui_version = version
    BoxService.version = version
    Configurator.set_value('version', 'version', version)
    _POP_.start_get_gui_version_signal.emit(version)


def start_reset_partial_transaction():
    ClientTools.get_global_pool().apply_async(reset_partial_transaction)


def reset_partial_transaction():
    try:
        flag = QP3000S.init_serial(x=1)
        _LOG_.debug(('reset_partial_transaction flag: ', flag))
        reset_partial = ""
        if flag:
            reset_partial = QP3000S.resetPartial()
            _LOG_.debug(('init_reset_partial_status: ', reset_partial))
            if reset_partial == '0000':
                _POP_.start_reset_partial_transaction_signal.emit('RESET-SUCCESS')
                _LOG_.info(('reset_partial_status: ', reset_partial))
            else:
                _POP_.start_reset_partial_transaction_signal.emit('RESET-FAILED')
                _LOG_.warning(('reset_partial_status: ', reset_partial))
        else:
            _POP_.start_reset_partial_transaction_signal.emit('RESET-FAILED')
            _LOG_.warning(('reset_partial_status: ', reset_partial))
    except Exception as e:
        _POP_.start_reset_partial_transaction_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] reset_partial_transaction: ', str(e)))


def start_post_capture_file():
    ClientTools.get_global_pool().apply_async(post_capture_file)


def post_capture_file():
    photo_param = dict()
    result_name, result_b64 = take_capture_file()
    photo_param['token'] = global_token
    photo_param['locker_name'] = locker_info['name']
    photo_param['event_name'] = result_name
    photo_param['datetime'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
    photo_param['base64_image'] = result_b64.decode('ascii')
    photo_url = global_url + 'locker/photo'
    _LOG_.debug(('url_photo_upload : ', photo_url))
    while True:
        status, response = capture_photo_post(photo_url, photo_param)
        _LOG_.info(('start_post_capture_file flag: ', photo_url, status))
        if status == '200' and response['response']['message'] == 'OK':
            return
        time.sleep(2)


def start_get_capture_file_location():
    ClientTools.get_global_pool().apply_async(get_capture_file_location)


def get_capture_file_location():
    chars = ['D:\\popboxgui_webkit2/video_capture\\']
    filename = None
    try:
        captured_image = max(glob.iglob(img_path + '*.jpg'), key=os.path.getctime)
        _LOG_.info(('value check :', captured_image))
        filename = rename_file(captured_image, chars, '../video_capture/')
        _POP_.start_get_capture_file_location_signal.emit(filename)
        _LOG_.info(('filename check :', filename))
    except Exception as e:
        _LOG_.warning(('get_capture_file_location failed:', filename, e))
        _POP_.start_get_capture_file_location_signal.emit('ERROR')


def start_get_locker_name():
    ClientTools.get_global_pool().apply_async(get_locker_name)


def get_locker_name():
    global locker_name
    locker_name = locker_info['name']
    _POP_.start_get_locker_name_signal.emit(locker_info["name"])


def start_global_payment_emoney(amount):
    ClientTools.get_global_pool().apply_async(global_payment_emoney, (amount,))
    print("pyt: [TRIGGER] global_payment_emoney("+ str(amount)+") | " + str(int(time.time()) * 1000))


DUMMY_RESPONSE = True if Configurator.get_value('popbox', 'dummy^transaction') == '1' else False
print('DUMMY_RESPONSE ', str(DUMMY_RESPONSE))

def global_payment_emoney(amount):
    global global_payment_info
    global timestamp_payment
    global_payment_info = ''
    response_purcdeb = None
    timestamp_payment = int(time.time()) * 1000
    print("pyt: [TRIGGER] global_payment_emoney("+ str(amount)+") | DUMMY_RESPONSE : " + str(DUMMY_RESPONSE))
    if DUMMY_RESPONSE is True:
        dummy = {
            "show_date": timestamp_payment,
            "locker": locker_info['name'],
            "last_balance": str(100000-int(amount)),
            "card_no": "12345678987654321",
            "terminal_id": Configurator.get_value('popbox', 'terminalID'),
            "raw": "60000123123123123123123123123123123123123" + "|" + str(timestamp_payment)
            }
        print("pyt: [RESULT] global_payment_emoney("+ str(amount)+") | " + str(timestamp_payment) + " | " + str(dummy))
        _POP_.start_global_payment_emoney_signal.emit(json.dumps(dummy))
        return
    try:
        flag = QP3000S.init_serial(x=1)
        time.sleep(1)
        if flag:
            response_purcdeb = QP3000S.purcDebGlobal(amountX=amount)
            _LOG_.debug(('init_status_payment : ', response_purcdeb + ', with price :', amount))
            if response_purcdeb == 'SUCCESS':
                global_payment_info = QP3000S.paymentSuccess()
                settlement = QP3000S.settlement()
                _LOG_.debug(('start_global_payment_emoney getReport result : ', global_payment_info))
                payment_result_info = {
                    "card_no": global_payment_info[4:20],
                    "last_balance": global_payment_info[32:40].lstrip('0'),
                    "show_date": timestamp_payment,
                    "terminal_id": Configurator.get_value('popbox', 'terminalID'),
                    "locker": locker_info["name"], 
                    "raw" : global_payment_info + "|" + str(timestamp_payment),
                    "settle_code": settlement
                }
                _POP_.start_global_payment_emoney_signal.emit(json.dumps(payment_result_info))
                _LOG_.info(('status_global_payment: ', response_purcdeb, ' param: ', payment_result_info))
            elif response_purcdeb in UNFINISHED_CODE:
                _POP_.start_global_payment_emoney_signal.emit('UNFINISHED')
                _LOG_.debug(('status_global_payment: ', response_purcdeb, ' get signal : UNFINISHED'))
            elif response_purcdeb in WRONGCARD_CODE:
                _POP_.start_payment_by_card_signal.emit('WRONG-CARD')
                _LOG_.debug(('status_global_payment: ', response_purcdeb, ' get signal : WRONG-CARD'))
            else:
                _POP_.start_global_payment_emoney_signal.emit('ERROR')
                _LOG_.warning(('status_global_payment: ', response_purcdeb, ' get signal : ERROR'))
        else:
            _POP_.start_global_payment_emoney_signal.emit('FAILED')
            _LOG_.warning(('status_global_payment: ', response_purcdeb, ' get signal : FAILED'))
    except Exception as e:
         _POP_.start_global_payment_emoney_signal.emit('FAILED')
         _LOG_.warning(str(e))


def start_get_sepulsa_product(phone):
    ClientTools.get_global_pool().apply_async(get_sepulsa_product, (phone,))


def get_sepulsa_product(phone):
    try:
        sepulsa_express_info = dict()
        sepulsa_info = dict()
        sepulsa_info["phone"] = str(phone)
        sepulsa_info["token"] = global_token
        if is_dev:
            query_url = global_url + 'service/sepulsa/getPulsaProduct2' + is_UAT
        else:
            query_url = global_url + 'service/sepulsa/getPulsaProduct' + is_UAT
        _LOG_.debug(('url_get_sepulsa : ', query_url))
        express_message, status_code = global_post(query_url, sepulsa_info)
        if status_code == 200 and express_message["response"]["message"] == 'OK':
            if len(express_message["data"]) != 0:
                _LOG_.info(('get_sepulsa_product_info : ', status_code, express_message['data']))
                sepulsa_express_info["data"] = express_message["data"]["pulsa_list"]
                sepulsa_express_info["operator"] = express_message["data"]["operator"]
                sepulsa_express_info["avail_prod"] = len(sepulsa_express_info["data"])
                _POP_.start_get_sepulsa_product_signal.emit(json.dumps(sepulsa_express_info))
            else:
                _POP_.start_get_sepulsa_product_signal.emit('ERROR')
                _LOG_.warning(('get_sepulsa_product_info : ', status_code, express_message["response"]["message"]))
        else:
            _POP_.start_get_express_info_result_signal.emit('ERROR')
            _LOG_.warning(('[ERROR] get_sepulsa_product_info : ', str(express_message)))
    except Exception as e:
        _POP_.start_get_express_info_result_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] get_sepulsa_product : ', str(e)))


def start_sepulsa_transaction(phone_no, prod_id, prod_type, pay_type):
    ClientTools.get_global_pool().apply_async(sepulsa_transaction, (phone_no, prod_id, prod_type, pay_type,))


def sepulsa_transaction(phone_no, prod_id, prod_type, pay_type):
    global transaction_param
    try:
        if phone_no == "" or prod_id == "" or prod_type == "" or pay_type == "":
            _LOG_.warning('[ERROR] missing parameter')
            return
        transaction_param = dict()
        if is_dev:
            query_url = global_url + 'service/sepulsa/postTransaction2'
        else:
            query_url = global_url + 'service/sepulsa/postTransaction'
        _LOG_.debug(('url_postTransaction_sepulsa : ', query_url))
        if pay_type == 'emoney':
            if global_payment_info != "":
                sepulsa_trans_info = global_payment_info
            else:
                sepulsa_trans_info = QP3000S.getReport()
            _LOG_.debug(('sepulsa_trans_info param : ', sepulsa_trans_info))
            transaction_param["product_amount"] = sepulsa_trans_info[24:32].lstrip('0')
            transaction_param["card_number"] = sepulsa_trans_info[4:20]
            transaction_param["payment_type"] = str(pay_type)
            try:
                transaction_param["last_balance"] = int(sepulsa_trans_info[32:40].lstrip('0'))
            except Exception as e:
                _LOG_.debug(('Empty Last Balance, Assuming 0', str(e)))
                transaction_param["last_balance"] = 0
        else:
            # Except emoney the pay_tipe format will be "name|trx_no|amount"
            p = pay_type.split('|')
            transaction_param["payment_type"] = p[0].lower()
            transaction_param["card_number"] = p[1]
            transaction_param["product_amount"] = p[2]
            transaction_param["last_balance"] = 0
        transaction_param["token"] = global_token
        transaction_param["phone"] = str(phone_no)
        transaction_param["product_type"] = str(prod_type).lower()
        transaction_param["product_id"] = str(prod_id)
        transaction_param["locker_name"] = locker_info["name"]
        express_message, status_code = global_post(query_url, transaction_param)
        if status_code == 200 and express_message["response"]["message"] == 'OK':
            _POP_.start_sepulsa_transaction_signal.emit(express_message["data"][0]["invoice_id"])
            _LOG_.info(('start_sepulsa_transaction result : ', status_code, express_message["data"]))
        else:
            _POP_.start_sepulsa_transaction_signal.emit("ERROR")
            _LOG_.warning(('[ERROR] sepulsa_transaction : ', str(express_message)))
    except Exception as e:
        _POP_.start_sepulsa_transaction_signal.emit("ERROR")
        _LOG_.warning(str(e))


def start_push_data_settlement(type__):
    ClientTools.get_global_pool().apply_async(push_data_settlement, (type__,))


def push_data_settlement(type__):
    if DUMMY_RESPONSE is True:
        _POP_.start_push_data_settlement_signal.emit('NO SETTLE DUMMY')
        _LOG_.info(('push_data_settlement info: ', 'No Settlement for Dummy Transaction'))
        return
    order_transaction_info = dict()
    flag = QP3000S.init_serial(x=1)
    if flag:
        current_time = time.localtime(int(time.time()))
        settlement = QP3000S.settlement()
        _LOG_.debug(('push_data_settlement settlement: ', settlement + ', for :', type__))
        order_transaction_info["token"] = global_token
        order_transaction_info["order_number"] = type__
        order_transaction_info["settle_code"] = settlement
        order_transaction_info["order_amount"] = settlement[46:54].lstrip('0')
        order_transaction_info["settle_timestamp"] = time.strftime('%Y-%m-%d %H:%M:%S', current_time)
        order_transaction_info["settle_place"] = locker_info["name"]
        _LOG_.info(('push_data_settlement info: ', order_transaction_info))
        start_post_payment_transaction(order_transaction_info)
        _POP_.start_push_data_settlement_signal.emit(order_transaction_info)
    else:
        _POP_.start_push_data_settlement_signal.emit('SETTLE FAILED')


def start_post_payment_transaction(payment_info):
    ClientTools.get_global_pool().apply_async(post_payment_transaction, (payment_info,))


def post_payment_transaction(payment_info):
    settle_url = global_url + 'cop/submit'
    _LOG_.debug(('settlement_server_url : ', settle_url))
    while True:
        express_message, status_code = global_post(settle_url, payment_info)
        if (status_code == "200" or status_code == 200) and express_message is not None:
            return
        time.sleep(3)


def start_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.connect(customer_scan_qr_code)
        customer_scanner_signal_connect_flag = True
    Scanner.start_get_text_info()


def stop_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        return
    customer_scanner_signal_connect_flag = False
    Scanner._SCANNER_.barcode_result.disconnect(customer_scan_qr_code)
    Scanner.start_stop_scanner()
    _POP_.stop_customer_scan_qr_code_signal.emit('CLOSED')


def customer_scan_qr_code(result):
    global pin_code
    global customer_scanner_signal_connect_flag
    if result == '':
        customer_scanner_signal_connect_flag = False
        _POP_.start_customer_scan_qr_code_signal.emit(result)
        return
    pin_code = str(result)
    customer_scanner_signal_connect_flag = False
    _POP_.start_customer_scan_qr_code_signal.emit(pin_code)


def get_popshop_product(category, maxcount):
    ClientTools.get_global_pool().apply_async(popshop_product, (category, maxcount,))


def popshop_product(category, maxcount):
    try:
        popshop_express_info = dict()
        popshop_info = dict()
        popshop_info["token"] = global_token
        popshop_info["page"] = 1
        popshop_info["category"] = int(category)
        popshop_info["pagesize"] = int(maxcount)
        query_url = global_url + 'paybyqr/list'
        _LOG_.debug(('url_get_popshop : ', query_url))
        express_message, status_code = get_list_popshop(query_url, popshop_info)
        if status_code == 200:
            popshop_express_info["prod_list"] = express_message["data"][0]
            popshop_express_info["prod_total"] = len(express_message["data"][0])
            _POP_.get_popshop_product_signal.emit(str(json.dumps(popshop_express_info)))
        else:
            _POP_.get_popshop_product_signal.emit('ERROR')
            _LOG_.warning(('[ERROR] popshop_product : ', str(express_message)))
    except Exception as e:
        _POP_.get_popshop_product_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] popshop_product : ', str(e)))


def get_query_member_popsend(phone):
    ClientTools.get_global_pool().apply_async(query_member_popsend, (phone,))


def query_member_popsend(phone):
    global post_data_popsend
    try:
        post_data_popsend = {"token": global_token, "phone": str(phone)}
        query_end = global_url + 'member/detail'
        _LOG_.debug(('url_query_member_popsend : ', query_end))
        express_message, status_code = global_post(query_end, post_data_popsend)
        if status_code == 200 and express_message["total_page"] != 0:
            get_data_popsend = express_message["data"][0]
            get_data_popsend['balance'] = get_member_balance(param=post_data_popsend)
            _POP_.get_query_member_popsend_signal.emit(str(json.dumps(get_data_popsend)))
            _LOG_.info(('get_query_member_popsend result : ', get_data_popsend))
        else:
            _POP_.get_query_member_popsend_signal.emit('NOT FOUND')
            _LOG_.warning(('[ERROR] query_member_popsend : ', str(express_message)))
    except Exception as e:
        _POP_.get_query_member_popsend_signal.emit('NOT FOUND')
        _LOG_.warning(('[ERROR] query_member_popsend : ', str(e)))


def get_member_balance(param):
    try:
        url = global_url + 'balance/info'
        _LOG_.debug(('get_member_balance : ', url))
        response, status = global_post(url, param)
        if (status == 200 or status == "200") and response['data'][0]['current_balance'] is not None:
            member_balance = response['data'][0]['current_balance']
        else:
            member_balance = 0
        print(member_balance)
        return member_balance
    except Exception as e:
        _LOG_.warning(('[ERROR] get_member_balance : ', str(e)))
        return None


def start_popsend_topup(amount):
    ClientTools.get_global_pool().apply_async(popsend_topup, (amount,))


def popsend_topup(amount):
    try:
        post_popsend_topup = dict()
        post_popsend_topup["token"] = global_token
        post_popsend_topup["phone"] = post_data_popsend["phone"]
        post_popsend_topup["top_up_amount"] = str(amount)
        timestamp_topup = int(time.time()) * 1000
        post_popsend_topup["transid"] = post_data_popsend["phone"] + "BAL_LOC" + str(timestamp_topup)
        query_end_ = global_url + "balance/topup"
        _LOG_.debug(('url_popsend_topup : ', query_end_))
        express_message, status_code = global_post(query_end_, post_popsend_topup)
        if status_code == "200":
            get_popsend_balance = str(express_message["data"][0]["current_balance"])
            _POP_.start_popsend_topup_signal.emit(get_popsend_balance)
            _LOG_.info(('start_popsend_topup result : ', get_popsend_balance))
        else:
            _POP_.start_popsend_topup_signal.emit('NOT FOUND')
            _LOG_.warning(('start_popsend_topup ERROR : ', str(express_message)))
    except Exception as e:
        _POP_.start_popsend_topup_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] locker_data : ', str(e)))


def get_locker_data(province, city):
    ClientTools.get_global_pool().apply_async(locker_data, (province, city,))


def locker_data(province, city):
    try:
        param = {
            "token": global_token,
            "country": "Indonesia",
            "zip_code": "",
            "province": str(province),
            "city": str(city)
        }
        url = global_url + 'locker/location'
        _LOG_.debug(('url_locker_data : ', url))
        response, status = global_post(url, param)
        if response["response"]["message"] == "OK":
            result = {"data": response["data"], "total": response["total_data"]}
            _POP_.get_locker_data_signal.emit(str(json.dumps(result)))
        else:
            _POP_.get_locker_data_signal.emit('ERROR')
            _LOG_.warning(('get_locker_data ERROR : ', str(response), str(status)))
    except Exception as e:
        _POP_.get_locker_data_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] locker_data : ', str(e)))


def start_popshop_transaction(product, customer, purchase, address):
    ClientTools.get_global_pool().apply_async(popshop_transaction, (product, customer, purchase, address,))


def popshop_transaction(product, customer, purchase, address):
    global timestamp_payment
    try:
        timestamp_payment = int(time.time()) * 1000
        time_payment = datetime.datetime.fromtimestamp(timestamp_payment/1000).strftime('%Y-%m-%d %H:%M')
        post_popshop_transaction = dict()
        post_popshop_transaction["token"] = global_token
        post_popshop_transaction["product_info"] = str(product)
        post_popshop_transaction["customer_info"] = str(customer)
        post_popshop_transaction["purchase_info"] = str(purchase) + "|" + str(time_payment)
        post_popshop_transaction["delivery_address"] = str(address)
        end_url = global_url + "ordershop/submit"
        _LOG_.debug(('url_popshop_transaction : ', end_url))
        express_message, status_code = global_post(end_url, post_popshop_transaction)
        if status_code == 200 and express_message["response"]["message"] == "SUCCESS":
            get_popshop_transaction = express_message["data"]["invoice_id"]
            _POP_.start_popshop_transaction_signal.emit(str(get_popshop_transaction))
        else:
            _POP_.start_popshop_transaction_signal.emit("ERROR")
            _LOG_.warning(('[ERROR] popshop_transaction : ', str(express_message)))
    except Exception as e:
        _POP_.start_popshop_transaction_signal.emit("ERROR")
        _LOG_.warning(('[ERROR] popshop_transaction : ', str(e)))


def get_popsend_button():
    ClientTools.get_global_pool().apply_async(popsend_button)


def popsend_button():
    try:
        post_popsend_button = dict()
        get_popsend_button = dict()
        post_popsend_button["token"] = global_token
        end_url = global_url + "balance/topupinfo"
        _LOG_.debug(('url_get_popsend_button : ', end_url))
        express_message, status_code = global_post(end_url, post_popsend_button)
        if status_code == 200 and express_message["response"]["message"] == "ok":
            get_popsend_button["data_button"] = express_message["data"]
            get_popsend_button["total_button"] = len(express_message["data"])
            _POP_.get_popsend_button_signal.emit(str(json.dumps(get_popsend_button)))
        else:
            _POP_.get_popsend_button_signal.emit("ERROR")
            _LOG_.warning(('[ERROR] popsend_button: ', str(express_message)))
    except Exception as e:
        _POP_.get_popsend_button_signal.emit("ERROR")
        _LOG_.warning(('[ERROR] popsend_button : ', str(e)))


def sepulsa_trx_check(trx_id):
    ClientTools.get_global_pool().apply_async(get_trx_record, (trx_id,))


def get_trx_record(trx_id):
    try:
        post_trx_record = dict()
        post_trx_record["token"] = global_token
        post_trx_record["invoice_id"] = str(trx_id)
        end_url = global_url + "service/sepulsa/getTransaction"
        _LOG_.debug(('url_get_trx_record : ', end_url))
        express_message, status_code = global_post(end_url, post_trx_record)
        if status_code == "200":
            get_trx_data = express_message["data"][0]
            _POP_.sepulsa_trx_check_signal.emit(str(json.dumps(get_trx_data)))
            _LOG_.info(str(json.dumps(express_message)))
        elif status_code == "400":
            get_trx_data = str(express_message["response"]["message"])
            _POP_.sepulsa_trx_check_signal.emit(get_trx_data)
        else:
            _POP_.sepulsa_trx_check_signal.emit("ERROR")
            _LOG_.warning('url_get_trx_record result : ERROR')
    except Exception as e:
        _POP_.sepulsa_trx_check_signal.emit("ERROR")
        _LOG_.warning(('[ERROR] get_trx_record : ', str(e)))


def start_deposit_express():
    ClientTools.get_global_pool().apply_async(store_deposit_express)


def store_direct_popsafe(param):
    _LOG_.debug(("store_direct_popsafe", param))
    ClientTools.get_global_pool().apply_async(store_deposit_express, (param,))


def store_deposit_express(extra_param=None):
    send_signal = True
    try:
        box_result = BoxService.get_box()
        if not box_result:
            ExpressService._EXPRESS_.store_express_signal.emit('Error')
            return
        mouth_result = BoxService.mouth
        if extra_param is None:
            overdue_time = ExpressService.get_overdue_timestamp(box_result)
            popdeposit_express = ExpressService.customer_store_express
        else:
            overdue_time = get_overdue_hours(24)
            popdeposit_express = json.loads(extra_param)
            popdeposit_express['customerStoreNumber'] = 'APXID-'+random_chars(box_result['validateType'], 5)
            # popdeposit_express['customerStoreNumber'] = 'PDSL'+random_chars(box_result['validateType'], 5)
            popdeposit_express['logisticsCompany'] = {'id': '161e5ed1140f11e5bdbd0242ac110001'}
        # operator_result = CompanyService.get_company_by_id(box_result['operator_id'])
        # if not operator_result:
        #     ExpressService._EXPRESS_.store_express_signal.emit('Error')
        #     return
        express_param = {'expressNumber': popdeposit_express['customerStoreNumber'],
                         'expressType': 'COURIER_STORE',
                         'overdueTime': overdue_time,
                         'status': 'IN_STORE',
                         'storeTime': ClientTools.now(),
                         'syncFlag': 0,
                         'takeUserPhoneNumber': popdeposit_express['takeUserPhoneNumber'],
                         'validateCode': ExpressService.random_validate(box_result['validateType']),
                         'version': 0,
                         'box_id': box_result['id'],
                         'logisticsCompany_id': popdeposit_express['logisticsCompany']['id'],
                        #  'logisticsCompany_id': '161e5ed1140f11e5bdbd0242ac110001',
                         'mouth_id': mouth_result['id'],
                         'operator_id': box_result['operator_id'],
                         'storeUser_id': '402880825dbcd4c3015de54d98c5518e',
                         'groupName': 'POPDEPOSIT',
                         'id': ClientTools.get_uuid()}

        if extra_param is not None:
            express_param['transactionRecord'] = popdeposit_express['transactionRecord']
            express_param['paymentMethod'] = popdeposit_express['paymentMethod']
            express_param['paymentAmount'] = popdeposit_express['paymentAmount']
            express_param['lockerSize'] = popdeposit_express['lockerSize']
            express_param['lockerNo'] = str(mouth_result['number'])

        # while True:
        #     _LOG_.info(('[ATTEMPT] store_popsafe_db', express_param['expressNumber']))
        #     store_popsafe_db = ExpressDao.save_express(express_param)
        #     if store_popsafe_db is True:
        #         _LOG_.info(('[SUCCESS] store_popsafe_db', express_param['expressNumber']))
        #         break
        ExpressDao.save_express(express_param)
        mouth_param = {'id': express_param['mouth_id'],
                       'express_id': express_param['id'],
                       'status': 'USED'}
        BoxService.use_mouth(mouth_param)
        express_param['box'] = {'id': express_param['box_id']}
        express_param.pop('box_id')
        express_param['logisticsCompany'] = {'id': express_param['logisticsCompany_id']}
        express_param.pop('logisticsCompany_id')
        express_param['mouth'] = {'id': express_param['mouth_id']}
        express_param.pop('mouth_id')
        express_param['operator'] = {'id': express_param['operator_id']}
        express_param.pop('operator_id')
        express_param['storeUser'] = {'id': express_param['storeUser_id']}
        express_param.pop('storeUser_id')
        if send_signal is True:
            send_signal = False
            ExpressService._EXPRESS_.store_express_signal.emit('Success')
            _LOG_.info(('[SUCCESS] param_express', express_param))

        message, status_code = HttpClient.post_message('express/staffStoreExpress', express_param)
        if status_code == 200 and message['id'] == express_param['id']:
            ExpressDao.mark_sync_success(express_param)
    except Exception as e:
        _LOG_.warning(str(e))
        ExpressService._EXPRESS_.store_express_signal.emit('Error')


def delete_custore_by_id(id):
    try:
        header = {"usertoken": "142487bdbb774060b4711debe39577f4"}
        url = Configurator.get_value("ClientInfo", "serveraddress")
        url_d = url + "express/deleteImportedExpress/" + str(id)
        # logger.debug("URL delete CustomerStoreNumber : " + url_d)
        d = requests.post(url_d, headers=header, json=None, timeout=50)
        d_resp = d.json()
        _LOG_.info("Deletion of CustomerStoreNumber " + str(id) + " : " + str(d_resp))
    except Exception as e:
        _LOG_.warning(("[ERROR] delete_custore_by_id : ", str(e)))


def get_comp_stats():
    import wmi
    global comp_stats
    comp_stats = dict()
    c = wmi.WMI()

    try:
        disk_space = []
        for d in c.Win32_LogicalDisk(Caption="D:"):
            disk_space.append(int(d.FreeSpace.strip()) / 1024 / 1024)
            comp_stats["disk_space"] = "%.2f" % disk_space[0]
    except Exception as e:
        # logger.error("Error in getting Disk Space : ", e)
        comp_stats["disk_space"] = "%.2f" % -1

    try:
        memory_space = []
        for e in c.Win32_OperatingSystem():
            memory_space.append(int(e.FreePhysicalMemory.strip()) / 1024)
            comp_stats["memory_space"] = "%.2f" % memory_space[0]
    except Exception as e:
        # logger.error("Error in getting Memory Space : ", e)
        comp_stats["memory_space"] = "%.2f" % -1

    try:
        cpu_temp = []
        f = wmi.WMI(namespace="root\wmi")
        common = 30
        variance = random.uniform(0.09, 1.09)
        for g in f.MSAcpi_ThermalZoneTemperature():
            cpu_temp.append((int(g.CurrentTemperature) / 10) - 273.15 + variance)
            comp_stats["cpu_temp"] = "%.2f" % cpu_temp[0]
    except Exception as e:
        # logger.error("Error in getting CPU Temperature : ", e)
        comp_stats["cpu_temp"] = "%.2f" % (common - variance)

    # try:
    #    vss_path = 'D://VSS/ads/'
    #    comp_stats["tvc_list"] = [v for v in os.listdir(vss_path) if os.path.isfile(v)]
    # except Exception as e:
    #    logger.error("Error in getting list of TVC Videos : ", e)
    #    comp_stats["tvc_list"] = "undefined"
    # _LOG_.info("STATUS SUHU : " + str(comp_stats))
    # push_status(comp_stats)
    return comp_stats



def box_start_migrate():
    ClientTools.get_global_pool().apply_async(start_migrate)


def start_migrate():
    if server in Configurator.get_value('ClientInfo', 'serveraddress'):
        _POP_.box_start_migrate_signal.emit('NO NEED')
        return
    else:
        new_url = 'http://pr0x.popbox.asia/'
        new_db = 'popboxclient.db'
        try:
            param_ = BoxService.box_config
            if param_ is not None or param_ != '':
                response, status_code = global_post(new_url + 'task/start/migrate', param_)
                if status_code == 200 and response is not None:
                    path_db = sys.path[0] + '/database/'
                    try:
                        resync_table('UPDATE express SET syncFlag =:syncFlag WHERE id not null', {'syncFlag': 0})
                    except Exception as e:
                        _LOG_.debug(("Force resync_table for : ", e))
                    force_rename(path_db + 'pakpobox.db', path_db + new_db)
                    Configurator.set_value('ClientInfo', 'serveraddress', new_url)
                    Configurator.set_value('ClientInfo', 'dbname', new_db)
                    _POP_.box_start_migrate_signal.emit('SUCCESS')
                    _LOG_.info(("Locker Migration SUCCESS : ", status_code))
                else:
                    _POP_.box_start_migrate_signal.emit('FAILED')
                    _LOG_.warning(("Locker Migration FAILED : ", status_code))
            else:
                _POP_.box_start_migrate_signal.emit('ERROR')
                _LOG_.warning("Locker Migration ERROR..!")
        except Exception as e:
            _LOG_.warning(("start_migrate FAILED : ", e))


def force_resync_all():
    # 7 Days Data For Roll-Back Sync
    rangeTime = (int(time.time()) - (7 * 604800)) * 1000
    try:
        resync_table('UPDATE express SET syncFlag =:syncFlag WHERE id not null AND storeTime >:limitTime',
                     {'syncFlag': 0, 'limitTime': rangeTime})
    except Exception as e:
        _LOG_.debug(("Force resync_table express exception : ", e))
    try:
        resync_table('UPDATE mouth SET syncFlag =:syncFlag WHERE id not null', {'syncFlag': 0})
    except Exception as e:
        _LOG_.debug(("Force resync_table mouth exception : ", e))


def start_get_ads_images(zpath):
    ClientTools.get_global_pool().apply_async(get_ads_images, (zpath,))


def get_ads_images(zpath):
    get_file_dir(directory_path=zpath)


def start_delete_express_by_range(limit, duration):
    ClientTools.get_global_pool().apply_async(delete_express_by_range, (limit, duration,))


def delete_express_by_range(limit, duration):
    if limit is None or duration is None:
        return
    ExpressDao.delete_express(limit=int(limit), duration=duration)
    _LOG_.info(('Delete Express If More Than ', str(limit), str(duration)))


def execute_command(command):
    try:
        os.system(command)
        _LOG_.info(('Remote Executing : ', command))
    except Exception as e:
        _LOG_.warning(('Remote Executing : ', str(e)))


def start_post_gui_info():
    ClientTools.get_global_pool().apply_async(post_gui_info)


def post_gui_info():
    global version
    try:
        message, status_code = HttpClient.post_message('box/guiInfo', {"gui_version": str(version)})
        _LOG_.info(('post_gui_info : ', str(status_code), str(message)))
    except Exception as e:
        _LOG_.warning(('[ERROR] post_gui_info: ', str(e)))


def start_post_subscribe_data(name, cust_data):
    ClientTools.get_global_pool().apply_async(subscribe_data, (name, cust_data,))


def subscribe_data(name, cust_data):
    try:
        post_subscribe_data = dict()
        if "||" in cust_data:
            cust_email = cust_data.split("||")[0]
            cust_phone = cust_data.split("||")[1]
            post_subscribe_data["phone"] = cust_phone
        else:
            cust_email = cust_data
        post_subscribe_data["token"] = global_token
        post_subscribe_data["name"] = str(name)
        post_subscribe_data["email"] = cust_email
        post_subscribe_data["register_place"] = locker_info["name"]
        query_url = global_url + 'member/newsletter'
        _LOG_.info(('subscribe_data url : ' + query_url + ", cust_data : " + cust_data))
        express_message, status_code = global_post(query_url, post_subscribe_data)
        if status_code == "200":
            result = express_message["response"]["message"]
            _POP_.start_post_subscribe_data_signal.emit(result)
        else:
            _POP_.start_post_subscribe_data_signal.emit("ERROR")
            _LOG_.warning(("start post subscribe_data result : ", status_code))
    except Exception as e:
        _POP_.start_post_subscribe_data_signal.emit("ERROR")
        _LOG_.warning(("[ERROR] subscribe_data : ", str(e)))


def start_get_file_dir(dir_):
    ClientTools.get_global_pool().apply_async(get_file_dir, (dir_,))


def get_file_dir(directory_path):
    if directory_path == "" or directory_path is None:
        _POP_.start_get_file_dir_signal.emit("ERROR")
        return
    if "video" in str(directory_path):
        ext_files = ('.mp4', '.mov', '.avi', '.mpg', '.mpeg')
    elif "image" in str(directory_path):
        ext_files = ('.png', '.jpeg', '.jpg')
    elif "music" in str(directory_path):
        ext_files = ('.mp3', '.ogg', '.wav')
    else:
        ext_files = ('.png', '.jpeg', '.jpg')

    try:
        _dir_ = directory_path.replace(".", "")
        _LOG_.info(("getting files from : ", _dir_))
        _tvclist = [xyz for xyz in os.listdir(sys.path[0] + _dir_) if xyz.endswith(ext_files)]
        post_tvclist(json.dumps(_tvclist))
        files = {"output": _tvclist}
        _POP_.start_get_file_dir_signal.emit(json.dumps(files))
    except Exception as e:
        _POP_.start_get_file_dir_signal.emit("ERROR")
        _LOG_.warning(("[ERROR] get_file_dir : ", str(e)))


# TODO Activate this function if required only, (missing imutils module issue)
# def start_get_detection(method):
#     if Detector.START_DETECTION is False:
#         ClientTools.get_global_pool().apply_async(get_detection, (method,))
#
#
# def get_detection(method):
#     if method is None or method == "all":
#         Detector.setting["face_detector"] = True
#         Detector.setting["motion_detector"] = True
#     elif method == "face":
#         Detector.setting["face_detector"] = True
#         Detector.setting["motion_detector"] = False
#     elif method == "motion":
#         Detector.setting["face_detector"] = False
#         Detector.setting["motion_detector"] = True
#
#     try:
#         Detector.setting["status"] = True if (Configurator.get_value('panel', 'detector') == "enabled") else False
#         Detector.setting["prompt"] = True
#         Detector.setting["printable"] = False
#         print("Detector Setting : " + str(Detector.setting))
#         status, response = Detector.start_detection()
#         # print("Detector Result : ", str(response))
#         if status is True and 'error' not in response:
#             pop_signal_handler.start_get_detection_signal.emit(json.dumps(response))
#         else:
#             pop_signal_handler.start_get_detection_signal.emit("ERROR")
#     except Exception as e:
#         pop_signal_handler.start_get_detection_signal.emit("FAILED")
#         logger.warning(("get_detection ERROR : ", e))
#


payment_token = "PHVVH4KXB9QJUK6NZ6EKGQNFQOHLVGVFX87RUYBV"
payment_url = "https://payment.popbox.asia/api"
# payment_url = "http://172.20.200.69:8000/api"
# payment_token = "ZNZNEONSYG6SUAFEPYNACR1H2AMJ29DS7D3630AH"

def define_dev_payment():
    global payment_token, payment_url
    if Configurator.get_value('popbox', 'dummy^transaction') == '1':
        payment_url = "http://paymentdev.popbox.asia/api"
        payment_token = "ZNZNEONSYG6SUAFEPYNACR1H2AMJ29DS7D3630AH"


define_dev_payment()


def get_payment_session(clientid="001"):
    try:
        param = {
            "token": payment_token,
            "client_id": clientid
        }
        checkConnections = is_internet_available()

        if checkConnections is True:
            url = payment_url + "/v1/createSession"
            _LOG_.debug(('get_payment_session : ', url))
            response, status = global_post(url, param)
            _LOG_.debug(('get_payment_session_reponse : ', response))
            if status == 200 and response['data'][0]['session_id'] is not None:
                return response['data'][0]['session_id']
            else:
                _LOG_.debug(('get_payment_session_offline : ', response))
                return None
        else:
            _LOG_.debug(('get_payment_session_offline : ', response))
            return None
    except Exception as e:
        _LOG_.warning(('[ERROR] get_payment_session : ', str(e)))
        return None


def get_trans_id():
    return "LKR" + time.strftime("%y%m%d%H%M") + ClientTools.get_random_chars(length=3)


PAYMENT_ID_GLOBAL = ""
TRANS_ID_GLOBAL = ""


def start_create_payment_yap(amount, cust_name, item, locker_name):
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name,))


def start_create_payment_tcash(amount, cust_name, item, locker_name):
    _channel = 'TCASH'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel,))


def start_create_payment_ottoqr(amount, cust_name, item, locker_name):
    _channel = 'OTTO-QR'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel,))


def start_create_payment_gopay(amount, cust_name, item, locker_name):
    _channel = 'MID-GOPAY'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel,))

def start_create_payment_emoney(amount, cust_name, item, locker_name, data_emoney):
    _channel = 'MANDIRI-EMONEY'
    ClientTools.get_global_pool().apply_async(create_payment_global, (amount, cust_name, item, locker_name, _channel, data_emoney, ))


def create_payment_global(amount, cust_name, item, locker_name, channel='BNI-YAP', emoney=""):
    global PAYMENT_ID_GLOBAL, TRANS_ID_GLOBAL
    # _LOG_.info(("data create payment global", amount, cust_name, item, locker_name, channel))
    try:
        if amount is None or cust_name is None:
            _POP_.start_create_payment_signal.emit("ERROR")
            return
        session_id = get_payment_session()
        if channel == 'MANDIRI-EMONEY':
            if session_id is None:
                pass
        else:
            if session_id is None:
                _POP_.start_create_payment_signal.emit("ERROR")
                return

        paymentRequest = True    
        trans_id = get_trans_id()
        TRANS_ID_GLOBAL = trans_id
        cust_name = cust_name.split('||')
        param = {
            "token": payment_token,
            "session_id": session_id,
            "method_code": channel,
            "amount": int(amount),
            "billing_type": "fixed",
            "transaction_id": trans_id,
            "customer_name": cust_name[0],
            "customer_phone": cust_name[1],
            "customer_email": cust_name[2],
            "description": item if item is not None else "N/A",
            "location_type": "locker",
            "location_name": locker_name if locker_name is not None else locker_info['name'],
            "data_emoney": emoney
        }
        _LOG_.info(("[PAYMENT_PARAM] create_payment_global : ", json.dumps(param)))        
        if session_id is None:
            paymentRequest = False
        else:
            url = payment_url + "/v1/payment/createPayment"
            response, status = global_post(url, param)

        if channel == 'MANDIRI-EMONEY':
            if paymentRequest is not False:
                try:
                    checkResponse = True
                    response['response']['code']
                except Exception as e:
                    checkResponse = False
                    
                _LOG_.debug(("[CHECKING_RESPONSE] : ", response))   
                if checkResponse is not False:
                    if status == 200 and (response['response']['code'] == 200 or len(response['data']) > 0):
                        PAYMENT_ID_GLOBAL = response['data'][0]['payment_id']
                        _POP_.start_create_payment_signal.emit(json.dumps(response['data'][0]))
                    elif status == 500 and response['response']['code'] == 500:
                        _LOG_.debug(("[PAYMENT_RESPONSE] : FALSE"))   
                        response = {
                            "token": payment_token,
                            "session_id": "",
                            "method_code": channel,
                            "amount": int(amount),
                            "billing_type": "fixed",
                            "transaction_id": trans_id,
                            "customer_name": cust_name[0],
                            "customer_phone": cust_name[1],
                            "customer_email": cust_name[2],
                            "description": item if item is not None else "N/A",
                            "location_type": "locker",
                            "location_name": locker_name if locker_name is not None else locker_info['name'],
                            "data_emoney": emoney,
                            "payment_offline": "true"
                        }
                        _POP_.start_create_payment_signal.emit(json.dumps(response))

                    else:
                        _POP_.start_create_payment_signal.emit(json.dumps(response['data'][0]))

                elif response['statusCode'] == -1:
                    _LOG_.debug(("[PAYMENT_RESPONSE] : FALSE"))   
                    response = {
                        "token": payment_token,
                        "session_id": "",
                        "method_code": channel,
                        "amount": int(amount),
                        "billing_type": "fixed",
                        "transaction_id": trans_id,
                        "customer_name": cust_name[0],
                        "customer_phone": cust_name[1],
                        "customer_email": cust_name[2],
                        "description": item if item is not None else "N/A",
                        "location_type": "locker",
                        "location_name": locker_name if locker_name is not None else locker_info['name'],
                        "data_emoney": emoney,
                        "payment_offline": "true"
                    }
                    _LOG_.warning(("[PAYMENT_ONLINE_OFFLINE] create_payment_global : ", response))
                    _POP_.start_create_payment_signal.emit(json.dumps(response))
            else:
                response = {
                    "token": payment_token,
                    "session_id": "",
                    "method_code": channel,
                    "amount": int(amount),
                    "billing_type": "fixed",
                    "transaction_id": trans_id,
                    "customer_name": cust_name[0],
                    "customer_phone": cust_name[1],
                    "customer_email": cust_name[2],
                    "description": item if item is not None else "N/A",
                    "location_type": "locker",
                    "location_name": locker_name if locker_name is not None else locker_info['name'],
                    "data_emoney": emoney,
                    "payment_offline": "true"
                }
                _LOG_.warning(("[PAYMENT_OFFLINE] create_payment_global : ", response))
                _POP_.start_create_payment_signal.emit(json.dumps(response))
        else:
            if status == 200 and (response['response']['code'] == 200 or len(response['data']) > 0):
                PAYMENT_ID_GLOBAL = response['data'][0]['payment_id']
                # payment_id = qr_url.replace('http://', '').replace('.png', '').split('/')[-1]
                _POP_.start_create_payment_signal.emit(json.dumps(response['data'][0]))
            else:
                _POP_.start_create_payment_signal.emit("ERROR")
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] create_payment_global : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _POP_.start_create_payment_signal.emit("FAILED")


def start_check_trans_global():
    ClientTools.get_global_pool().apply_async(check_trans_global)


def check_trans_global():
    global TRANS_ID_GLOBAL, PAYMENT_ID_GLOBAL
    if DUMMY_RESPONSE is True:
        dummy_response = {
            'status': 'PAID',
            'transaction_id': TRANS_ID_GLOBAL,
            'payment_id': PAYMENT_ID_GLOBAL,
            'expired_datetime': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'url': 'http://dummy.url.img'
        }
        _POP_.start_check_trans_global_signal.emit(json.dumps(dummy_response))
        return
    try:
        param = {
            "token": payment_token,
            "session_id": get_payment_session(),
            "trans_id": TRANS_ID_GLOBAL,
            "payment_id": PAYMENT_ID_GLOBAL
        }
        url = payment_url + "/v1/payment/inquiryPayment"
        response, status = global_post(url, param)
        if status == 200 and len(response['data']) > 0:
            _POP_.start_check_trans_global_signal.emit(json.dumps(response['data'][0]))
        else:
            _POP_.start_check_trans_global_signal.emit("ERROR")
    except Exception as e:
        _POP_.start_check_trans_global_signal.emit("FAILED")
        _LOG_.warning(("[ERROR] check_trans_global: ", str(e)))


def post_tvclist(list_):
    try:
        HttpClient.post_message('box/tvcList', {"tvclist": list_})
        # logger.info(('post_tvclist RESULT : ', response))
    except Exception as e:
        _LOG_.warning(("[ERROR] post_tvclist: ", str(e)))


def start_post_tvclog(media):
    ClientTools.get_global_pool().apply_async(post_tvclog, (media,))


def post_tvclog(media):
    try:
        param = {
            "filename": media,
            "country": "ID",
            "playtime": time.strftime("%Y-%m-%d %H")
        }
        HttpClient.post_message('box/tvcLog', param)
        # logger.info(("post_tvclog RESULT : ", status, response))
    except Exception as e:
        _LOG_.warning(("[ERROR] post_tvclog : ", str(e)))


def start_post_activity(activity):
    ClientTools.get_global_pool().apply_async(post_activity, (activity,))


def post_activity(activity):
    try:
        param = {
            "activity": activity,
            "country": "ID",
            "recordtime": time.strftime("%Y-%m-%d %H")
        }
        HttpClient.post_message('box/activityLog', param)
        # logger.info(("post_activity RESULT : ", status, response))
    except Exception as e:
        _LOG_.warning(("[ERROR] post_activity : ", str(e)))


def start_extend_express(express_no):
    ClientTools.get_global_pool().apply_async(extend_express, (express_no,))


def extend_express(express_no):
    if express_no is None: 
        return
    try:
        __express_no = json.loads(express_no)
        param = {
            "expressNumber": __express_no["expressNumber"],
            "syncFlag": 0,
            "overdueTime": get_overdue_hours(24)
        }
        ExpressDao.local_extend_express(param)
        _LOG_.debug(("extend_express", param))
        send_param = {
            "token": "4EI5COIOWBVPF6JVXTFPP4TMC8LZYPZ5J5HYUA1SKXEQQXA10Q",
            "session_id": __express_no["userSession"],
            "invoice_id": __express_no["expressNumber"],
            "remarks": __express_no["trxRemarks"]
            }
        global_post("http://popsendv2.popbox.asia/popsafe/extend", send_param)
    except Exception as e:
        _LOG_.warning(str(e))

def start_time_overdue():
    ClientTools.get_global_pool().apply_async(get_time_overdue)

def get_time_overdue():
    time_overdue = Configurator.get_value('OverdueTime', 'take^overdue')
    _POP_.start_time_overdue_result_signal.emit(str(time_overdue))

def start_hour_overdue():
    ClientTools.get_global_pool().apply_async(get_hour_overdue)

def get_hour_overdue():
    hour_overdue = Configurator.get_value('OverdueTime', 'hours')
    hour_overdue = int(float(hour_overdue))
    _POP_.start_hour_overdue_result_signal.emit(str(hour_overdue))

def check_connection_time():
    ClientTools.get_global_pool().apply_async(get_time_value)

def get_time_value():
    global start_flag
    schedule.every(1).minutes.do(postwrite_datetime)
    schedule.every(10).minutes.do(postget_internetStatus)
    schedule.every(15).minutes.do(delete_backupdtime)
    #schedule.every().hour.do(postget_internetStatus)
    while start_flag:
        schedule.run_pending()
        time.sleep(1)

def postwrite_datetime():
    # if not(os.path.exists('asyn_time.txt')):
    #     mfile = open('asyn_time.txt', 'a')
    fileHandle = open ('asyn_time.txt',"r" ) # Membaca DataBackup Terakhir
    lineList = fileHandle.readlines()
    fileHandle.close()
    data_teks = lineList[-1]
    post_data_teks = str (data_teks)
    now= datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    #Melakukan writes datatime ke file backup
    year_sys=now[:4]
    year_backup=post_data_teks[:4]
    month_sys=now[5:7]
    month_backup=post_data_teks[5:7]
    _LOG_.info(('Reading file from BackUp : ', str(post_data_teks), 'LocalTime: ', str(now), 'Tahun Local ', str(year_sys), 'Tahun Backup', str(year_backup), 'Bulan Local', str(month_sys), 'Bulan BackUp', str(month_backup)))
    if(year_sys == year_backup and month_sys == month_backup):
        myFile = open('asyn_time.txt', 'a')
        _LOG_.info(('Write datetime file backup is success : ', str(now)))
        myFile.write('\n'+str(now))        

def delete_backupdtime():
    with open("asyn_time.txt", "r+") as f:
        lines = f.readlines()
        r = len(lines) - 1
        for i in range (0, r):
            del lines[0]  # use linenum - 1 if linenum starts from 1
        f.seek(0)
        f.truncate()
        f.writelines(lines)

def is_internet_available():
    global connectionStatus    
    try:
        urlopen('http://pr0x.popbox.asia/', timeout=1)
        connectionStatus = True
        time.sleep(1)
        return connectionStatus
    except:
        connectionStatus =  False
        time.sleep(1)
        return connectionStatus

def internet_status():
    counter = 0
    for i in range(5):
        if is_internet_available() == True:
            counter += 1
        elif is_internet_available() == False:  
            counter -= 1    
    if counter >=3:
        return True
    else:
        return False

def postget_internetStatus():
    statusConnection = internet_status()
    print("status internet: " + str(statusConnection))
    _LOG_.info('Internet Status is checking... ')
    if statusConnection == True:
        #init_time('/neutronSync.bat')
        _LOG_.info(('Internet Status: ', str(statusConnection),'So Running NTP Server' ))
        _LOG_.info('Run NTP Server get time')
        get_ntpServer_datetime()
        today= datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        myFile = open('asyn_time.txt', 'a')
        myFile.write('\n'+str(today))
    else:
        _LOG_.info(('Internet Status: ', str(statusConnection), 'So Change Datetime from File Backup'))
        change_dateBackup() #Mengganti datetime system dengan DataBackUp

def get_ntpServer_datetime():
    dt_str = None
    ntp_servers = ['ntp.iitb.ac.in', 'pool.ntp.org', 'time.windows.com', 'ntp.nict.jp']
    for server in ntp_servers:
        try:
            client_add = ntplib.NTPClient()
            response = client_add.request(server, version=3)
            dt_ = datetime.datetime.fromtimestamp( response.tx_time )
            dt_str = str (dt_.strftime( '%Y-%m-%d %H:%M:%S'))
        except Exception as e:
            _LOG_.info(('Error message: ' + str(e)))

    ntp_temp = converter_strdtime_listint(dt_str)
    conv_ntp_zone = converter_dateIna(ntp_temp)
    write_systemMachine(conv_ntp_zone)
    _LOG_.info(('Change system time from NTP Server is Success : ', str(dt_str)))

def change_dateBackup():
    fileHandle = open ('asyn_time.txt',"r" )
    lineList = fileHandle.readlines()
    fileHandle.close()
    data_teks = lineList[-1]
    post_data_teks = str (data_teks)
    str_time = time.strptime(post_data_teks,"%Y-%m-%d %H:%M:%S")
    time_stmp_converter = time.mktime(str_time)
    time_stamp_delay = time_stmp_converter + 74
    reconvert_timestamp = datetime.datetime.fromtimestamp(time_stamp_delay)
    post_temp = converter_strdtime_listint(str(reconvert_timestamp))
    conv_zone = converter_dateIna(post_temp)
    write_systemMachine(conv_zone)
    _LOG_.info(('Change system time from local file backup is success : ', str(post_data_teks)))        

def first_internetchecking():
    statusConnection = internet_status()
    print("status internet: " + str(statusConnection))
    _LOG_.info('Checking Internet Status for the first time after reboot Gui')
    openFile = open('asyn_time.txt', 'a')
    if statusConnection == True:
        _LOG_.info('Run NTP Server getting time from Internet')
        get_ntpServer_datetime()
        tdy= datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        openFile.write('\n'+str(tdy))
    else:
        fileHandle = open ('asyn_time.txt',"r" )
        lineList = fileHandle.readlines()
        fileHandle.close()
        data_teks = lineList[-1]
        post_data_teks = str (data_teks)
        post_temp = converter_strdtime_listint(post_data_teks)
        conv_zone = converter_dateIna(post_temp)
        write_systemMachine(conv_zone)
        _LOG_.info(('Change system time from local file backup is success : ', str(post_data_teks)))


def converter_strdtime_listint(data_convert):
    time_tuple = time.strptime(str(data_convert), "%Y-%m-%d %H:%M:%S")
    time_str= list(time_tuple)
    time_int = list (map (int, time_str))
    temp = [0,0]
    temp[0:1] =  time_int[0:7]
    temp[2] = 0
    temp[3:7]= time_int[2:6]
    return temp

def converter_dateIna(data):
    if Configurator.get_value('timezone', 'timezone') == "WIB":
        convert = data
        if ((data[4] >= 0) and (data[4] < 7 )):
            convert[3] = data[3] - 1
            convert[4] = data[4] + 17
        elif ((data[4] >= 7) and (data[4] < 24 )):
            convert[4] = data[4]-7
        return convert
    elif Configurator.get_value('timezone', 'timezone') == "WITA":
        convert = data
        if ((data[4] >= 0) and (data[4] < 8 )):
            convert[3] = data[3] - 1
            convert[4] = data[4] + 16
        elif ((data[4] >= 8) and (data[4] < 24 )):
            convert[4] = data[4]-8
        return convert
    elif Configurator.get_value('timezone', 'timezone') == "WIT":
        convert = data
        if ((data[4] >= 0) and (data[4] < 9 )):
            convert[3] = data[3] - 1
            convert[4] = data[4] + 15
        elif ((data[4] >= 9) and (data[4] < 24 )):
            convert[4] = data[4]-9
        return convert 

def write_systemMachine(list_temp):
    tup =  tuple(map(int, list_temp))
    win32api.SetSystemTime(tup[0], tup[1], tup[2], tup[3], tup[4], tup[5], tup[6], tup[7])

# def update_status_locker():
#     ClientTools.get_global_pool().apply_async(get_status_locker)

# def get_status_locker():
#     schedule.every(1).seconds.do(push_status)
#     while True:
#         schedule.run_pending()
#         time.sleep(1)

# def push_status():
#     # mqtt_host = "mqtt.popbox.asia"
#     # mqtt_port = 1883
#     mqtt_host = "m16.cloudmqtt.com"
#     mqtt_port = 19606
#     mqtt_timeout = 60
#     mqtt_topic = "locker/"
#     devicename = locker_name
#     # print (comp_stats)
#     mq_suhu = "0"
#     mq_hdd = "0"
#     mq_ram = "0"
#     # print("Locker Name : ",locker_name)
#     # if (comp_stats!=""):
#     #     mq_json = str(json.dumps(comp_stats))
#     #     mq_json = json.loads(mq_json)
#     #     mq_suhu = mq_json["cpu_temp"]
#     #     mq_hdd = mq_json["disk_space"]
#     #     mq_ram = mq_json["memory_space"]
#     # mqtt_value = str(json.dumps(com_status))
#     msg = mqtt_topic + devicename
#     # msg = mqtt_topic + 
#     # mqttc.subscribe("locker/#", 2) # <- pointless unless you include a subscribe callback

#     # _LOG_.info("STATUS SUHU MQTT : " + str(json.dumps(com_status)))
#     # mqttc = mqtt.Client()
#     mqttc = mqtt.Client("popbox-loker")
#     mqttc.username_pw_set("qqjisfgm", "yj3WmRcQovNp")
#     mqttc.connect(mqtt_host, mqtt_port, mqtt_timeout)
#     mqttc.loop_start()
#     mqttc.publish(msg + "/suhu", str(mq_suhu)+"^C", qos=0)
#     mqttc.publish(msg + "/hdd", str(mq_hdd), qos=0)
#     mqttc.publish(msg + "/ram", str(mq_ram), qos=0)


# def init_time(file):
#     if file is None:
#         return
#     try:
#         import platform
#         os_ver = platform.platform()
#         if 'Windows-7' in os_ver:
#             process = subprocess.Popen(sys.path[0] + file, shell=True, stdout=subprocess.PIPE)
#             output = process.communicate()[0].decode('utf-8').strip().split("\r\n")
#             _LOG_.info(('time initiation is success : ', str(output)))
#             time.sleep(5)
#         else:
#             _LOG_.debug(('time initiation is failed : ', str(os_ver)))

#     except Exception as e:
#         _LOG_.warning(('time initiation is failed : ', e))       