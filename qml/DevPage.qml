import QtQuick 2.4
import QtQuick.Controls 1.2

HideWindow{
        id:secret_code_check
        visible: true

        Image {
            id: img_lock_secret
            x: 434
            y: 214
            width: 200
            height: 200
            source: "img/otherImages/lock_opened.png"
        }

        Text {
            x: 92
            y:386
            width: 848
            height: 200
            text: qsTr("Hi, secret code is enterred!\n Select Function Below?")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:40
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:secret_sure_button
            x:550
            y:576
            show_text:qsTr("Quit GUI")
            // show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    //my_stack_view.push(manager_service_view)
                    slot_handler.start_explorer()
                    Qt.quit()
                }
                onEntered:{
                    secret_sure_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    secret_sure_button.show_source = "img/button/7.png"
                }
            }
        }

        OverTimeButton{
            id:secret_admin_button
            x:200
            y:576
            show_text:qsTr("Access Doors")
            // show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(manager_cabinet_view) //,{press_time:1})
                }
                onEntered:{
                    secret_sure_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    secret_sure_button.show_source = "img/button/7.png"
                }
            }
        }

        BackButton{
            id:secret_back_button
            x:130
            y:230
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    // courier_login_username.bool = true
                    // courier_login_psw.bool = false
                    // courier_login_username.show_text=""
                    // courier_login_psw.show_text=""
                    // main_page.enabled = true
                    // courier_login_button.enabled = true
                    secret_code_check.close()
                }
            }
        }

        Rectangle {
            id: camera_button
            x: 853
            y: 214
            width: 80
            height: 60
            color: "#00000000"
            anchors.topMargin: 0
            anchors.top: img_lock_secret.top
            Image {
                id: camera_image_button
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/item/camera.png"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(camera_capture_view)

                }
                onEntered:{
                    camera_image_button.source = "img/item/camera_red.png"
                }
                onExited:{
                    camera_image_button.source = "img/item/camera.png"
                }
            }
        }

        Rectangle {
            id: input_email
            x: 848
            y: 314
            width: 80
            height: 60
            color: "#00000000"
            anchors.topMargin: 70
            anchors.top: img_lock_secret.top
            Image {
                id: input_email_button
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/item/email.png"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(customer_input_email_view)

                }
                onEntered:{
                    input_email_button.source = "img/item/email-red.png"
                }
                onExited:{
                    input_email_button.source = "img/item/email.png"
                }
            }
        }

        Rectangle {
            id: test_settle_button
            x: 750
            y: 254
            width: 80
            height: 60
            color: "#00000000"
            anchors.topMargin: 0
            anchors.top: img_lock_secret.top
            Image {
                id: test_settle_button_image_button
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "img/courier08/08user.png"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log('test settle order')
                    slot_handler.start_push_data_settlement("TEST_SETTLEMENT")
                }
            }
        }

    }