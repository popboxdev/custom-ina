import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    id: baseAP
    property var press: '0'
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    property bool isNew: true
    property bool emoney_reader: false
    property bool gopay_reader: false
    property bool yap_reader: false
    property bool tcash_reader: false
    property bool ottopay_reader: false
    property var phone_number: ''
    property var locker_name: ''
    property var selectedPayment: ''
    property int timer_value: 60
    property int timer_animasi: 0

    /* Penambahan Baru*/
    property var products
    property var customers
    property var selected_locker: ""
    property var amount: ""
    property var item: ""
    property var customer_data: ""

    // property bool emoneyReady: false // lama
    // property bool bniYapReady: true  // lama
    property bool emoney: true
    property bool yap: true
    property bool dimo: false
    property bool popsend: false
    property bool tcash: true
    property bool gopay: true
    property bool otto_qr: true
    property bool isExtending: false
    property var extendData: undefined

    // penambahan riwandy
    property var product_class: undefined
    property var popsafe_param: undefined
    property var sepulsa_product: undefined
    property var dataOverdue: undefined

    property var customer_phone: ""
    property var imageLangkah: undefined

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_get_payment()
            console.log("product_class : " +  product_class)
           
            if (imageLangkah != undefined) {
                step_progress.source = imageLangkah
            }

            abc.counter = timer_value
            gif.counter = timer_animasi
            my_timer.restart()
            gif_timer.restart()
            press = "0"
            loadingGif.open()
            selectedPayment = ''
            
            if (dataOverdue == undefined) {
                define_trans_data(products, customers)
            }
            // press = '0';
            // abc.counter = timer_value;
            // my_timer.start();
            // selectedPayment = '';
            slot_handler.start_get_cod_status();
            slot_handler.start_get_locker_name();
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
            gif_timer.stop()
        }
    }

    Component.onCompleted: {
        // root.start_create_payment_result.connect(global_trans_result);
        root.start_get_cod_status_result.connect(cod_result);
        root.start_get_locker_name_result.connect(show_locker_name);
        root.get_payment_result.connect(payment_status);

    }

    Component.onDestruction: {
        // root.start_create_payment_result.disconnect(global_trans_result);
        root.start_get_cod_status_result.disconnect(cod_result);
        root.start_get_locker_name_result.disconnect(show_locker_name);
        root.get_payment_result.disconnect(payment_status);

    }

     function payment_status(r){
        console.log('payment_result', r)
        var p = JSON.parse(r)
        var d = p.data
        for (var i in d){
            if (d[i].status=='OPEN'){
                if (d[i].code=='BNI-YAP') {
                    yap = true;
                    yap_reader = true;
                }
                if (d[i].code=='MANDIRI-EMONEY') { 
                    // console.log('emoney......')
                    emoney = true;
                    emoney_reader = true;
                }
                if (d[i].code=='DIMO-QR') dimo = true;
                if (d[i].code=='POPSEND-WALLET') popsend = true;
                if (d[i].code=='OTTO-QR') { 
                    otto_qr = true;
                    ottopay_reader = true;
                }  
                if (d[i].code=='TCASH') {
                    tcash = true;
                    tcash_reader = true;
                } 
                if (d[i].code=='MID-GOPAY') { 
                    gopay = true;
                    gopay_reader = true;
                }
            }
        }

        // force open gopay_button --------
        // gopay = true
        // gopay_reader = true;
        // --------------------------------

        slot_handler.start_get_cod_status()
        // console.log("emoney status payment_status : ", emoney, emoney_reader)
        // loadingGif.close();
    }

    function show_locker_name(text){
        if(text == ""){
            return
        }
        locker_name = text;
    }

    function define_trans_data(objA, objB){
        var product = JSON.parse(objA)

        amount = product.total_amount
        item = product.sku + '|' + product.name + '|' + product.qty
        var customer = JSON.parse(objB)
        customer_data = customer.name + '||' + customer.phone + '||' + customer.email

        //TODO Define Popsafe Param
        popsafe_param = JSON.stringify({
            "takeUserPhoneNumber": customer.phone,
            "lockerSize": product.lockerSize,
            "transactionRecord": "",
            "paymentMethod": selectedPayment,
            "paymentAmount": amount
        })

        create_pretransaction(product_class)

    }

    function create_pretransaction(u){
        console.log('create_pretransaction : ', u)
        switch (u) {
        case 'ppob-starter-pack':
            var sim_number = JSON.parse(products).number
            // call slot_function for starter pack create transaction
            var param = JSON.stringify({ 'phone': sim_number, 'customer_phone': cust_phone,
                                           'customer_name': cust_name, 'customer_email': cust_email,
                                           'product_id': [prod_sku], 'addon_id': []
                                         })
            slot_handler.start_create_transaction('starter-pack', param)
            break;
        default:
            break;
        }

    }


/*
extendData = '{"id": "fc6852c09ba211e89941509a4ccea7f5", "mouth_id": "4028808359ff616b015a172842265752", "amount": 2000, "express_id": "fb4f280a9ba211e8be26509a4ccea7f5", "createTi
me": 1533798495000, "transactionType": "{\"express\": \"APXID-9M2VX\", \"pin_code\": \"E3S9VC\", \"result\": \"SUCCESS\", \"door_no\": 44, \"phone_number\": \"085710157057\", \"date\": \"2018-08-09
 22:08:13\"}", "paymentType": "E3S9VC"}'
*/

    function cod_result(result){
        console.log("cod_result is ", result)
        if(result=="enabled"){
            emoney = true;
        }
    }

    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    Rectangle{
        id: timer_set
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Image {
        id: step_progress
        anchors.top: parent.top
        anchors.topMargin: 115
        anchors.horizontalCenter: parent.horizontalCenter
        source: "img/apservice/langkah/2.png"
    }
    Text {
        id: text_select
        font.bold: true
        anchors.top: parent.top
        anchors.topMargin: 200
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Pilih Metode Pembayaran")
        font.family:"Microsoft YaHei"
        font.pixelSize: 35
        color:"#000000"
    }

     Rectangle{
        id: main_page
        color: "#dedede"

        Grid{
            id: buttons_group
            x: 12
            y: 247
            width: 700
            height: 500


            OtherServiceButton{
                id:mandiri_emoney
                visible: emoney
                width: 250
                height: 235
                scale: 0.8
                show_image:(emoney_reader==true) ? "img/payment/logo-baru/logopayment_emoney_active.png" : "img/payment/logo-baru/logopayment_emoney_inactive.png"
                // show_text:qsTr("E-Money")
                show_text_color:"#ab312e"

                MouseArea {
                    anchors.fill: parent
                    enabled: emoney_reader
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        my_timer.stop()
                        selectedPayment = 'EMONEY'
                        my_stack_view.push(confirm_payment_ap, {
                                selectedPayment: selectedPayment,
                                phone_number: phone_number, 
                                isExtending: isExtending,
                                extendData: extendData,
                                dataOverdue: dataOverdue, 
                                locker_name: locker_name,
                                popsafe_param: popsafe_param,
                                product_class: product_class,
                                provider: selectedPayment,
                                topPanelColor: topPanelColor,
                                imageLangkah: imageLangkah

                        });
                    }
                }
            }

            OtherServiceButton{
                id:bni_yap
                visible: yap
                width: 250
                height: 235
                scale: 0.8
                // show_image:"img/payment/bni-yap.png"
                show_image: (yap_reader==true) ? "img/payment/logo-baru/logopayment_yap_active.png" : "img/payment/logo-baru/logopayment_yap_inactive.png"
                // show_text:qsTr("BNI YAP!")
                show_text_color:"#ab312e" 

                MouseArea {
                    anchors.fill: parent
                    enabled: yap_reader
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        my_timer.stop()
                        // loadingGif.open()
                        selectedPayment = 'BNI-YAP'
                        // slot_handler.start_create_payment_yap(amount.toString(), customer_data, item, selected_locker)
                         my_stack_view.push(confirm_payment_ap, {
                                selectedPayment: selectedPayment,
                                phone_number: phone_number, 
                                isExtending: isExtending,
                                extendData: extendData,
                                dataOverdue: dataOverdue, 
                                locker_name: locker_name,
                                popsafe_param: popsafe_param,
                                product_class: product_class,
                                provider: selectedPayment,
                                topPanelColor: topPanelColor,
                                imageLangkah: imageLangkah
                        });
                    }
                }
            }

            OtherServiceButton {
                id: pay_by_qr
                visible: dimo
                width: 250
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                show_text: qsTr("PAY BY QR")
                show_image: "img/payment/dimo-paybyqr.png"
               

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        selectedPayment = 'DIMO-PAYBYQR'
                        my_timer.stop()
                        my_stack_view.push(qr_payment_channel, {products: products,
                                               provider: selectedPayment,
                                               product_class: product_class,
                                               customers: customers,
                                               popsafe_param: popsafe_param
                                           })
                    }
                }
            }

            OtherServiceButton {
                id: popsend_wallet
                visible: popsend
                width: 250
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                show_text: qsTr("PopBox Wallet")
                show_image: "img/payment/popbox-popsend.png"

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        // loadingGif.open()
                        selectedPayment = 'POPSEND-WALLET'
                        my_timer.stop()
                        my_stack_view.push(on_develop_view)
                    }
                }
            }

            OtherServiceButton {
                id: tcash_button
                visible: tcash
                width: 250
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                // show_text: qsTr("TCASH QR")
                show_image: (tcash_reader==true) ? "img/payment/logo-baru/logopayment_tcash_active.png" : "img/payment/logo-baru/logopayment_tcash_inactive.png"

                MouseArea {
                    anchors.fill: parent
                    enabled: tcash_reader
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        // loadingGif.open()
                        selectedPayment = 'TCASH'
                        my_timer.stop()
                        // slot_handler.start_create_payment_tcash(amount.toString(), customer_data, item, selected_locker)
                        my_stack_view.push(confirm_payment_ap, {
                                selectedPayment: selectedPayment,
                                phone_number: phone_number, 
                                isExtending: isExtending,
                                extendData: extendData,
                                dataOverdue: dataOverdue, 
                                locker_name: locker_name,
                                provider: selectedPayment,
                                popsafe_param: popsafe_param,
                                product_class: product_class,
                                topPanelColor: topPanelColor,
                                imageLangkah: imageLangkah

                        });
                    }
                }
            }

            OtherServiceButton {
                id: ottopay_button
                visible: otto_qr
                width: 250
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                // show_text: qsTr("OTTO QR")
                show_image: (ottopay_reader==true) ? "img/payment/logo-baru/logopayment_ottopay_active.png" : "img/payment/logo-baru/logopayment_ottopay_inactive.png"

                MouseArea {
                    anchors.fill: parent
                    enabled: ottopay_reader
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        // loadingGif.open()
                        selectedPayment = 'OTTO-QR'
                        my_timer.stop()
                        // slot_handler.start_create_payment_ottoqr(amount.toString(), customer_data, item, selected_locker)
                        my_stack_view.push(confirm_payment_ap, {
                                selectedPayment: selectedPayment,
                                phone_number: phone_number, 
                                isExtending: isExtending,
                                extendData: extendData,
                                dataOverdue: dataOverdue, 
                                locker_name: locker_name,
                                provider: selectedPayment,
                                popsafe_param: popsafe_param,
                                product_class: product_class,
                                topPanelColor: topPanelColor,
                                imageLangkah: imageLangkah

                        });
                    }
                }
            }

            OtherServiceButton {
                id: gopay_button
                visible: gopay
                width: 250
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                // show_text: qsTr("GOPAY")
                show_image: (gopay_reader==true) ? "img/payment/logo-baru/logopayment_gopay_active.png" : "img/payment/logo-baru/logopayment_gopay_inactive.png"

                MouseArea {
                    anchors.fill: parent
                    enabled: gopay_reader
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        // loadingGif.open()
                        selectedPayment = 'MID-GOPAY'
                        my_timer.stop()
                        my_stack_view.push(confirm_payment_ap, {
                                selectedPayment: selectedPayment,
                                phone_number: phone_number, 
                                isExtending: isExtending,
                                extendData: extendData,
                                dataOverdue: dataOverdue, 
                                locker_name: locker_name,
                                provider: selectedPayment,
                                popsafe_param: popsafe_param,
                                product_class: product_class,
                                topPanelColor: topPanelColor,
                                imageLangkah: imageLangkah
                        });
                    }
                }
            }

        }

        Text {
            id: powered_text
            x: 228
            y: 638
            width: 250
            height: 40
            color: "#ffffff"
            text: qsTr("Powered by :")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 30
            font.family: "Microsoft YaHei"
            visible: !isNew
        }

        Rectangle {
            id: rec_emoney
            x: 488
            y: 619
            width: 300
            height: 77
            color: "#ffffff"
            visible: !isNew

            Image {
                id: image_emoney
                x: 0
                y: 0
                width: 300
                height: 77
                source: "img/otherservice/logo-emoney300.png"
            }
        }

}
    

    LoadingView{
        id: loadingGif
        QtObject{
            id:gif
            property int counter
            Component.onCompleted:{
                gif.counter = timer_animasi
            }
        }
        Timer{
            id:gif_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                gif.counter -= 1
                if(gif.counter < 0){
                    gif_timer.stop()
                    loadingGif.close()
                }
            }
        }
    }

}
