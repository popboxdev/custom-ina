import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{

    width:230
    height:90
    color:"transparent"
    property var show_text:""
    property var show_source: "img/bottondown/1.png"
    property var show_icon:""
    property var show_text_color:"#BF2E26"


    Image{
        width:230
        height:90
        source:show_source
    }

    Image {

        x: 0
        y: 0
        width: 230
        height: 90
        source: show_icon
    }

    Text {

        x: 25
        y: 0
        width: 230
        height: 90
        color:show_text_color
        text:show_text
        textFormat: Text.PlainText
        font.family: "Microsoft YaHei"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 28
    }
}
