import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    property url source_img
    property int timer_value: 60
    property string activity: "CAMERA_CAPTURE_TEST"
    property var press: "0"
    property int timer_display: 5
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            photoPreviewAnimated.visible = true
            photoPreviewCaptured.visible = false
            press = "0"
            time_saving.counter = timer_display
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            show_timer_saving.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_capture_file_location_result.connect(handle_text)
    }

    Component.onDestruction: {
        root.start_get_capture_file_location_result.disconnect(handle_text)
    }

    Rectangle{
        id: rec_timer
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Text {
        id: text_notif_1
        x: 0
        y: 142
        width: 1024
        height: 80
        text: qsTr("Please place your parcel as instructed below")
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        font.pixelSize: 35
    }

    Rectangle {
        id: rec_preview
        x: 223
        y: 217
        width: 576
        height: 432
        color: "#ffffff"
        radius: 0
        border.width: 6
        border.color: "#ffffff"
        anchors.horizontalCenter: text_notif_1.horizontalCenter

        AnimatedImage{
            id: photoPreviewAnimated
            width: 576
            fillMode: Image.PreserveAspectFit
            anchors.fill: parent
            source: "img/item/capture_parcel.gif"
        }

        Image{
            id: photoPreviewCaptured
            width: 576
            sourceSize.height: 480
            sourceSize.width: 640
            fillMode: Image.PreserveAspectFit
            anchors.fill: parent
            //source: "../video_capture/20161130142328_other_service_COP1611301.jpg"
        }
    }

    OverTimeButton{
        id:continue_button
        x:558
        y:674
        show_text:qsTr("Capture")
        show_x:15

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                slot_handler.start_video_capture(activity)
                press = "1"
                saving_image_notif.open()
                show_timer_saving.start()
                my_timer.stop()
                photoPreviewAnimated.visible = false
                continue_button.enabled = false
                cancel_button.enabled = false
            }
            onEntered:{
                continue_button.show_source = "img/bottondown/down_1.png"
            }
            onExited:{
                continue_button.show_source = "img/button/7.png"
            }
        }
    }

    OverTimeButton{
        id:cancel_button
        x:181
        y:674
        show_text:qsTr("Cancel")
        show_x:15

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                cancel_button.show_source = "img/bottondown/down_1.png"
            }
            onExited:{
                cancel_button.show_source = "img/button/7.png"
            }
        }
    }

    function handle_text(result){
        if(result == "ERROR"){
            return
        }else{
            source_img = result
        }
    }

    CameraWindow{
        id: saving_image_notif
        //visible: true
        x: 223
        y: 217
        width: 576
        height: 432
        show_gif: "img/item/Spin-arrows.gif"

        Rectangle{
            id: rec_text_saving
            x: 248
            y: 142
            width: 80
            height: 80
            QtObject{
                id:time_saving
                property int counter
                Component.onCompleted:{
                    time_saving.counter = timer_display
                }
            }
            Text {
                id:time_saving_text
                anchors.fill: parent
                width: 80
                height: 80
                font.family:"Microsoft YaHei"
                color:"#c50808"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.PlainText
                font.pointSize:35
                wrapMode: Text.WordWrap
            }
            Timer{
                id:show_timer_saving
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    time_saving_text.text = time_saving.counter
                    time_saving.counter -= 1
                    if(time_saving.counter == 2){
                        slot_handler.start_get_capture_file_location()
                    }
                    if(time_saving.counter < 0){
                        //press: "0"
                        //continue_button.enabled = true
                        show_timer_saving.stop()
                        saving_image_notif.close()
                        my_timer.start()
                        abc.counter = timer_value
                        cancel_button.enabled = true
                        photoPreviewCaptured.source = source_img
                        photoPreviewCaptured.visible = true
                        slot_handler.start_post_capture_file()
                    }
                }
            }
        }
    }
}
