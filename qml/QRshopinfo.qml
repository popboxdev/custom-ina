import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:qr_shop_info
    logo_y: 25
    img_vis: false
    gif_vis: true
    show_gif: "img/item/grocery_how_to.gif"
    property int timer_value: 60
    property var initurl
    property bool texty_visible: false

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Item{
        id: texty_howto
        visible: texty_visible

        FullWidthReminderText{
            id:qr_shop_info_title
            x: 0
            y:135
            border.width: 0
            remind_text:qsTr("How To Shop with QR")
            remind_text_size:"45"
        }

        Image {
            id: step1
            x: 0
            y: 173
            width: 350
            height: 350
            source: "img/shopqr/shopQR1.png"
        }

        Image {
            id: step2
            x: 337
            y: 173
            width: 350
            height: 350
            source: "img/shopqr/shopQR2.png"
        }

        Image {
            id: step3
            x: 668
            y: 173
            width: 350
            height: 350
            source: "img/shopqr/shopQR3.png"
        }

        Text {
            id: textstep1
            x: 37
            y: 519
            width: 276
            height: 125
            color: "#ffffff"
            text: qsTr("Download UANGKU App for IOS or ANDROID.")
            textFormat: Text.AutoText
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 24
            font.italic: true
            font.family:"Microsoft YaHei"
            wrapMode: Text.WordWrap
        }

        Text {
            id: textstep2
            x: 374
            y: 519
            width: 276
            height: 125
            color: "#ffffff"
            text: qsTr("Register and top up your balance.")
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 24
            font.italic: true
            font.family:"Microsoft YaHei"
            wrapMode: Text.WordWrap
        }

        Text {
            id: textstep3
            x: 705
            y: 519
            width: 276
            height: 125
            color: "#ffffff"
            text: qsTr("Scan and shop the product you like.")
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 24
            font.italic: true
            font.family:"Microsoft YaHei"
            wrapMode: Text.WordWrap
        }
    }

    Rectangle{
        id:next_button
        x: 0
        y: 708
        width:1024
        height:60
        color: "black"
        opacity: 0.7

        Text{
            text:qsTr("Touch Everywhere to Start")
            font.family:"Microsoft YaHei"
            color:"white"
            font.pixelSize:25
            anchors.centerIn: parent
            font.bold: true
        }
    }

    MouseArea {
        y: 0
        width: parent.width
        height: parent.height
        enabled: true
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
        onClicked: {
            my_stack_view.push(grocery_product_list_view)
        }
    }
    
    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
        }
    }
}
