import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:courier_input_phone
    width: 1024
    height: 768

    property var show_text:""
    property var show_source:""
    property int timer_value: 60
    property var press: "0"
    property var count:0
    property var usageOf: undefined
    property var pref_login_user: undefined
    property var expressNo: ""
    property var phone_no_temp: ""
    property variant check_number: ['0814','0815', '0816', '0855', '0856', '0857', '0858',
        '0811', '0812', '0813', '0821', '0822', '0823', '0851', '0852', '0853',
        '0817', '0818', '0819', '0859', '0877', '0878', '0879',
        '0831', '0832', '0838', '9991', '9992', '9993', '9994', '9995', '9988',
        '0881', '0882', '0883', '0884', '0885', '0886', '0887', '0888', '0889',
        '0895', '0896', '0897', '0898', '0899']

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log('prefix_user_account : ' + pref_login_user + ', usage: ' + usageOf)
            if(courier_input_phone.show_text!=""){
                courier_input_phone.show_text=""
            }
            if(pref_login_user == "LAZ"){
                main_page.enabled = false
                bypass_lazada.open()
            }
            if(expressNo==""){
                show_rec_express.visible = false
            }
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            loading_gif.close()
        }
    }

    Component.onCompleted: {
        root.get_query_member_popsend_result.connect(handle_popsend_member)
    }

    Component.onDestruction: {
        root.get_query_member_popsend_result.disconnect(handle_popsend_member)
    }

    function handle_popsend_member(result){
        console.log('handle_popsend_member : ' + JSON.stringify(result))
        if(result == ""){
            return
        }else if(result == "NOT FOUND"){
            main_page.enabled = false
            error_tips.open()
        }else{
            my_stack_view.push(popsend_denom_topup, {member_data: result})
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
            }
    }

    Rectangle{
        id:main_page

        FullWidthReminderText {
            id: main_title
            x: 0
            y: 190
            remind_text: (usageOf=="popsafe-order") ? qsTr("Enter Your Phone Number") : qsTr("Enter Recipient Phone Number")
            remind_text_size:"35"
        }

        Rectangle{
            x:137
            y:256
            width:750
            height:75
            color:"transparent"

            Image{
                width:750
                height:75
                source:"img/courier11/input1.png"
            }

            TextEdit{
                id:input_phone
                text:show_text
                y:10
                x:30
                font.family:"Microsoft YaHei"
                color:"#FFFFFF"
                font.pixelSize:35
            }
    }

    OkButton{
        id:select_service_ok_button
        x:699
        y:620
        show_text:qsTr("OK")
        color: "#3b8f23"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                if(courier_input_phone.show_text != ""){
                    if(check_number.indexOf(courier_input_phone.show_text.substring(0,4)) > -1 && courier_input_phone.show_text.length > 9){
                        count=0
                        if(usageOf == "popsend_topup"){
                            loading_gif.open()
                            main_page.enabled = false
                            slot_handler.get_query_member_popsend(courier_input_phone.show_text)
                        } else if (usageOf == "popsafe-order"){
                            if (phone_no_temp == ""){
                                phone_no_temp = courier_input_phone.show_text
                                courier_input_phone.show_text = ''
                                press = '0'
                                count = 0
                                main_title.remind_text = qsTr("Verify Your Phone Number")
                            } else {
                                if (courier_input_phone.show_text == phone_no_temp){
                                     my_stack_view.push(send_select_box, {usageOf: usageOf, customer_phone: phone_no_temp})
                                } else {
                                    notif_text.text = qsTr("Please Ensure You Have Enterred The Same Phone Number!")
                                    error_tips.open()
                                }
                            }
                        } else{
                            my_stack_view.push(courier_input_phone_sure_view,
                                               {show_text:courier_input_phone.show_text, expressNo: expressNo, pref_login_user:pref_login_user})
                        }
                    }else{
                        main_page.enabled = false
                        error_tips.open()
                    }
                }else{
                    main_page.enabled = false
                    error_tips.open()
                }
            }
            onEntered:{
            }
            onExited:{
            }
        }
    }

    NumKeyboard{
        id:customer_take_express_keyboard
        x:378
        y:350

        property var validate_code:""

        Component.onCompleted: {
            customer_take_express_keyboard.letter_button_clicked.connect(show_validate_code)
            customer_take_express_keyboard.function_button_clicked.connect(function_button_action)
        }

        function function_button_action(str){
            if (str == "BACK" || str == "BATAL"){
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 4) return true }))
            }
            if (str == "DELETE" || str == "HAPUS"){
                show_validate_code("")
            }
        }

        function show_validate_code(str){
            if (str == "" && count > 0){
                if(count>=15){
                    count=15
                }
                count--
                courier_input_phone.show_text=courier_input_phone.show_text.substring(0,count);
            }
            if(count==0){
                courier_input_phone.show_text=""
            }
            if (str != "" && count < 15){
                count++
            }
            if (count>=15){
                str=""
            }
            else{
                courier_input_phone.show_text += str
            }
            abc.counter = timer_value
            my_timer.restart()
        }
    }
}

    Rectangle{
        id: show_rec_express
        x : -20
        y: 125
        width : text_show_rec_express.width + 50
        height : 50
        color : "white"
        opacity : 0.85
        radius : 20

        Text {
            id: text_show_rec_express
            x: 25
            text: expressNo
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: 27
            font.italic: true
            anchors.leftMargin: 60
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            color:"darkred"
            textFormat: Text.PlainText
            height: show_rec_express.height

        }
    }

    LoadingView{
        id:loading_gif
    }

    HideWindow{
        id:error_tips
//        visible: true

        Image {
            id: img_error_tips
            x: 137
            y: 304
            width: 200
            height: 200
            source: "img/manage25/error.png"
        }

        Text {
            id: notif_text
            x: 373
            y:329
            width: 525
            height: 150
            text: qsTr("The memory is not input or not correct")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:error_tips_back
            x:373
            y:598
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    main_page.enabled = true
                    error_tips.close()
                    loading_gif.close()
                    count=0
                    courier_input_phone.show_text = ""
                }
            }
        }
    }

    HideWindow{
        id:bypass_lazada
        //visible: true

        Image {
            id: img_bypass_lazada
            x: 144
            y: 306
            width: 200
            height: 200
            fillMode: Image.PreserveAspectFit
            source: "img/returnstep/lazada-white.png"
        }

        Text {
            x: 388
            y:306
            width: 500
            height: 120
            text: qsTr("This parcel is detected under Lazada Account")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:bypass_lazada_back
            x:373
            y:598
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    slot_handler.set_phone_number('081212231457')
                    my_stack_view.push(courier_select_box_view, {pref_login_user:pref_login_user})
                }
            }
        }
    }

}
