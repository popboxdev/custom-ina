import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:reject_input_phone
    width: 1024
    height: 768
    property var show_text:""
    property int timer_value: 30
    property var press: "0"
    property var count:0
    property variant check_number: ['0814','0815', '0816', '0855', '0856', '0857', '0858',
        '0811', '0812', '0813', '0821', '0822', '0823', '0851', '0852', '0853',
        '0817', '0818', '0819', '0859', '0877', '0878', '0879',
        '0831', '0832', '0838', '9991', '9992', '9993', '9994', '9995', '9988',
        '0881', '0882', '0883', '0884', '0885', '0886', '0887', '0888', '0889',
        '0895', '0896', '0897', '0898', '0899']

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            if(reject_input_phone.show_text!=""){
                reject_input_phone.show_text=""
            }
            press = "0"
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Timer{
        id:my_timer
        interval:timer_value*1000
        repeat:true
        running:true
        triggeredOnStart:false
        onTriggered:{
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
        }
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
            onEntered:{
                select_service_back_button.show_source = "img/bottondown/down.2.png"
            }
            onExited:{
                select_service_back_button.show_source = "img/button/7.png"
            }
        }
    }

    Rectangle{
    id:main_page

    FullWidthReminderText {
        x: 0
        y: 180
        remind_text:qsTr("Enter Recipient Phone Number")
        remind_text_size:"35"
    }

    Rectangle{
        x:137
        y:256
        width:750
        height:75
        color:"transparent"

        Image{
            width:750
            height:75
            source:"img/courier11/input1.png"
        }

        TextEdit{
            id:input_phone
            text:show_text
            y:10
            x:30
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            font.pixelSize:35
        }
    }

    OkButton{
        id:reject_inputphone_ok_button
        x:699
        y:620
        show_text:qsTr("OK")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                if(reject_input_phone.show_text != ""){
                    if(check_number.indexOf(reject_input_phone.show_text.substring(0,4)) > -1 && reject_input_phone.show_text.length > 9){
                        count=0
                        my_stack_view.push(reject_input_phone_sure_view,{show_text:reject_input_phone.show_text})
                    }else{
                        main_page.enabled = false
                        error_tips.open()
                    }
                }
                else{
                    main_page.enabled = false
                    error_tips.open()
                }
            }
            onEntered:{
            }
            onExited:{
            }
        }
    }

    NumKeyboard{
        id:customer_take_express_keyboard
        x:378
        y:350

        property var validate_code:""

        Component.onCompleted: {
            customer_take_express_keyboard.letter_button_clicked.connect(show_validate_code)
            customer_take_express_keyboard.function_button_clicked.connect(function_button_action)
        }

        function function_button_action(str){
            if (str == "BACK" || str == "BATAL"){
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 2) return true }))
            }
            if (str == "DELETE" || str == "HAPUS"){
                show_validate_code("")
            }
        }

        function show_validate_code(str){

            if (str == "" && count > 0){
                if(count>=15){
                    count=15
                }
                count--
                reject_input_phone.show_text=reject_input_phone.show_text.substring(0,count);
            }
            if(count==0){
                reject_input_phone.show_text=""
            }
            if (str != "" && count < 15){
                count++
            }
            if (count>=15){
                str=""
            }
            else{
                reject_input_phone.show_text += str
            }
            my_timer.restart()
        }
    }
}

    HideWindow{
        id:error_tips
        //visible: true

        Image {
            id: img_error_tips
            x: 137
            y: 304
            width: 200
            height: 200
            source: "img/manage25/error.png"
        }

        Text {
            id: notif_text
            x: 373
            y:329
            width: 525
            height: 150
            text: qsTr("The memory is not input or not correct")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:error_tips_back
            x:373
            y:598
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    my_timer.restart()
                    main_page.enabled = true
                    error_tips.close()
                }
                onEntered:{
                }
                onExited:{
                }
            }
        }
    }
}
