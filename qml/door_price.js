//Helper JS for Door Compartment Pricing

var default_price = "10000";
var popsafe_price = "1"

function get_price(door_no) {
    if (door_no == undefined) return popsafe_price;
    switch (door_no) {
        case "MINI":
            return "6000";
        case "S":
            return "7000";
        case "M":
            return "8000";
        case "L":
            return "1";
        case "XL":
            return "10000";
        default:
            return popsafe_price;
    }
}