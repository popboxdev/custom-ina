import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: rectangle1
    width:415
    height:50
    color:"#3b8f23"
    radius: 22
    property var slot_text:""
    property var show_image:""

    Image{
        id:function_button_Image
        width:415
        height:50
        source:""
    }

    Text{
        text:qsTr("OK")
        font.bold: true
        color:"#ffffff"
        font.family:"Microsoft YaHei"
        font.pixelSize:24
        anchors.centerIn: parent;
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if(slot_text != "delete"){
                full_keyboard.function_button_clicked(slot_text)
            }
            else
                full_keyboard.letter_button_clicked("")
        }
    }
}
