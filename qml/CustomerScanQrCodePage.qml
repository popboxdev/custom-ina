import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    property int timer_value: 60

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.customer_scan_barcode_take_express()
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            slot_handler.customer_cancel_scan_barcode()
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
    }

    Image{
        y:200
        width:473
        height:520
        source:"img/item/1.png"
    }

    Text{
        x:584
        y:290
        text:qsTr("Please open your QR code, \n the code sent by SMS \n to your mobile. Please QR \n code placed in front of the scan window \n 10cm Office")
        width:280
        color:"#FFFFFF"
        font.family:"Microsoft YaHei"
        font.pixelSize:30
    }

    Component.onCompleted: {
        root.customer_take_express_result.connect(log_text)
    }

    Component.onDestruction: {
        root.customer_take_express_result.disconnect(log_text)
    }

    function log_text(text){
        switch(text){
        case "Success" : my_stack_view.push(customer_take_express_opendoor_view)
            break;
        case "Overdue" : my_stack_view.push(customer_take_express_overtime_view)
            break;
        case "Error" : my_stack_view.push(customer_take_express_error_view)
            break;
        default : my_stack_view.pop()
        }
    }

    BackButtonMenu{
        id:back_button
        x:584
        y:630
        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_source = "img/bottondown/down1.png"
            }
            onExited:{
                back_button.show_source = "img/button/7.png"
            }
        }
    }

}
