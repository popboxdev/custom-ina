import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    id: baseAP
    property var press: '0'
    watermark: true
    topPanelColor: 'GREEN'
    mainMode: false
    property var pin_code: ''
    property int timer_value: 30
    property int count: 0
    property var extend: undefined
    property var dataOverdue: ''

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            abc.counter = timer_value;
            my_timer.start();
            reset_value();
//            extend = undefined;
 //            slot_handler.start_customer_scan_qr_code();
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            slot_handler.stop_customer_scan_qr_code();
        }
    }

    Component.onCompleted: {
        root.customer_take_express_result.connect(process_result);
        // root.overdue_apservice_calculate_result.connect(overdue_apexpress_calculate);
    }

    Component.onDestruction: {
        root.customer_take_express_result.disconnect(process_result);
        // root.overdue_apservice_calculate_result.connect(overdue_apexpress_calculate);

    }

    function reset_value(){
        first_box.show_text = "";
        second_box.show_text = "";
        third_box.show_text = "";
        fourth_box.show_text = "";
        fifth_box.show_text = "";
        sixth_box.show_text = "";
        pin_code = '';
        count = 0;
    }

    function process_result(text){
        console.log('process_result_ : ', text)
        if (text=='Error'||text=='NotInput'){
            failure_notif.titleText = qsTr('Parcel Tidak Ditemukan');
            failure_notif.notifText = qsTr('Mohon Maaf, Silakan Masukkan Kode Pin Yang Benar.'); 
            //failure_notif.buttonCancel = false;
            failure_notif.open();
            reset_value();
            return
        }
/*
Success||{"id": "fc6852c09ba211e89941509a4ccea7f5", "mouth_id": "4028808359ff616b015a172842265752", "amount": 2000, "express_id": "fb4f280a9ba211e8be26509a4ccea7f5", "createTi
me": 1533798495000, "transactionType": "{\"express\": \"APXID-9M2VX\", \"pin_code\": \"E3S9VC\", \"result\": \"SUCCESS\", \"door_no\": 44, \"phone_number\": \"085710157057\", \"date\": \"2018-08-09
 22:08:13\"}", "paymentType": "E3S9VC"}
277842000
 1543575004000
*/
        var status = text.split('||')[0];
        var info = text.split('||')[1];
        
        console.log(" status: " + status + " info " + info)
        // undefined
        if (info != undefined && info != "undefined") {
            var detail = JSON.parse(info);
            dataOverdue = detail.overdue_data

            if (text.split('||')[2] != undefined) {
                var tempDoor = text.split('||')[2]
                console.log('data_tempdoor ' + tempDoor)
                var x = JSON.parse(tempDoor)
            } else {
                var x = JSON.parse(detail.transactionType);
                console.log("DATA JSON INPUT PIN : " + text.x)
            }
        } else if (info == "undefined" || info == undefined) {
            var tempDoor = text.split('||')[2]
            console.log('data_tempdoor ' + tempDoor)
            var x = JSON.parse(tempDoor)
        }

        if (status=='Overdue'){
            extend = { 'phone_number': x.phone_number, 'isExtending': true, 'data': info }
            failure_notif.titleText = qsTr('Melebihi Batas Waktu');
            failure_notif.notifText = qsTr('\nPaket sudah melebihi batas waktu\npengambilan.\n\n\nUntuk mengambil paket Anda\nsilakan melakukan pembayaran atas\nperpanjangan waktu paket Anda.');
            failure_notif.buttonCloseText = qsTr('KEMBALI')
            failure_notif.open();
            // slot_handler.overdue_apexpress(express_id);
            // my_stack_view.push(select_payment_ap, {phone_number: x.phone_number, isExtending: true, extendData: info});
        }

        if (status=='Success'){
            console.log('status_door' + status);
            my_stack_view.push(take_parcel_ap, {door_no: x.door_no});
        }
    }

    function process_repayment(){        
        
        console.log("DATA_OVERDUE: " + dataOverdue + "isExtend: " + extend.isExtending)
        
        my_stack_view.push(select_payment_ap, {
            phone_number: extend.phone_number, 
            isExtending: extend.isExtending, 
            extendData: extend.data, 
            dataOverdue: dataOverdue,
            topPanelColor: 'GREEN',
            imageLangkah: "img/apservice/langkah/ambil02.png"
        });
    }

    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    Rectangle{
        id: timer_set
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Image {
        id: icon_confirm
        fillMode: Image.PreserveAspectFit
        //scale: 0.9
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        source: "img/apservice/img/inputpin.png"
    }

    Text {
        id: main_command_text
        width: 500
        height: 100
        color: "#323232"
        text: qsTr("Masukkan Kode PIN")
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 170
        font.bold: true
        font.family: 'Microsoft YaHei'
        font.pixelSize: 50

    }

    Row{
        anchors.top: parent.top
        anchors.topMargin: 250
        anchors.left: parent.left
        anchors.leftMargin: 50
        spacing: 5
        PinBoxAP{
            id:first_box
        }
        PinBoxAP{
            id:second_box
        }
        PinBoxAP{
            id:third_box
        }
        PinBoxAP{
            id:fourth_box
        }
        PinBoxAP{
            id:fifth_box
        }
        PinBoxAP{
            id:sixth_box
        }
    }

    Rectangle{
        id: base_ground_keyboard
        color: '#333A44'
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        opacity: 0.60
        width: parent.width
        height: 400

    }

    NumKeyboard{
        id:touch_keyboard
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        functionPad: false
        enabled: !notif_text.visible

        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(show_validate_code);
            touch_keyboard.function_button_clicked.connect(on_function_button_clicked);
            root.start_customer_scan_qr_code_result.connect(read_scanner);

        }

        Component.onDestruction: {
            touch_keyboard.letter_button_clicked.disconnect(show_validate_code);
            touch_keyboard.function_button_clicked.disconnect(on_function_button_clicked);
            root.start_customer_scan_qr_code_result.disconnect(read_scanner);
        }

        NumButton{
            id: reset_button
            x: 0
            y: 270
            defaultColor: 'orange'
            img_source: ''
            show_text: qsTr('HAPUS')
            textColor: 'white'
            textSize: 20
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (count == 1) first_box.show_text = '';
                    if (count == 2) second_box.show_text = '';
                    if (count == 3) third_box.show_text = '';
                    if (count == 4) fourth_box.show_text = '';
                    if (count == 5) fifth_box.show_text = '';
                    if (count == 6) sixth_box.show_text = '';
                    if (count>0) {
                        count--;
                    } else {
                        count=0;
                    }
                }
            }
        }

        NumButton{
            id: find_button
            x: 180
            y: 270
            enabled: (sixth_box.show_text != '') ? true : false
            defaultColor: (enabled) ? '#7ED321' : 'gray'
            img_source: ''
            show_text: qsTr('OK')
            textColor: 'white'
            textSize: 20
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (press!='0') return;
                    press = '1';
                    pin_code = '';
                    pin_code += first_box.show_text;
                    pin_code += second_box.show_text;
                    pin_code += third_box.show_text;
                    pin_code += fourth_box.show_text;
                    pin_code += fifth_box.show_text;
                    pin_code += sixth_box.show_text;
                    slot_handler.customer_take_express(pin_code+'||AP_EXPRESS');
                    slot_handler.start_video_capture("take_apexpress_" + pin_code);
                    console.log('OK Button Processing with : ', pin_code);
                }
            }
        }

        function read_scanner(text){
            var result = text
            console.log('read_scanner_result : ' + result + ', with length : ' + result.length)
            if(result == "" || result.length != 6){
                return
            }
            first_box.show_text = result.substring(0,1)
            second_box.show_text = result.substring(1,2)
            third_box.show_text = result.substring(2,3)
            fourth_box.show_text = result.substring(3,4)
            fifth_box.show_text = result.substring(4,5)
            sixth_box.show_text = result.substring(5,6)
            slot_handler.customer_take_express(result+'||AP_EXPRESS')
            slot_handler.start_video_capture("take_apexpress_" + result)
            console.log('Scanner Read Processing')
        }

        function on_function_button_clicked(str){
            if(str == "ok"){
                if(press != "0") return;
                press = "1";
                pin_code = '';
                pin_code += first_box.show_text;
                pin_code += second_box.show_text;
                pin_code += third_box.show_text;
                pin_code += fourth_box.show_text;
                pin_code += fifth_box.show_text;
                pin_code += sixth_box.show_text;
                slot_handler.customer_take_express(pin_code+'||AP_EXPRESS');
                slot_handler.start_video_capture("take_apexpress_" + pin_code);
                console.log('OK Button Processing with : ', pin_code);
            }
        }

        function show_validate_code(str){
            if (count == 0) first_box.show_text = str;
            if (count == 1) second_box.show_text = str;
            if (count == 2) third_box.show_text = str;
            if (count == 3) fourth_box.show_text = str;
            if (count == 4) fifth_box.show_text = str;
            if (count == 5) sixth_box.show_text = str;
            if (count <= 5) count++;
            press = '0';
            abc.counter = timer_value;
            my_timer.restart();
        }
    }


    Notification{
        id: notif_text
        contentNotif: qsTr('Pastikan Kode Pin Anda adalah Benar.')
        successMode: false;

    }
    Image {
        id: step_progress
        width: 500
        height: 51
        anchors.top: parent.top
        anchors.topMargin: 115
        anchors.horizontalCenter: parent.horizontalCenter
        source: "img/apservice/langkah/ambil01.png"
    }

    FailurePaymentAPAmbil{
        id: failure_notif
//        visible: true
        NotifButtonAP{
            id: proceed_button
            x: 357
            y: 640
            visible: (extend==undefined) ? false : true
            buttonColor: parent.topPanelColor
            buttonText: qsTr('LANJUTKAN')
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    process_repayment();
                }
            }
        }
    }
}
