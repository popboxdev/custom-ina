import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQml.Models 2.1

Background{
    id: base_bground
    width: 1024
    height: 768
    logo_y: 25
    property int timer_value: 60
    property var press:"0"
    property var grocery_prod_list:""
    property var category:"16"
    property var maxcount:"25"
    property variant bad_chars: ["'", "\t", "\n", "\r", "`", "&"]
    property var playMode:"normal"

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.get_popshop_product(category,maxcount)
            abc.counter = timer_value
            my_timer.restart()
            loadingGif.open()
            prod_item_view.visible = false           
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            groceryItem_listModel.clear()
        }
    }

    Component.onCompleted: {
        root.get_popshop_product_result.connect(handle_result_product)
    }

    Component.onDestruction: {
        root.get_popshop_product_result.disconnect(handle_result_product)
    }

    function insert_flg(a){
        var newstrA=""
        newstrA = a.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstrA
    }

    function patch_url(b){
        var newstrB=""
        if(b != null){
            newstrB = b.replace("https://shop.popbox.asia/assets/images","img/grocery_prod")
        }else{
            return b
        }
        return newstrB
    }

    function remove_badchar(c){
        var newstrC=""
        for (var bad in bad_chars){
            if(c.indexof(bad) > -1){
                newstrC = c.replace(bad,"")
            }else{
                return
            }
        }
        return newstrC
    }

    function handle_result_product(result){
        //console.log("start_get_popshop_product_result : " + result)
        if(result == "ERROR"){
            network_error.open()
            main_page.enabled = false
        }else{
            var list_products = JSON.parse(result)
            var total_products = list_products.prod_total
            grocery_prod_list = list_products.prod_list
            parseDataProduct()
            if(grocery_prod_list != "" && total_products > 0){
                loadingGif.close()
                prod_item_view.visible = true
                //main_page.visible = true
            }else{
                loadingGif.open()
                prod_item_view.visible = false
            }
        }
    }

    function parseDataProduct(){
        //Undefined the gridview model
        gridview.model = undefined
        var items = grocery_prod_list
        if(!groceryItem_listModel.count){
            for(var x in items) {
                var show_prod_sku = items[x].sku
                var show_prod_init_price = insert_flg(items[x].price)
                var show_prod_disc = "Save: " + insert_flg(items[x].discount)
                var show_prod_price = insert_flg(items[x].total)
                var show_prod_name = items[x].name
                var url_prod_image = patch_url(items[x].image)
                var url_prod_image_qr = patch_url(items[x].qrcode)
                var array_product = items[x]
                //Appending Product Items
                groceryItem_listModel.append({"prod_name_": show_prod_name,
                                                 "prod_price_":show_prod_price,
                                                 "prod_disc_": show_prod_disc,
                                                 "prod_init_price_": show_prod_init_price,
                                                 "prod_sku_": show_prod_sku,
                                                 "prod_image_": url_prod_image,
                                                 "prod_image_qr_": url_prod_image_qr,
                                                 "array_prod_": array_product})
            }
        }else{
            return
        }
        //Re-define the gridview model
        gridview.model = groceryItem_listModel
        console.log('Total Products in Category ' + category + " : " + groceryItem_listModel.count);
        /*for(var i = 0; i < groceryItem_listModel.count; ++i) {
            console.log(groceryItem_listModel.get(i).prod_sku_ +" -> "+ groceryItem_listModel.get(i).prod_name_);
        }*/
    }

    function buttonGetProduct(x,y){
        abc.counter = timer_value
        my_timer.restart()
        groceryItem_listModel.clear()
        loadingGif.open()
        prod_item_view.visible = false
        category = x
        maxcount = y
        slot_handler.get_popshop_product(category,maxcount)
    }

    LoadingView{
        id: loadingGif
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter == 30 && grocery_prod_list == ""){
                    network_error.open()
                }
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                //TODO Check referer page after action
                my_stack_view.pop()
            }
        }
    }

    Rectangle{
        id: main_page

        Item{
            id: prod_item_view
            x: 73
            y: 105
            width: 920
            height: 650
            anchors.bottom: parent.bottom
            anchors.bottomMargin: -767
            anchors.right: parent.right
            anchors.rightMargin: -1023
            focus: true

            ScrollBarPop{
                id: vertical_sbar
                flickable: gridview
                x: 955
                y: 266
                height: prod_item_view.height
                color: "white"
            }

            GridView{
                id: gridview
                focus: true
                clip: true
                anchors.bottomMargin: 0
                anchors.topMargin: 0
                flickDeceleration: 750
                maximumFlickVelocity: 1500
                anchors.centerIn: parent
                layoutDirection: Qt.LeftToRight
                flow: GridView.FlowLeftToRight
                boundsBehavior: Flickable.StopAtBounds
                snapMode: GridView.SnapToRow
                anchors.fill: parent
                anchors.margins: 20
                delegate: delegate_item_view
                //model: groceryItem_listModel -> Defined on function below
                cellWidth: 215
                cellHeight: 315
            }

            ListModel {
                id: groceryItem_listModel
                dynamicRoles: true
            }

            Component{
                id: delegate_item_view
                GroceryItemView{
                    id: item_view
                    prod_name: prod_name_
                    prod_price: prod_price_
                    prod_disc: prod_disc_
                    prod_init_price: prod_init_price_
                    prod_sku: prod_sku_
                    prod_image: prod_image_
                    prod_image_qr: prod_image_qr_
                    property var array_prod: array_prod_

                    MouseArea {
                        id: img_prod_click
                        x: 0
                        y: 0
                        width: parent.width
                        height: parent.height
                        onClicked: {
                            abc.counter = timer_value
                            my_timer.restart()
                            if(playMode=="normal"){
                                var usageOf = "popshop"
                                var data_product = item_view.array_prod
                                my_stack_view.push(grocery_input_quantity, {data_prod_: data_product, usageOf: usageOf})
                            }else{
                                return
                            }
                        }
                    }

                    //Emoney Shortcut on Previous View
                    /*MouseArea{
                        id: emoney_click
                        enabled: (item_view.prod_sku_vis==true) ? true : false
                        //enabled: true
                        x: 0
                        y: img_prod_click.height
                        width: parent.width
                        height: parent.height - img_prod_click.height
                        onClicked: {
                            var usageOf = "popshop"
                            var data_product = item_view.array_prod
                            my_stack_view.push(grocery_input_quantity, {data_prod_: data_product, usageOf: usageOf})
                        }

                        Image{
                            id:img_logo_emoney
                            x: 6
                            y:15
                            width: parent.width - 12
                            height: parent.height - 12
                            visible: emoney_click.enabled
                            source: "img/otherservice/logo-emoney300.png"
                            fillMode: Image.PreserveAspectFit
                        }

                        Text{
                            id: text_notif_with_emoney
                            visible: emoney_click.enabled
                            anchors.fill: parent
                            color: "darkred"
                            text: qsTr("or Pay with")
                            font.family: "Microsoft YaHei"
                            font.italic: true
                            font.bold: true
                            font.pixelSize: 15
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }*/
                }
            }
        }

        Rectangle{
            id: category_button
            focus: true
            visible: true
            color: "transparent"
            anchors.right: prod_item_view.left
            anchors.rightMargin: 0
            x: 0
            y: 117
            width: base_bground.width - prod_item_view.width
            height: prod_item_view.height

            Column{
                Image{
                    id: cat1_button
                    source: "img/grocery_prod/button/musthave_white.png"
                    width: category_button.width
                    height: 90
                    fillMode: Image.Stretch

                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        buttonGetProduct("7","25")
                        cat1_button.enabled = false
                        cat2_button.enabled = true
                        cat3_button.enabled = true
                        cat4_button.enabled = true
                        cat5_button.enabled = true
                        cat6_button.enabled = true
                        cat7_button.enabled = true
                        cat1_button.source = "img/grocery_prod/button/musthave_red.png"
                        cat2_button.source = "img/grocery_prod/button/beacare_white.png"
                        cat3_button.source = "img/grocery_prod/button/cosmetic_white.png"
                        cat4_button.source = "img/grocery_prod/button/gadget_white.png"
                        cat5_button.source = "img/grocery_prod/button/household_white.png"
                        cat6_button.source = "img/grocery_prod/button/snack_white.png"
                        cat7_button.source = "img/grocery_prod/button/toys_white.png"
                        }
                    }
                }
                Image{
                    id: cat2_button
                    source: "img/grocery_prod/button/beacare_white.png"
                    width: category_button.width
                    height: 90
                    fillMode: Image.Stretch

                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        buttonGetProduct("1","25")
                        cat1_button.enabled = true
                        cat2_button.enabled = false
                        cat3_button.enabled = true
                        cat4_button.enabled = true
                        cat5_button.enabled = true
                        cat6_button.enabled = true
                        cat7_button.enabled = true
                        cat1_button.source = "img/grocery_prod/button/musthave_white.png"
                        cat2_button.source = "img/grocery_prod/button/beacare_red.png"
                        cat3_button.source = "img/grocery_prod/button/cosmetic_white.png"
                        cat4_button.source = "img/grocery_prod/button/gadget_white.png"
                        cat5_button.source = "img/grocery_prod/button/household_white.png"
                        cat6_button.source = "img/grocery_prod/button/snack_white.png"
                        cat7_button.source = "img/grocery_prod/button/toys_white.png"
                        }
                    }
                }
                Image{
                    id: cat3_button
                    source: "img/grocery_prod/button/cosmetic_white.png"
                    width: category_button.width
                    height: 90
                    fillMode: Image.Stretch

                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        buttonGetProduct("2","25")
                        cat1_button.enabled = true
                        cat2_button.enabled = true
                        cat3_button.enabled = false
                        cat4_button.enabled = true
                        cat5_button.enabled = true
                        cat6_button.enabled = true
                        cat7_button.enabled = true
                        cat1_button.source = "img/grocery_prod/button/musthave_white.png"
                        cat2_button.source = "img/grocery_prod/button/beacare_white.png"
                        cat3_button.source = "img/grocery_prod/button/cosmetic_red.png"
                        cat4_button.source = "img/grocery_prod/button/gadget_white.png"
                        cat5_button.source = "img/grocery_prod/button/household_white.png"
                        cat6_button.source = "img/grocery_prod/button/snack_white.png"
                        cat7_button.source = "img/grocery_prod/button/toys_white.png"
                        }
                    }
                }
                Image{
                    id: cat4_button
                    source: "img/grocery_prod/button/gadget_white.png"
                    width: category_button.width
                    height: 90
                    fillMode: Image.Stretch

                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        buttonGetProduct("3","25")
                        cat1_button.enabled = true
                        cat2_button.enabled = true
                        cat3_button.enabled = true
                        cat4_button.enabled = false
                        cat5_button.enabled = true
                        cat6_button.enabled = true
                        cat7_button.enabled = true
                        cat1_button.source = "img/grocery_prod/button/musthave_white.png"
                        cat2_button.source = "img/grocery_prod/button/beacare_white.png"
                        cat3_button.source = "img/grocery_prod/button/cosmetic_white.png"
                        cat4_button.source = "img/grocery_prod/button/gadget_red.png"
                        cat5_button.source = "img/grocery_prod/button/household_white.png"
                        cat6_button.source = "img/grocery_prod/button/snack_white.png"
                        cat7_button.source = "img/grocery_prod/button/toys_white.png"
                        }
                    }
                }
                Image{
                    id: cat5_button
                    source: "img/grocery_prod/button/household_white.png"
                    width: category_button.width
                    height: 90
                    fillMode: Image.Stretch

                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        buttonGetProduct("4","25")
                        cat1_button.enabled = true
                        cat2_button.enabled = true
                        cat3_button.enabled = true
                        cat4_button.enabled = true
                        cat5_button.enabled = false
                        cat6_button.enabled = true
                        cat7_button.enabled = true
                        cat1_button.source = "img/grocery_prod/button/musthave_white.png"
                        cat2_button.source = "img/grocery_prod/button/beacare_white.png"
                        cat3_button.source = "img/grocery_prod/button/cosmetic_white.png"
                        cat4_button.source = "img/grocery_prod/button/gadget_white.png"
                        cat5_button.source = "img/grocery_prod/button/household_red.png"
                        cat6_button.source = "img/grocery_prod/button/snack_white.png"
                        cat7_button.source = "img/grocery_prod/button/toys_white.png"
                        }
                    }
                }                
                Image{
                    id: cat6_button
                    source: "img/grocery_prod/button/snack_white.png"
                    width: category_button.width
                    height: 90
                    fillMode: Image.Stretch

                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        buttonGetProduct("5","25")
                        cat1_button.enabled = true
                        cat2_button.enabled = true
                        cat3_button.enabled = true
                        cat4_button.enabled = true
                        cat5_button.enabled = true
                        cat6_button.enabled = false
                        cat7_button.enabled = true
                        cat1_button.source = "img/grocery_prod/button/musthave_white.png"
                        cat2_button.source = "img/grocery_prod/button/beacare_white.png"
                        cat3_button.source = "img/grocery_prod/button/cosmetic_white.png"
                        cat4_button.source = "img/grocery_prod/button/gadget_white.png"
                        cat5_button.source = "img/grocery_prod/button/household_white.png"
                        cat6_button.source = "img/grocery_prod/button/snack_red.png"
                        cat7_button.source = "img/grocery_prod/button/toys_white.png"
                        }
                    }
                }                
                Image{
                    id: cat7_button
                    source: "img/grocery_prod/button/toys_white.png"
                    width: category_button.width
                    height: 90
                    fillMode: Image.Stretch

                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        buttonGetProduct("6","25")
                        cat1_button.enabled = true
                        cat2_button.enabled = true
                        cat3_button.enabled = true
                        cat4_button.enabled = true
                        cat5_button.enabled = true
                        cat6_button.enabled = true
                        cat7_button.enabled = false
                        cat1_button.source = "img/grocery_prod/button/musthave_white.png"
                        cat2_button.source = "img/grocery_prod/button/beacare_white.png"
                        cat3_button.source = "img/grocery_prod/button/cosmetic_white.png"
                        cat4_button.source = "img/grocery_prod/button/gadget_white.png"
                        cat5_button.source = "img/grocery_prod/button/household_white.png"
                        cat6_button.source = "img/grocery_prod/button/snack_white.png"
                        cat7_button.source = "img/grocery_prod/button/toys_red.png"
                        }
                    }
                }
            }
        }
    }

    HideWindow{
        id:network_error
        //visible: true

        Image {
            id: img_network_error
            x: 437
            y: 412
            width: 150
            height: 180
            source: "img/otherImages/network-offline.png"
        }

        Text {
            id: text_notif
            x: 112
            y:232
            width: 800
            height: 150
            text: qsTr("Currently we are not connected to the system, Please retry later")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:network_error_back_button
            x:373
            y:607
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
                onEntered:{
                    network_error_back_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    network_error_back_button.show_source = "img/button/7.png"
                }
            }
        }
    }


}
