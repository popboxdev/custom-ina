import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:sepulsa_phone_confirmation_page
    width: 1024
    height: 768
    property var phone_no
    property int timer_value: 60
    property var press: "0"
    property var show_provider: "Undefined"
    property var service
    property var list_provider

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            get_provider(phone_no, list_provider)
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        width: 50
        height: 50
        color: "transparent"
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Text{
            id:countShow_test
            x: 10
            y: 10
            anchors.centerIn:parent
            color:"#fff000"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:20
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                countShow_test.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Rectangle{
        id:opacity_bground
        x: 0
        y: 0
        width: 1024
        height: 768
        color: "#472f2f"
        opacity: 0.4
    }

    Rectangle{
        id: notif_one
        x: 130
        y: 220
        width: 790
        height: 440
        visible: true

        Image {
            id: bground_one
            x: -5
            y: -2
            width: 800
            height: 450
            source: "img/courier13/layerground1.png"
            Text {
                id: please_check_phone
                x: 0
                y: 40
                width: 800
                height: 60
                color: "#ffffff"
                font.family:"Microsoft YaHei"
                text: qsTr("Kindly verify your phone number")
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 30
                textFormat: Text.AutoText
            }            
            Text {
                id: text_provider
                x: 0
                y: 195
                width: 800
                height: 60
                color: "#ffffff"
                text: '(' + show_provider + ')'
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 40
                font.family:"Microsoft YaHei"
                textFormat: Text.AutoText
            }
            Text {
                id: phone_num_one
                x: 0
                y: 115
                width: 800
                height: 60
                color: "#ffffff"
                text: phone_no
                font.family:"Microsoft YaHei"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 50
            }
        }

        OverTimeButton{
            id:back_button_notif_one
            x:90
            y:300
            show_text:qsTr("Back")
            show_x:15
            show_image:"img/05/back.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop()
                }
                onEntered:{
                    back_button_notif_one.show_source = "img/05/pushdown.png"
                }
                onExited:{
                    back_button_notif_one.show_source = "img/05/button.png"
                }
            }
        }

        OverTimeButton{
            id:ok_button_notif_one
            x:430
            y:300
            show_text:qsTr("OK")
            show_x:205
            show_image:"img/05/ok.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    my_stack_view.push(sepulsa_select_denom_view,{show_provider:show_provider, phone_no:phone_no})
                }
                onEntered:{
                    ok_button_notif_one.show_source = "img/05/pushdown.png"
                }
                onExited:{
                    ok_button_notif_one.show_source = "img/05/button.png"
                }
            }
        }
    }

    function get_provider(phone_no, list_provider){
        if(service == "Sepulsa"){
            var cells = JSON.parse(list_provider)
            for(var c in cells){
                if(phone_no.substring(0,4) == cells[c].init){
                    show_provider = cells[c].name
                }
            }
        }
    }
}
