import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: sepulsa_payment_successful_page
    width: 1024
    height: 768
    property int timer_value: 10
    property int timer_thanks: 20
    property var last_balance:"-"
    property var card:"-"
    property var tx_date:"-"
    property var locker:"-"
    property var terminal_id:"-"
    property var service:"-"
    //property var payment_result:'{"show_date": 1483511917000, "locker": "Demo Locker", "last_balance": "995000", "card_no": "1234567887654321", "terminal_id": "01234567"}'
    property var payment_result:""
    property var amount:"0"
    property var product_id:"-"
    property var product_type:"-"
    property var phone_no:"-"
    property var payment_type:"MANDIRI-EMONEY"
    property var new_amount:"-"
    property var trx_id:""
    property var trx_result:"Transaksi Sukses "

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log("succesfully deducted by e-money: " + insert_flg(amount))
            loadingGif.open()
            main_page.enabled = false
            ok_button_main_page.enabled = false
            show_payment_result(payment_result)
            abc.counter = timer_value
            my_timer.restart()
            thank_you_notif.close()
            if(phone_no != "-" && product_id != "-" && product_type != "-"){
                slot_handler.start_sepulsa_transaction(phone_no, product_id, product_type, payment_type)
            }
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            show_timer_thanks.stop()
        }
    }

    Component.onCompleted: {
        root.start_sepulsa_transaction_result.connect(handle_text)
        root.sepulsa_trx_check_result.connect(handle_trx)
    }

    Component.onDestruction: {
        root.start_sepulsa_transaction_result.disconnect(handle_text)
        root.sepulsa_trx_check_result.disconnect(handle_trx)
    }

    function show_payment_result(payment_result){
        console.log("settlement_result : " + payment_result)
        if(payment_result == ""){
            return
        }

        var obj = JSON.parse(payment_result)
        var now_date = new Date(obj.show_date)
        var time_Y = add_zero(now_date.getFullYear());
        var mouth = (now_date.getMonth()+1 < 10 ? +(now_date.getMonth()+1) : now_date.getMonth()+1)

        switch (mouth){
            case 1: var time_M = "January"
                break;
            case 2: var time_M = "February"
                break;
            case 3: var time_M = "March"
                break;
            case 4: var time_M = "April"
                break;
            case 5: var time_M = "May"
                break;
            case 6: var time_M = "June"
                break;
            case 7: var time_M = "July"
                break;
            case 8: var time_M = "August"
                break ;
            case 9: var time_M = "September"
                break;
            case 10: var time_M = "October"
                break;
            case 11: var time_M = "November"
                break;
            case 12: var time_M = "December"
                break;
        }

        var time_D = add_zero(now_date.getDate());
        var time_h = add_zero(now_date.getHours());
        var time_m = add_zero(now_date.getMinutes());
        var time_s = add_zero(now_date.getSeconds());

        tx_date = time_D+" "+time_M+" "+time_Y+" "+time_h+":"+time_m+":"+time_s
        terminal_id = obj.terminal_id
        locker = obj.locker
        last_balance = insert_flg(obj.last_balance + "")
        card = obj.card_no
        new_amount = insert_flg(amount)
    }

    function add_zero(temp){
        if(temp<10) return "0"+temp;
        else return temp;
    }

    function change_mouth(mouth){
        switch (mouth){
            case "1": print("January")
                break;
            case "2": print("February")
                break;
            case "3": print("March")
                break;
            case "4": print("April")
                break;
            case "5": print("May")
                break;
            case "6": print("June")
                break;
            case "7": print("July")
                break;
            case "8": print("August")
                break ;
            case "9": print("September")
                break;
            case "10": print("October")
                break;
            case "11": print("November")
                break;
            case "12": print("December")
                break;
        }
    }

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    }

    function handle_text(result){
        console.log('sepulsa_transaction_record_no : ' + result)
        if(result == "ERROR"){
            show_timer_thanks.restart()
        }else{
            trx_id = result
        }
    }

    function handle_trx(status){
        console.log('sepulsa_transaction_result : ' + status)
        if(status == ""){
            return
        }
        var obj = JSON.parse(status)
        trx_result = obj.status_text

        if(trx_result!=""){
            text_thank_you_notif_2.text =  "( " + trx_result.toUpperCase() + " )"
            if(trx_result=="Transaksi Telah Sukses"){
                text_thank_you_notif_2.color = "green"
            }else if(trx_result=="Transaksi Sedang Diproses"){
                text_thank_you_notif_2.color = "orange"
            }else{
                text_thank_you_notif_2.color = "blue"
            }
        }
    }

    function check_value(){
        if(payment_result != "" || card != "-" ||
                terminal_id != "-" || locker != "-" ||
                amount != "0" || product_id != "-" ||
                product_type != "-" || phone_no != "-" ||
                last_balance != "-" || service != "-" ||
                new_amount != "-" || tx_date != "-"){
            return true
        }else{
            return false
        }
    }


    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(check_value()){
                    loadingGif.close()
                    main_page.enabled = true
                    ok_button_main_page.enabled = true
                }else{
                    show_payment_result(payment_result)
                    abc.counter = timer_value
                    my_timer.restart()
                }
                if(abc.counter < 0){
                    my_timer.stop()
                    main_page.enabled = false
                    ok_button_main_page.enabled = false
                    show_timer_thanks.start()
                    thank_you_notif.open()
                }
            }
        }
    }

    Rectangle{
        QtObject{
            id:time_thanks
            property int counter
            Component.onCompleted:{
                time_thanks.counter = timer_thanks
            }
        }

        Timer{
            id:show_timer_thanks
            interval:1000
            repeat:true
            running:false
            triggeredOnStart:true
            onTriggered:{
                show_time_thanks.text = time_thanks.counter
                time_thanks.counter -= 1
                //Checking Sepulsa Transaction here after wait 10 seconds
                if(time_thanks.counter == 10){
                    console.log("get transaction final status : ", trx_id)
                    slot_handler.sepulsa_trx_check(trx_id)
                }
                if(time_thanks.counter < 0){
                    show_timer_thanks.stop()
//                    console.log('settle order : ' + phone_no +'-'+ product_id +'-'+ product_type)
//                    slot_handler.start_push_data_settlement(phone_no +'-'+ product_id +'-'+ product_type)
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Rectangle{
        id: main_page
        FullWidthReminderText{
           id:text
           y:140
           remind_text:qsTr("Order Details")
           remind_text_size:"30"
        }

        FullWidthReminderText{
           id:_notif_text1
           x: 0
           y:566
           remind_text:qsTr("The sms notification will be sent to you for this credit purchase")
           remind_text_size:"25"
        }

        DoorButton{
            id:ok_button_main_page
            y:671
            x:373
            show_text:qsTr("OK")
            show_image:"img/door/1.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    main_page.enabled = false
                    ok_button_main_page.enabled = false
                    show_timer_thanks.start()
                    thank_you_notif.open()
                }
                onEntered:{
                    ok_button_main_page.show_image = "img/door/2.png"
                }
                onExited:{
                    ok_button_main_page.show_image = "img/door/1.png"
                }
            }
        }

        Row{
            x:223
            y:200
            spacing:30
            Column{
                spacing:15
                Text {
                    text: qsTr("Card No.")
                    font.family:"Microsoft YaHei"
                    color:"white"
                    font.pixelSize: 25
                }
                Text {
                    text: qsTr("Last Balance")
                    font.family:"Microsoft YaHei"
                    color:"white"
                    font.pixelSize: 25
                }
                Text {
                    text: qsTr("Product Item")
                    font.family:"Microsoft YaHei"
                    color:"white"
                    font.pixelSize: 25
                }
                Text {
                    text: qsTr("Amount")
                    font.family:"Microsoft YaHei"
                    color:"white"
                    font.pixelSize: 25
                }
                Text {
                    text: qsTr("Date")
                    font.family:"Microsoft YaHei"
                    color:"white"
                    font.pixelSize: 25
                }
                Text {
                    text: qsTr("Terminal ID")
                    font.family:"Microsoft YaHei"
                    color:"white"
                    font.pixelSize: 25
                }
                Text {
                    text: qsTr("Locker Name")
                    font.family:"Microsoft YaHei"
                    color:"white"
                    font.pixelSize: 25
                }
            }
            Row{
                spacing:10
                Column{
                    spacing:15
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                    }
                }
                Column{
                    spacing:15
                    Text {
                        text: card.toString()
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: last_balance.toString()
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: product_type + ' - ' + service
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                        font.capitalization: Font.Capitalize
                    }
                    Text {
                        text: new_amount.toString()
                        font.family:"Microsoft YaHei"
                        color:(new_amount=="-") ? "transparent" : "white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: tx_date.toString()
                        font.family:"Microsoft YaHei"
                        color:(tx_date=="-") ? "transparent" : "white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: terminal_id.toString()
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                    }
                    Text {
                        text: locker.toString()
                        font.family:"Microsoft YaHei"
                        color:"white"
                        font.pixelSize: 25
                        font.capitalization: Font.Capitalize
                    }
                }
            }
        }

        FullWidthReminderText {
            id: notif_text2
            x: 0
            y: 612
            remind_text: qsTr("Please do not forget to take your prepaid card")
            remind_text_size: "25"
        }
    }

    LoadingView{
        id: loadingGif
    }

    FloatingWindow{
        id:thank_you_notif
        //visible:true

        Text {
            id: text_thank_you_notif_1
            x:140
            y:224
            width: 750
            height: 100
            text: qsTr("Please keep the transaction number ") + "[" + trx_id.toUpperCase() + "]" + qsTr(" for tracking purpose.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:20
            wrapMode: Text.WordWrap
        }

        Text {
            id: text_thank_you_notif_2
            x:290
            y:330
            width: 600
            height: 123
            //text: "( " + trx_result.toUpperCase() + ")"
            text: qsTr("Please wait a moment for the transaction final status...")
            font.bold: true
            font.italic: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:20
            wrapMode: Text.WordWrap
        }

        Text {
            id: text_thank_you_notif_3
            x:290
            y:469
            width: 600
            height: 70
            text: qsTr("For further assistance, Please contact us at 021-29022537/38.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:20
            wrapMode: Text.WordWrap
        }

        Text {
            id:show_time_thanks
            x:860
            y:516
            width: 40
            height: 40
            font.family:"Microsoft YaHei"
            color:"#ff0000"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.italic: true
            textFormat: Text.PlainText
            font.pointSize:15
            wrapMode: Text.WordWrap
        }
    }
}
