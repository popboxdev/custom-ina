import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: parent_rec
    color:"silver"
    radius: 22
    width:80
    height:80
    property var show_text:""
    property bool status_enable: true

    Rectangle{
        id: opacity_rec
        anchors.fill: parent
        radius: 22
        color: (status_enable==true) ? "silver" : "#404040"
    }
//    Image{
//        id:new_letter_button
//        visible: status_enable
//        width:80
//        height:80
//        source: "img/button/KeyButton_1.png"
//    }

    Text{
        text: (status_enable==true) ? show_text : "-"
        color: (status_enable==true) ? "black" : "silver"
        font.family:"Microsoft YaHei"
        font.pixelSize:24
        anchors.centerIn: parent
        font.italic: true
    }

    MouseArea {
        enabled: status_enable
        anchors.fill: parent
        onClicked: {
            email_keyboard.letter_button_clicked(show_text)
        }
        onEntered:{
            parent_rec.color = "white"
        }
        onExited:{
            parent_rec.color = "silver"
        }
    }

}
