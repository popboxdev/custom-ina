import QtQuick 2.4
import QtQuick.Controls 1.2


Background{
    width: 1024
    height: 768
    property int timer_value: 60
    property var press: "0"
    property bool canProceed: false

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_store_express();
            loadingGif.open();
            abc.counter = timer_value;
            my_timer.restart();
            canProceed = false;
            press = "0";
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop();
//                    slot_handler.start_store_express();
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }));
                }
            }
        }
    }

    FullWidthReminderText {
        x: 0
        y: 200
        remind_text:qsTr("Thank You for using PopBox.")
        remind_text_size:"35"
    }

    FullWidthReminderText {
        x: 0
        y: 250
        remind_text:qsTr("A SMS notification has been sent to")
        remind_text_size:"35"
    }

    FullWidthReminderText {
        x: 0
        y: 300
        remind_text:qsTr("the recipient of this parcel.")
        remind_text_size:"35"
    }

//    OverTimeButton3{
//        id:main_error_button
//        x:389
//        y:576
//        show_text:qsTr("MAIN MENU")
//        show_x:25
//        show_image:"img/07/rewrite.png"
//        MouseArea {
//            anchors.fill: parent
//            onClicked: {
//                if (press != "0") return;
//                press = "1";
//                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }));
//            }
//        }
//    }

    OverTimeButton2{
        id:pass_error_button
        x:232
        y:400
        show_text:qsTr("Send Another Parcel?")
        MouseArea {
            anchors.fill: parent
            enabled: canProceed
            onClicked: {
                if (press != "0") return;
                press = "1";
                slot_handler.start_retry_login();
//                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 4) return true }))
            }
        }
    }

    OverTimeButton2{
        id:pass_button
        x:513
        y:400
        show_text:qsTr("I'm Done!")
       MouseArea {
            anchors.fill: parent
            enabled: canProceed
            onClicked: {
                if (press != "0") return;
                press = "1";
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }));
            }
        }
    }

    Component.onCompleted: {
        root.user_login_result.connect(handle_login_result);
        root.store_express_result.connect(handle_store_parcel);
    }

    Component.onDestruction: {
        root.user_login_result.disconnect(handle_login_result);
        root.store_express_result.disconnect(handle_store_parcel);
    }

    function handle_store_parcel(result){
        console.log('handle_store_parcel : ', result);
        loadingGif.close();
        if (result=='Success') {
            canProceed = true;
        }


    }

    function handle_login_result(result){
        if(result == "Success"){
            my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 3) return true }))
        }
        if(result == "Failure" || result == "NoPermission" || result == "NetworkError"){
            my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
        }
    }

    LoadingView{id: loadingGif}


}
