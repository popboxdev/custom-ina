import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:select_payment
    height: 768
    width: 1024
//    color: 'darkred'
//    img_vis: true
    property int timer_value: 60
    property var press: "0"
    property bool emoney_reader: false
    property bool isNew: true
    property var products
    property var customers
    property var selected_locker: ""
    property var amount: ""
    property var item: ""
    property var customer_data: ""
    property bool emoney: false
    property bool yap: false
    property bool dimo: false
    property bool popsend: false
    property bool tcash: false
    property bool gopay: false
    property bool otto_qr: false
    property var selectedPayment: undefined
    property var product_class: undefined
    property var popsafe_param: undefined
    property var sepulsa_product: undefined

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_get_payment()
            console.log("product : " + products)
            console.log("customer : " +  customers)
            console.log("product_class : " +  product_class)
            console.log("popsafe_param : " +  popsafe_param)
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
            loadingGif.open()
            selectedPayment = undefined
//            slot_handler.start_get_cod_status()
            define_trans_data(products, customers)
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.start_create_payment_result.connect(global_trans_result);
        root.start_get_cod_status_result.connect(cod_result);
        root.get_payment_result.connect(payment_status);
    }

    Component.onDestruction: {
        root.start_create_payment_result.disconnect(global_trans_result);
        root.start_get_cod_status_result.disconnect(cod_result);
        root.get_payment_result.disconnect(payment_status);
    }

    function payment_status(r){
        console.log('payment_result', r)
        var p = JSON.parse(r)
        var d = p.data
        for (var i in d){
            if (d[i].status=='OPEN'){
                if (d[i].code=='BNI-YAP') yap = true;
                if (d[i].code=='MANDIRI-EMONEY') emoney = true;
                if (d[i].code=='DIMO-QR') dimo = true;
                if (d[i].code=='POPSEND-WALLET') popsend = true;
                if (d[i].code=='OTTO-QR') otto_qr = true;
                if (d[i].code=='TCASH') tcash = true;
                if (d[i].code=='MID-GOPAY') gopay = true;
            }
        }

        // force open gopay_button --------
        // gopay = true
        // --------------------------------

        slot_handler.start_get_cod_status()
        console.log("emoney status payment_status : ", emoney, emoney_reader)
        loadingGif.close();
    }

    function global_trans_result(result){
        console.log("global_trans_result is ", result)
        if (result==""){
            return
        } else {
            if ( popsafe_param != undefined ){
                    var r = JSON.parse(result);
                    var popsafe = JSON.parse(popsafe_param)
                    popsafe.transactionRecord = r.transaction_id+"|"+r.payment_id
                    popsafe.paymentMethod = selectedPayment
                    popsafe_param = JSON.stringify(popsafe)
            }
            console.log("popsafe_param : ", selectedPayment, popsafe_param)
            loadingGif.close()
            my_stack_view.push(qr_payment_channel, {products: products,
                                    provider: selectedPayment,
                                    global_trans: result,
                                    product_class: product_class,
                                    customers: customers,
                                    selected_locker: selected_locker,
                                    popsafe_param: popsafe_param
                                })
        }
    }

    function cod_result(result){
//        console.log("cod_result is", result)
        if(result=="") return
        if(result=="enabled"){
            emoney_reader = true;
            console.log("emoney status cod_result : ", result, emoney, emoney_reader)
        }
    }

    function define_trans_data(objA, objB){
        var product = JSON.parse(objA)
        amount = product.total_amount
        item = product.sku + '|' + product.name + '|' + product.qty
        var customer = JSON.parse(objB)
        customer_data = customer.name + '||' + customer.phone + '||' + customer.email
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                //TODO Check referer page after action
                my_stack_view.pop()
            }
        }
    }

    Rectangle{
        id: main_page
        FullWidthReminderText{
            id:page_title
            y:148
            remind_text:qsTr("Please Select Payment Option")
            remind_text_size:"45"
        }

        Grid{
            id: buttons_group
            x: 12
            y: 247
            width: 1000
            height: 500

            OtherServiceButton{
                id:bni_yap
                visible: yap
                width: 245
                height: 235
                scale: 0.8
                show_image:"img/payment/bni-yap.png"
                show_text:qsTr("BNI YAP!")
                show_text_color:"#ab312e"

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        my_timer.stop()
                        loadingGif.open()
                        selectedPayment = 'BNI-YAP'
                        slot_handler.start_create_payment_yap(amount.toString(), customer_data, item, selected_locker)
                    }
                }
            }

            OtherServiceButton{
                id:mandiri_emoney
                visible: emoney
                width: 245
                height: 235
                scale: 0.8
                show_image:(emoney_reader==true) ? "img/payment/mandiri-emoney.png" : "img/payment/emoney-gray.png"
                show_text:qsTr("E-Money")
                show_text_color:"#ab312e"

                MouseArea {
                    anchors.fill: parent
                    enabled: emoney_reader
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        my_timer.stop()
                        selectedPayment = 'MANDIRI-EMONEY'
                        my_stack_view.push(grocery_express_info, {products:products,
                                               selected_locker: selected_locker,
                                               customers: customers,
                                               payment_channel: selectedPayment,
                                               popsafe_param: popsafe_param,
                                               usageOf: product_class,
                                               data_product: sepulsa_product
                                           })
                    }
                }
            }

            OtherServiceButton {
                id: pay_by_qr
                visible: dimo
                width: 245
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                show_text: qsTr("PAY BY QR")
                show_image: "img/payment/dimo-paybyqr.png"

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        selectedPayment = 'DIMO-PAYBYQR'
                        my_timer.stop()
                        my_stack_view.push(qr_payment_channel, {products: products,
                                               provider: selectedPayment,
                                               product_class: product_class,
                                               customers: customers,
                                               popsafe_param: popsafe_param
                                           })
                    }
                }
            }

            OtherServiceButton {
                id: popsend_wallet
                visible: popsend
                width: 245
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                show_text: qsTr("PopBox Wallet")
                show_image: "img/payment/popbox-popsend.png"

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        loadingGif.open()
                        selectedPayment = 'POPSEND-WALLET'
                        my_timer.stop()
                        my_stack_view.push(on_develop_view)
                    }
                }
            }

            OtherServiceButton {
                id: tcash_button
                visible: tcash
                width: 245
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                show_text: qsTr("TCASH QR")
                show_image: "img/payment/tcash_qr_white.png"

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        loadingGif.open()
                        selectedPayment = 'TCASH'
                        my_timer.stop()
                        slot_handler.start_create_payment_tcash(amount.toString(), customer_data, item, selected_locker)
                    }
                }
            }

            OtherServiceButton {
                id: ottopay_button
                visible: otto_qr
                width: 245
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                show_text: qsTr("OTTO QR")
                show_image: "img/payment/otto_qr_white.png"

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        loadingGif.open()
                        selectedPayment = 'OTTO-QR'
                        my_timer.stop()
                        slot_handler.start_create_payment_ottoqr(amount.toString(), customer_data, item, selected_locker)
                    }
                }
            }

            OtherServiceButton {
                id: gopay_button
                visible: gopay
                width: 245
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                show_text: qsTr("GOPAY")
                show_image: "img/payment/gopay_qr_white.png"

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "0") return
                        press = "1"
                        loadingGif.open()
                        selectedPayment = 'GOPAY'
                        my_timer.stop()
                        slot_handler.start_create_payment_gopay(amount.toString(), customer_data, item, selected_locker)
                    }
                }
            }

        }

        Text {
            id: powered_text
            x: 228
            y: 638
            width: 250
            height: 40
            color: "#ffffff"
            text: qsTr("Powered by :")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 30
            font.family: "Microsoft YaHei"
            visible: !isNew
        }

        Rectangle {
            id: rec_emoney
            x: 488
            y: 619
            width: 300
            height: 77
            color: "#ffffff"
            visible: !isNew

            Image {
                id: image_emoney
                x: 0
                y: 0
                width: 300
                height: 77
                source: "img/otherservice/logo-emoney300.png"
            }
        }

}

    LoadingView{
        id: loadingGif
    }

}
