import QtQuick 2.4
import QtQuick.Controls 1.2


Background{

    property int timer_value: 60
    property var press: "0"
    property string designationSize: "S"


    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            octopus.show_source = "img/button/1.png"
            coin.show_source = "img/button/1.png"
            app.show_source = "img/button/1.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


    Column{
        x:118
        y:170

        FullWidthReminderText{
            width: 800
            transformOrigin: Item.Center
            //remind_text:qsTr("Please select the type of payment")
            remind_text:qsTr("Please choose the method of payment")
            remind_text_size:"40"
        }

        FullWidthReminderText{
            width: 800
            //remind_text:qsTr("If you have any questions, please call 888888")
            remind_text:qsTr("If you have any questions, please contact ********")
            remind_text_size:"30"
        }
    }

    SelectServiceButton{
        id:octopus
        x:89
        y:300
        show_text:qsTr("octopus")
        show_image:"img/26/octopus1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                octopus.show_source = "img/26/down.png"
                coin.show_source = "img/button/1.png"
                app.show_source = "img/button/1.png"

                my_timer.stop()

                my_stack_view.push(on_develop_view)
            }
        }
    }

    SelectServiceButton{
        id:coin
        x:372
        y:300
        show_text:qsTr("coin")
        show_image:"img/26/coin1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                octopus.show_source = "img/button/1.png"
                coin.show_source = "img/26/down.png"
                app.show_source = "img/button/1.png"

                my_timer.stop()
                //my_stack_view.push(on_develop_view)
                my_stack_view.push(send_inser_coin_pagee)
            }
        }
    }

    SelectServiceButton{
        id:app
        x:655
        y:300
        show_text:qsTr("app")
        show_image:"img/26/app1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                octopus.show_source = "img/button/1.png"
                coin.show_source = "img/button/1.png"
                app.show_source = "img/26/down.png"

                my_timer.stop()
                my_stack_view.push(on_develop_view)
            }
        }
    }


    OverTimeButton{
        id:back_button
        x:372
        y:620
        show_text:qsTr("back to menu")
        show_x:0
        show_image:"img/05/back.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                back_button.show_source = "img/05/pushdown.png"
            }
            onExited:{
                back_button.show_source = "img/05/button.png"
            }
        }
    }
}
