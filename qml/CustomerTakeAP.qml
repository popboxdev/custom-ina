import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    id: baseAP
    property var press: '0'
    watermark: true
    topPanelColor: 'GREEN'
    mainMode: false
    property var door_no: '10'
    property int timer_value: 60
    property int reOpenButton: 0
    property int count:2
    property int val

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            abc.counter = timer_value;
            my_timer.start();
            reOpenButton = 0;
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
        }
    }

    Component.onCompleted: {
    }

    Component.onDestruction: {

    }


    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }
    }

    Rectangle{
        id: timer_set
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                take_time_.text = abc.counter + qsTr(' Detik')
                if(abc.counter <= 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


    Text {
        id: main_command_text
        width: 500
        height: 100
        color: "#323232"
        text: qsTr("Ambil Barang")
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 200
        font.bold: true
        font.family: 'Microsoft YaHei'
        font.pixelSize: 50

    }

    Image {
        id: icon_confirm
        scale: 0.85
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -10
        anchors.right: parent.right
        anchors.rightMargin: -35
        source: "img/apservice/img/customer_take.PNG"
    }

    Column{
        id: details_text
        anchors.verticalCenterOffset: 10
        anchors.left: parent.left
        anchors.leftMargin: 44
        anchors.verticalCenter: parent.verticalCenter
        spacing: 60
        RowTextAP{
            labelText: qsTr('No Pintu')
            contentText: ''
            colorContent: '#323232'
            contentBold: true
            separatorPoint: 250
        }
        RowTextAP{
            labelText: qsTr('Hitung Mundur')
            contentText: ''
            colorContent: '#009BE1'
            contentBold: true
            separatorPoint: 250
        }
    }

    Column{
        id: details2_text
        anchors.verticalCenterOffset: 60
        anchors.left: parent.left
        anchors.leftMargin: 55
        anchors.verticalCenter: parent.verticalCenter
        spacing: 60
        Text {
            id: door_no_
            text: door_no
            font.pixelSize: 50
            font.bold: true
            color: '#65B82E'
            font.family: 'Microsoft YaHei'
        }
        Text {
            id: take_time_
            text: abc.counter + qsTr(' Detik')
            font.pixelSize: 30
            font.italic: false
            color: '#65B82E'
            font.family: 'Microsoft YaHei'

        }

    }
    Image {
        id: step_progress
        width: 500
        height: 51
        anchors.top: parent.top
        anchors.topMargin: 115
        anchors.horizontalCenter: parent.horizontalCenter
        source: "img/apservice/langkah/ambil03.png"
    }


    NotifButtonAP{
        id: ok_button
        x: 365
        y: 644
        buttonColor: 'GREEN'
        modeReverse: false
        buttonText: qsTr('SELESAI')
        MouseArea{
            anchors.fill: parent
            onClicked: {
                my_timer.stop();
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }));
            }
        }
    }

    NotifButtonAP{
        id: reopen_button
        x: 50
        y: 644
        buttonColor: (reOpenButton>1) ? 'GRAY' : 'GREEN'
        modeReverse: true
        buttonText: qsTr('BUKA KEMBALI')
        enabled: (reOpenButton>1) ? false : true
        MouseArea{
            anchors.fill: parent
            onClicked: {
                reOpenButton += 1;
                val = count - reOpenButton;
                availableKlik.text = val
                slot_handler.start_open_mouth_again();
                console.log('reopen_button is pressed')
            }
        }
    }

    Rectangle{
        id: klik_avail
        x: 70
        y: 725
        color: "transparent"

        Row {
            id: avail_doors_view
            x: 0
            y: 0   
            spacing: 5

            Text {
                id: mouth_title
                text: qsTr("Sisa buka kembali:  ")
                font.family:"Microsoft YaHei"
                font.bold: false
                color:"black"
                font.pixelSize: 20
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            Row {
                spacing: 5
                Text {
                    id: availableKlik
                    text: qsTr("2")
                    font.family:"Microsoft YaHei"
                    font.bold: false
                    color:"black"
                    font.pixelSize: 20
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                }
            }
          
        }
    }

    Text {
        id: perhatian
        x:50
        y:535
        color: "black"
        text: qsTr("Pastikan barang Anda tidak ada yg tertinggal dan jangan\nlupa tutup pintu kembali. Anda diberikan 3 kali\nkesempatan buka pintu selama 1 menit")
        anchors.left: parent.left
        anchors.leftMargin: 50
        font.bold: false
        font.family: 'Microsoft YaHei'
        font.pixelSize: 17

    }
    // Put Main Component Here


    Notification{
        id: notif_text
        contentNotif: qsTr('Terima Kasih telah menggunakan Loker PopBox.')
        successMode: true
    }
}
