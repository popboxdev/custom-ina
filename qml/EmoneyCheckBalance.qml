import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: emoney_check_balance
    property int timer_value: 15
    property var press:"0"

    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
            group_img_emoney.enabled = true
            ok_button.enabled = true
            back_button.enabled = true
            error_notif.close()
            no_balance_notif.close()
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_customer_info_by_card_result.connect(card_info)
    }

    Component.onDestruction: {
        root.start_get_customer_info_by_card_result.disconnect(card_info)
    }

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    }

    function card_info(result){
        console.log("card_balance : "+ insert_flg(result + ""))
        if(result == "EMPTY"){
            loading.close()
            group_img_emoney.enabled = false
            ok_button.enabled = false
            back_button.enabled = false
            no_balance_notif.open()
        }else if(result == "ERROR" || result == "WRONG-CARD"){
            loading.close()
            group_img_emoney.enabled = false
            ok_button.enabled = false
            back_button.enabled = false
            error_notif.open()
        }else{
            my_stack_view.push(emoney_balance_info, {balance:insert_flg(result + "")})
        }
    }

    Rectangle{
        id: timer_box
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Text{
            id:show_time
            x:13
            y:727
            width: 24
            height: 24
            font.family:"Microsoft YaHei"
            color:"#f8f400"
            font.italic: true
            textFormat: Text.PlainText
            font.pointSize:15
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                show_time.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

   BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
            onEntered:{
                back_button.show_source = "img/bottondown/down.2.png"
            }
            onExited:{
                back_button.show_source = "img/button/7.png"
            }
        }
    }

   FullWidthReminderText{
       id:title_text
       x: 0
       y:147
       remind_text:qsTr("Balance Information")
       remind_text_size:"45"
   }


    Rectangle {
        id: group_img_emoney
        x: 54
        y: 244
        width: 300
        height: 301
        color: "#00000000"

        Rectangle {
            id: rectagle_reader
            width: 900
            height: 350
            color: "#ffffff"
            opacity: 0.85
            radius: 10
            border.width: 0
        }

        Image {
            id: reader_image
            x: 14
            y: 0
            width: 300
            height: 301
            opacity: 1
            fillMode: Image.PreserveAspectFit
            source: "img/otherservice/icon_reader_portrait.png"
        }

        Image {
            id: emoney
            x: 75
            y: 270
            width: 150
            height: 50
            fillMode: Image.PreserveAspectFit
            sourceSize.width: 0
            source: "img/otherservice/logo-emoney300.png"
        }

        Text {
            id: notify_text
            x: 313
            y:40
            width: 578
            height: 234
            text:qsTr("Please place your pre-paid card into the reader now before continuing.")
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"red"
            font.pixelSize: 30
        }
    }

    DoorButton{
        id:ok_button
        y:616
        x:373
        show_text:qsTr("Next")
        show_image:"img/bottondown/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                loading.open()
                press = "1"
                ok_button.enabled = false
                back_button.enabled = false
                slot_handler.start_get_customer_info_by_card()
            }
            onEntered:{
                ok_button.show_image = "img/bottondown/down_1.png"
            }
            onExited:{
                ok_button.show_image = "img/bottondown/1.png"
            }
        }
    }


    LoadingView{
        id:loading
    }

    HideWindow{
        id:no_balance_notif
        //visible:true

        Text {
            id: text_your_prepaid
            x: 93
            y:434
            width: 850
            height: 50
            text: qsTr("Your prepaid balance is 0")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            id: text_please_topup
            x: 93
            y:484
            width: 850
            height: 50
            text: qsTr("Please top up your prepaid card.")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: img_no_balance
            x: 410
            y: 213
            width: 215
            height: 215
            fillMode: Image.PreserveAspectFit
            source: "img/otherservice/no_credit.png"
        }

        OverTimeButton{
            id:no_balance_back
            x:378
            y:589
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
                }
                onEntered:{
                    no_balance_back.show_source = "img/bottondown/down_1.png"
                }
                onExited:{
                    no_balance_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:error_notif
        //visible:true

        Text {
            id: text_error_1
            x: 93
            y:434
            width: 850
            height: 50
            text: qsTr("Balance Check Failed")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            id: text_error_2
            x: 93
            y:484
            width: 800
            height: 100
            text: qsTr("Please use e-Money Mandiri which placed properly into the reader.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:25
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: img_error_notif
            x: 410
            y: 213
            width: 215
            height: 215
            fillMode: Image.PreserveAspectFit
            source: "img/otherservice/no_credit.png"
        }

        OverTimeButton{
            id:no_error_notif
            x:378
            y:589
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    ok_button.enabled = true
                    back_button.enabled = true
                    my_stack_view.pop()
                    //my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
                }
                onEntered:{
                    no_error_notif.show_source = "img/bottondown/down_1.png"
                }
                onExited:{
                    no_error_notif.show_source = "img/button/7.png"
                }
            }
        }
    }

}
