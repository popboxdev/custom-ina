import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: rec_base
    width:150
    height:53
    color:"transparent"
    property var button_text:"Jakarta Selatan"
    property var button_color:"white"


    Rectangle{
        id: rec_subbase
        anchors.fill: parent
        width: parent.width
        height: parent.height
        radius: 10
        color: button_color
        border.width: 3
        border.color: "#00000000"
    }

    Text {
        id: txt_button
        anchors.fill: parent
        width: parent.width
        height: parent.height
        color:(button_color=="white") ? "red" : "white"
        text:button_text
        font.bold: true
        textFormat: Text.PlainText
        font.family: "Microsoft YaHei"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 15
    }
}
