import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:250
    height:200
    color:"transparent"

    property var show_text:""
    property var show_image:""
    property var show_source:""

    Image{
        x:0
        y:0
        width:250
        height:200
        //source:"img/courier14/14ground.png"
        source:show_source
    }

    Image{
        x:0
        y:0
        width:250
        height:200
        //source:"img/courier14/14ground.png"
        source:show_image
    }
}
