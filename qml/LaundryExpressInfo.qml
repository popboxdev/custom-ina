import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: bground_laundryinfo
    width: 1024
    height: 768
    property var show_name:""
    property var show_address:""
    property var show_provider:"PopBox Service"
    property var memory:""
    property string send_data:'{"id":"f37fa91cf7a211e4b7926c40088b8482","customerStoreNumber":"P201500010001","expressType":"CUSTOMER_STORE","storeUser":{"id":"ace76ad2f7a211e49b736c40088b8482"},"recipientName":"高阳","takeUserPhoneNumber":"18566691650","startAddress":{"id":"48eb7c50f7a311e49ec16c40088b8482","region":{"id":"35865af8f66411e4bd404ef6eab0b474","name":"南山区","parentRegion":{"id":"e4e99678f66311e4bd404ef6eab0b474","name":"鹤岗","parentRegion":{"id":"b3b57ab8f66311e4bd404ef6eab0b474","name":"黑龙江","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"detailedAddress":"高新产业园"},"endAddress":{"id":"2494f228f7a311e4bb216c40088b8482","region":{"id":"358b9c7af66411e4bd404ef6eab0b474","name":"宝安区","parentRegion":{"id":"e4ea8862f66311e4bd404ef6eab0b474","name":"深圳","parentRegion":{"id":"b3b582cef66311e4bd404ef6eab0b474","name":"广东","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"detailedAddress":"任达科技园"},"rangePrice":{"id":"a4ded9a6f7a011e48bfb6c40088b8482","businessType":{"id":"ca6ef91cf7a011e490516c40088b8482","name":"今日送"},"startRegion":{"id":"35865af8f66411e4bd404ef6eab0b474","name":"南山区","parentRegion":{"id":"e4e99678f66311e4bd404ef6eab0b474","name":"鹤岗","parentRegion":{"id":"b3b57ab8f66311e4bd404ef6eab0b474","name":"黑龙江","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"endRegion":{"id":"358b9c7af66411e4bd404ef6eab0b474","name":"宝安区","parentRegion":{"id":"e4ea8862f66311e4bd404ef6eab0b474","name":"深圳","parentRegion":{"id":"b3b582cef66311e4bd404ef6eab0b474","name":"广东","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"firstHeavy":5000,"firstPrice":1000,"continuedHeavy":1000,"continuedPrice":200},"version":0,"status":"IMPORTED"}'
    property string chargeType:"BY_WEIGHT"
    property string designationSize: "S"
    property int payOfAmount:0
    property var provider_data:'[{"pref_order":"LBA","prov_name":"Laba-laba"},{"pref_order":"VCS","prov_name":"Vcleanshoes"},{"pref_order":"TAP","prov_name":"Taptopick Laundry"},{"pref_order":"KNK","prov_name":"KliknKlin Laundry"},{"pref_order":"OMS","prov_name":"Omaisu - Shoe Repair"},{"pref_order":"TYK","prov_name":"Tayaka Laundry"}]'
    property int timer_value: 60
    property var locker_name: ""

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            slot_handler.start_get_locker_name()
            show_recive_info(send_data,provider_data)
            root.choose_mouth_result.connect(handle_text)
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            root.choose_mouth_result.disconnect(handle_text)
            my_timer.stop()
        }
    }

    Rectangle{
        x:50
        y:50

        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Component.onCompleted: {
        root.free_mouth_by_size_result.connect(check_free_mouth)
        root.start_get_locker_name_result.connect(show_locker_name)
    }

    Component.onDestruction: {
        root.free_mouth_by_size_result.disconnect(check_free_mouth)
        root.start_get_locker_name_result.disconnect(show_locker_name)

    }

    function show_recive_info(send_data,provider_data){
        if(send_data==""){
            return
        }

        var a = JSON.parse(send_data)
        show_name = a.recipientName
        //show_phone = a.recipientUserPhoneNumber
        show_address = a.endAddress
        chargeType = a.chargeType

        if(chargeType == "FIXED_VALUE"){
            payOfAmount = a.payOfAmount
            if("designationSize" in a){
                if(a.designationSize == ""){
                	designationSize = a.designationSize
                }else{
                	designationSize = "none"
                }
            }else{
                designationSize = "none"
            }
        }

        if(a.chargeType == "NOT_CHARGE"){
            if("designationSize" in a){
                if(a.designationSize == ""){
                	designationSize = "none"
                }else{
                	designationSize = a.designationSize
                }
            }else{
                designationSize = "none"
            }
        }

        //Parsing Provider Value Name
        var prov_list = JSON.parse(provider_data)

        for(var i in prov_list){
            if(memory.substring(0,3) == prov_list[i].pref_order){
                show_provider = prov_list[i].prov_name
            }
        }     
    }

    function check_free_mouth(text){
        if(text == "Failure"){
            ok_button.enabled = false
            back_button.enabled = false
            not_mouth.open()
        }
        if(text == "Success"){
            slot_handler.start_pull_pre_pay_cash_for_customer_express(payOfAmount)
            my_stack_view.push(send_pay_page,{designationSize:designationSize})
        }
    }

    function handle_text(text){
            if(text == 'Success'){
                my_stack_view.push(laundry_door_open_page)
            }
            if(text == 'NotMouth'){
                ok_button.enabled = false
                back_button.enabled = false
                not_mouth.open()
            }
    }

    function show_locker_name(text){
        if(text == ""){
            return
        }
        locker_name = 'PopBox @ ' + text
    }

    function validate_locker(x,y){
        var check1 = x.toLowerCase().replace(/\s+|a|e|i|u|o|-|[.\,\:\+\&\//\;\@\\!]/g, "")
        var check2 = y.toLowerCase().replace(/\s+|a|e|i|u|o|-|[.\,\:\+\&\//\;\@\\!]/g, "")
        if(check1==check2 ||
                check1.slice(-5)==check2.slice(-5) ||
                check1.indexOf(check2) > -1 ||
                check2.indexOf(check1) > -1){
            console.log("MATCH >> " + check1 + ' VS ' + check2)
            return true
        }else{
            console.log("NOT MATCH >> " + check1 + ' VS ' + check2)
            return false
        }
    }

    Text {
        id: info_title
        x: 314
        y: 133
        width: 400
        text: qsTr("Laundry Details")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.family:"Microsoft YaHei"
        color:"#FFFFFF"
        font.pixelSize: 40
    }

    GroupBox {
        id: group_text
        x: 33
        y: 200
        width: 936
        height: 403
        flat: true
        checkable: false
        checked: false
        //title: qsTr("Group Box")


        Text {
            id: provider_title
            x: 193
            y: 0
            width: 180
            height: 50
            color: "#ffffff"
            text: qsTr("Provider :")
            anchors.right: provider_value.left
            anchors.rightMargin: 29
            font.family: "Microsoft YaHei"
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 35
        }

        Text {
            id: provider_value
            x: 402
            y: 0
            width: 400
            height: 50
            color: "#ffffff"
            text: show_provider
            font.pixelSize: 35
            font.family: "Microsoft YaHei"
        }

        Text {
            id: order_title
            x: 193
            y: 53
            width: 180
            height: 50
            color: "#ffffff"
            text: qsTr("Order No.:")
            anchors.right: order_value.left
            font.pixelSize: 35
            horizontalAlignment: Text.AlignRight
            anchors.rightMargin: 29
            font.family: "Microsoft YaHei"
        }

        Text {
            id: order_value
            x: 402
            y: 53
            width: 400
            height: 50
            color: "#ffffff"
            text: memory
            font.pixelSize: 35
            font.family: "Microsoft YaHei"
        }

        Text {
            id: name_title
            x: 193
            y: 106
            width: 180
            height: 50
            text: qsTr("Customer :")
            horizontalAlignment: Text.AlignRight
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            font.pixelSize: 35
            anchors.right: name_value.left
            anchors.rightMargin: 29
        }

        Text {
            id: name_value
            x: 402
            y: 106
            width: 400
            height: 50
            color:"#FFFFFF"
            text: show_name
            font.pixelSize: 35
            font.family:"Microsoft YaHei"
        }

        Text {
            id: address_title
            x: 193
            y: 159
            width: 180
            height: 50
            text: qsTr("Address :")
            horizontalAlignment: Text.AlignRight
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            font.pixelSize: 35
            anchors.right: address_value.left
            anchors.rightMargin: 31
        }

        Text {
            id: address_value
            x: 404
            y: 159
            width:500
            height: 250
            text: show_address
            verticalAlignment: Text.AlignTop
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            font.pixelSize: 35
            wrapMode: Text.WordWrap
        }
    }

    Image {
        id: img_cloths
        x: 45
        y: 162
        width: 125
        height: 125
        z: 0
        rotation: -13
        source: "img/item/folded_cloths.png"
    }

    Image {
        id: img_cloths1
        x: 880
        y: 221
        width: 100
        height: 100
        scale: 1
        source: "img/item/short_pant.png"
        rotation: 15
        z: 0
    }

    DoorButton{
        id:back_button
        y:646
        x:176
        show_text:qsTr("Back")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 2) return true }))
            }
            onEntered:{
                back_button.show_image = "img/door/2.png"
            }
            onExited:{
                back_button.show_image = "img/door/1.png"
            }
        }
    }

    DoorButton{
        id:ok_button
        y:646
        x:558
        show_text:qsTr("OK")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                console.log('locker_name : ' + locker_name + ', show_address : ' + show_address)
                if(validate_locker(locker_name, show_address)){
                    if(chargeType == "BY_WEIGHT"){
                        my_stack_view.push(customer_send_express_view)
                    }
                    if(chargeType == "NOT_CHARGE"){
                        if(designationSize == "none"){
                            my_stack_view.push(laundry_select_box)
                        }else{
                            slot_handler.start_choose_mouth_size(designationSize,"customer_store_express")
                        }
                    }
                    if(chargeType == "FIXED_VALUE"){
                        if(payOfAmount == 0){
                            no_payOfAmount.open()
                        }
                        if(payOfAmount != 0){
                            slot_handler.start_free_mouth_by_size(designationSize)
                        }
                    }
                }else{
                    non_choosen_locker.open()
                    back_button.enabled = false
                    ok_button.enabled = false
                }
            }
            onEntered:{
                ok_button.show_image = "img/door/2.png"
            }
            onExited:{
                ok_button.show_image = "img/door/1.png"
            }
        }
    }

    HideWindow{
        id:not_mouth
        //visible:true
        Text {
            x: 92
            y:332
            width: 850
            height: 110
            text: qsTr("not_mouth")
            font.family:"Microsoft YaHei"
            color:"#444586"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:not_mouth_back
            x:350
            y:560
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    ok_button.enabled = true
                    back_button.enabled = true
                    not_mouth.close()
                }
                onEntered:{
                    not_mouth_back.show_source = "img/bottondown/down_1.png"
                }
                onExited:{
                    not_mouth_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:no_payOfAmount
        //visible:true
        Text {
            x: 92
            y:330
            width: 850
            height: 112
            text: qsTr("no_payOfAmount")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:no_payOfAmount_back
            x:354
            y:558
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    ok_button.enabled = true
                    back_button.enabled = true
                    no_payOfAmount.close()
                }
                onEntered:{
                    no_payOfAmount_back.show_source = "img/bottondown/down_1.png"
                }
                onExited:{
                    no_payOfAmount_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:non_choosen_locker
        //visible: true

        Text {
            id: we_are_sorry_text
            x: 96
            y: 251
            width: 848
            height: 50
            color: "#ffffff"
            text: qsTr("We are sorry")
            textFormat: Text.PlainText
            font.pointSize: 35
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
            font.family: "Microsoft YaHei"
        }

        Text {
            id: notif_text_non_choosen_locker
            x: 96
            y:325
            width: 848
            height: 200
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            text: qsTr("This order cannot be processed, Please ensure you have choose the right locker.")
            textFormat: Text.PlainText
            font.pointSize:32
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        /*OverTimeButton{
            id:non_choosen_yes_button
            x:546
            y:576
            show_text:qsTr("Yes")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    non_choosen_locker.close()
                    if(chargeType == "BY_WEIGHT"){
                        my_stack_view.push(customer_send_express_view)
                    }
                    if(chargeType == "NOT_CHARGE"){
                        if(designationSize == "none"){
                            my_stack_view.push(laundry_select_box)
                        }else{
                            slot_handler.start_choose_mouth_size(designationSize,"customer_store_express")
                        }
                    }
                    if(chargeType == "FIXED_VALUE"){
                        if(payOfAmount == 0){
                            no_payOfAmount.open()
                        }
                        if(payOfAmount != 0){
                            slot_handler.start_free_mouth_by_size(designationSize)
                        }
                    }
                }
                onEntered:{
                    non_choosen_yes_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    non_choosen_yes_button.show_source = "img/button/7.png"
                }
            }
        }*/

        OverTimeButton{
            id:non_choosen_back_button
            x:381
            y:581
            show_text:qsTr("Back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 2) return true }))
                }
                onEntered:{
                    non_choosen_back_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    non_choosen_back_button.show_source = "img/button/7.png"
                }
            }
        }
    }
}
