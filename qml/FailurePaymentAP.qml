import QtQuick 2.4

BaseAP{
    id: main_rectangle
    watermark: true
    topPanelColor: 'BLUE'
    property alias titleText: failure_text.text
    property alias notifText: notif_text.text
    property alias buttonCancel: cancel_button.visible
    property var buttonCobaText: qsTr('COBA LAGI')
    property var buttonCloseText: qsTr('BATAL')
    mainMode: false
    width: 1024
    height: 768
    visible: false

    Image{
        id: notif_img
        scale: 0.8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -10
        anchors.right: parent.right
        anchors.rightMargin: -50
        source: "img/apservice/img/failed.PNG"
    }

    Text {
        id: failure_text
        width: 500
        height: 100
        color: "#323232"
        text: qsTr("Pembayaran Gagal")
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 200
        font.bold: true
        font.family: 'Microsoft YaHei'
        font.pixelSize: 50

    }

    Text{
        id: notif_text
        y: 179
        width: 500
        height: 200
        font.family: 'Microsoft YaHei'
        font.pixelSize: 30
        color: '#323232'
        text: "Mohon Maaf, Transaksi Anda gagal. Silakan Ulangi Pembayaran."
        anchors.verticalCenterOffset: 30
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.verticalCenter: parent.verticalCenter
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    NotifButtonAP{
        id: coba_lagi
        x: 350
        y: 640
        buttonColor: parent.topPanelColor
        buttonText: buttonCobaText
        MouseArea{
            anchors.bottomMargin: 0
            anchors.fill: parent
            onClicked: {
                close();
            }
        }
    }

    NotifButtonAP{
        id: cancel_button
        x: 50
        y: 640
        visible: true
        buttonColor: parent.topPanelColor
        modeReverse: true
        buttonText: buttonCloseText
        MouseArea{
            anchors.bottomMargin: 0
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
                my_stack_view.pop()
                my_stack_view.pop()
            }
        }
    }


    function open(){
        main_rectangle.visible = true;
    }

    function close(){
        main_rectangle.visible = false;
    }

}





