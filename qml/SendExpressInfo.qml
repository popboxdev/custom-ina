import QtQuick 2.4
import QtQuick.Controls 1.2

Background {
    id: bground_sendinfo
    width: 1024
    height: 768
    property int timer_value: 60
    property var show_name: ""
    property var show_address: ""
    property string send_data: '{"id":"f37fa91cf7a211e4b7926c40088b8482","customerStoreNumber":"P201500010001","expressType":"CUSTOMER_STORE","storeUser":{"id":"ace76ad2f7a211e49b736c40088b8482"},"recipientName":"高阳","takeUserPhoneNumber":"18566691650","startAddress":{"id":"48eb7c50f7a311e49ec16c40088b8482","region":{"id":"35865af8f66411e4bd404ef6eab0b474","name":"南山区","parentRegion":{"id":"e4e99678f66311e4bd404ef6eab0b474","name":"鹤岗","parentRegion":{"id":"b3b57ab8f66311e4bd404ef6eab0b474","name":"黑龙江","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"detailedAddress":"高新产业园"},"endAddress":{"id":"2494f228f7a311e4bb216c40088b8482","region":{"id":"358b9c7af66411e4bd404ef6eab0b474","name":"宝安区","parentRegion":{"id":"e4ea8862f66311e4bd404ef6eab0b474","name":"深圳","parentRegion":{"id":"b3b582cef66311e4bd404ef6eab0b474","name":"广东","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"detailedAddress":"任达科技园"},"rangePrice":{"id":"a4ded9a6f7a011e48bfb6c40088b8482","businessType":{"id":"ca6ef91cf7a011e490516c40088b8482","name":"今日送"},"startRegion":{"id":"35865af8f66411e4bd404ef6eab0b474","name":"南山区","parentRegion":{"id":"e4e99678f66311e4bd404ef6eab0b474","name":"鹤岗","parentRegion":{"id":"b3b57ab8f66311e4bd404ef6eab0b474","name":"黑龙江","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"endRegion":{"id":"358b9c7af66411e4bd404ef6eab0b474","name":"宝安区","parentRegion":{"id":"e4ea8862f66311e4bd404ef6eab0b474","name":"深圳","parentRegion":{"id":"b3b582cef66311e4bd404ef6eab0b474","name":"广东","parentRegion":{"id":"a0bd0d9af66311e4bd404ef6eab0b474","name":"中国"}}}},"firstHeavy":5000,"firstPrice":1000,"continuedHeavy":1000,"continuedPrice":200},"version":0,"status":"IMPORTED"}'
    property string chargeType: "BY_WEIGHT"
    property string designationSize: "none"
    property int payOfAmount: 0
    property var locker_name: ""
    property var show_locker: ""
    property var addresses: ""
    property var split_address: ""
    property var delimiter: " ]==)> "
    property bool isPopDeposit: false
    property var customerStoreNumber: undefined
    property var titleText: qsTr("PopSend Details")


    Stack.onStatusChanged: {
        if (Stack.status == Stack.Activating) {
//            console.log("popsend json data : " + JSON.stringify(send_data))
            define_parcel_data(send_data)
            root.choose_mouth_result.connect(handle_text)
            abc.counter = timer_value
            my_timer.restart()
        }
        if (Stack.status == Stack.Deactivating) {
            root.choose_mouth_result.disconnect(handle_text)
            my_timer.stop()
        }
    }

    Rectangle{
        x:50
        y:50

        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Component.onCompleted: {
        root.free_mouth_by_size_result.connect(check_free_mouth)
    }

    Component.onDestruction: {
        root.free_mouth_by_size_result.disconnect(check_free_mouth)
    }

    function define_parcel_data(send_data) {
        console.log('Recipient Info : ' + JSON.stringify(send_data))
        if (send_data == "") {
            return
        }

        //Parse Recipient Data
        var a = JSON.parse(send_data)
        show_name = a.recipientName
        //show_phone = a.recipientUserPhoneNumber
        addresses = a.endAddress
        chargeType = a.chargeType
        customerStoreNumber = a.customerStoreNumber

        //Parse if classified as PopDeposit Parcel under "PDS" prefix
        if(customerStoreNumber.substring(0,3)=="PDS"){
            isPopDeposit = true
        }else{
            isPopDeposit = false
        }

        //Parse From-To
        if (addresses.indexOf(delimiter) > -1){
            split_address = addresses.split(delimiter.toString())
            show_locker = split_address[0]
            show_address = split_address[1]
        }else{
            show_address = addresses
            show_locker = locker_name
        }

        //Parse Booked Door Size
        if (chargeType == "FIXED_VALUE") {
            payOfAmount = a.payOfAmount
            if ("designationSize" in a) {
                if (a.designationSize=="") {
                    designationSize = a.designationSize
                }else{
                    designationSize = "none"
                }
            }else{
                designationSize = "none"
            }
        }
        if (chargeType == "NOT_CHARGE") {
            if ("designationSize" in a) {
                if (a.designationSize=="") {
                    designationSize = "none"
                }else if (a.designationSize=="DOC") {
                    designationSize = "S"
                }else{
                    designationSize = a.designationSize
                }
            }else{
                designationSize = "none"
            }
        }
    }

    function check_free_mouth(text) {
        if (text == "Failure") {
            ok_button.enabled = false
            back_button.enabled = false
            not_mouth.open()
        }
        if (text == "Success") {
            slot_handler.start_pull_pre_pay_cash_for_customer_express(payOfAmount)
            my_stack_view.push(send_pay_page, {designationSize: designationSize})
        }
    }

    function handle_text(text) {
        if (text == 'Success') {
            my_stack_view.push(send_door_open_page, {isPopDeposit: isPopDeposit})
        }
        if (text == 'NotMouth') {
            ok_button.enabled = false
            back_button.enabled = false
            not_mouth.open()
        }
    }

    function validate_locker(x,y){
        var check1 = x.toLowerCase().replace(/\s+|a|e|i|u|o|-|[.\,\:\+\&\//\;\@\\!]/g, "")
        var check2 = y.toLowerCase().replace(/\s+|a|e|i|u|o|-|[.\,\:\+\&\//\;\@\\!]/g, "")
        if(check1==check2 ||
                check1.slice(-5)==check2.slice(-5) ||
                check1.indexOf(check2) > -1 ||
                check2.indexOf(check1) > -1){
            console.log("MATCH >> " + check1 + ' VS ' + check2)
            return true
        }else{
            console.log("NOT MATCH >> " + check1 + ' VS ' + check2)
            return false
        }
    }

    Text {
        id: info_title
        x: 266
        y: 150
        width: 1024
        text: titleText
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.family: "Microsoft YaHei"
        color: "#FFFFFF"
        font.pixelSize: 45
    }

    DoorButton {
        id: back_button
        y: 610
        x: 178
        show_text: qsTr("Back")
        show_image: "img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function (item) {if (item.Stack.index === 2)return true}))
            }
//            onEntered: {
//                back_button.show_image = "img/door/2.png"
//            }
//            onExited: {
//                back_button.show_image = "img/door/1.png"
//            }
        }
    }

    DoorButton {
        id: ok_button
        y: 610
        x: 560
        show_text: qsTr("OK")
        show_image: "img/door/1.png"

    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log('actual_locker : ' + locker_name + ', ordered_locker : ' + show_locker)
            if (validate_locker(locker_name, show_locker)) {//order vs actual locker comparation
                if (chargeType == "BY_WEIGHT") {
                    my_stack_view.push(customer_send_express_view)
                }
                if (chargeType == "NOT_CHARGE") {
                    if (designationSize == "none") {
                        my_stack_view.push(send_select_box, {isPopDeposit: isPopDeposit})
                    }else{
                        slot_handler.start_choose_mouth_size(designationSize, "customer_store_express")
                    }
                }
                if (chargeType == "FIXED_VALUE") {
                    if (payOfAmount == 0) {
                        no_payOfAmount.open()
                    }
                    if (payOfAmount != 0) {
                        slot_handler.start_free_mouth_by_size(designationSize)
                    }
                }
            }else{
                non_choosen_locker.open()
                back_button.enabled = false
                ok_button.enabled = false
            }
        }
        onEntered: {
            ok_button.show_image = "img/door/2.png"
        }
        onExited: {
            ok_button.show_image = "img/door/1.png"
            }
        }
    }

    GroupBox {
        id: group_text
        x: 157
        y: 200
        width: 750
        height: 350
        flat: true
        checkable: false
        checked: false
        //title: qsTr("Group Box")

        Text {
            id: name_title
            x: 44
            y: 10
            width: 150
            height: 50
            text: qsTr("Name:")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.family: "Microsoft YaHei"
            color: "#FFFFFF"
            font.pixelSize: 32
            anchors.right: name_value.left
            anchors.rightMargin: 26
        }

        Text {
            id: name_value
            x: 174
            y: 10
            width: 400
            height: 50
            text: show_name
            verticalAlignment: Text.AlignVCenter
            font.family: "Microsoft YaHei"
            color: "#FFFFFF"
            font.pixelSize: 32
        }

        Text {
            id: locker_name_title
            x: 44
            y: 70
            width: 150
            height: 50
            text: qsTr("From:")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.family: "Microsoft YaHei"
            color: "#FFFFFF"
            font.pixelSize: 32
            anchors.right: locker_value.left
            anchors.rightMargin: 26
        }

        Text {
            id: locker_value
            x: 174
            y: 70
            width: 400
            height: 50
            text: 'PopBox @ ' + show_locker
            verticalAlignment: Text.AlignVCenter
            font.family: "Microsoft YaHei"
            color: "#FFFFFF"
            font.pixelSize: 32
        }

        Text {
            id: address_title
            x: 44
            y: 130
            width: 150
            height: 50
            text: qsTr("Address:")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.family: "Microsoft YaHei"
            color: "#FFFFFF"
            font.pixelSize: 32
            anchors.right: address_value.left
            anchors.rightMargin: 26
            }

        Text {
            id: address_value
            x: 174
            y: 130
            width: 600
            height: 50
            text: show_address
            font.family: "Microsoft YaHei"
            color: "#FFFFFF"
            font.pixelSize: 30
            wrapMode: Text.WordWrap
        }

    }

    HideWindow {
        id: not_mouth
        //visible:true

        Image {
            id: img_not_mouth
            x: 413
            y: 218
            width: 200
            height: 200
            source: "img/otherImages/x-mark.png"
        }

        Text {
            text: qsTr("not_mouth")
            x: 94
            y:464
            width: 852
            height: 80
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton {
            id: not_mouth_back
            x:374
            y:590
            show_text: qsTr("back")
            show_x: 15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    ok_button.enabled = true
                    back_button.enabled = true
                    not_mouth.close()
                }

            }
        }
    }

    HideWindow {
        id: no_payOfAmount
        //visible:true

        Text {
            x: 92
            y: 330
            width: 850
            height: 112
            text: qsTr("no_payOfAmount")
            font.family: "Microsoft YaHei"
            color: "#ffffff"
            textFormat: Text.PlainText
            font.pointSize: 30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton {
            id: no_payOfAmount_back
            x: 354
            y: 558
            show_text: qsTr("back")
            show_x: 15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    ok_button.enabled = true
                    back_button.enabled = true
                    no_payOfAmount.close()
                }
            }
        }
    }

    HideWindow {
        id: non_choosen_locker
        //visible: true

        Text {
            id: we_are_sorry_text
            x: 96
            y: 251
            width: 848
            height: 50
            color: "#ffffff"
            text: qsTr("We are sorry")
            textFormat: Text.PlainText
            font.pointSize: 35
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
            font.family: "Microsoft YaHei"
        }

        Text {
            id: notif_text_non_choosen_locker
            x: 96
            y: 325
            width: 848
            height: 200
            wrapMode: Text.WordWrap
            font.family: "Microsoft YaHei"
            color: "#ffffff"
            text: qsTr("This order cannot be processed, Please ensure you have choose the right locker.")
            textFormat: Text.PlainText
            font.pointSize: 32
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton {
            id: non_choosen_back_button
            x: 381
            y: 581
            show_text: qsTr("Back")
            show_x: 15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function (item) {if (item.Stack.index === 2)return true}))
                }
            }
        }
    }
}
