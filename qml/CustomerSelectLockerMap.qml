import QtQuick 2.4
import QtQuick.Controls 1.2
import QtWebKit 3.0
import QtWebKit.experimental 1.0
//import QtTest 1.0

Background{
    id: background
    img_vis: true
    width: 1024
    height: 768
    property bool devMode: false
    property var _url: 'https://www.popbox.asia/locker/maps/id/'
    property int timer_value: 120
    property var selected_locker: "N/A"
    property var press: "0"
    property var status_loading: "null"
    property var products: ""
    property var customers: ""
    property var usageOf: ""
    property bool readyStatus: false
    property var links: {"bandung": _url + "bandung",
                          "bogor": _url + "bogor",
                          "depok": _url + "depok",
                          "cibinong": _url + "cibinong",
                          "bekasi": _url + "bekasi",
                          "tangerang": _url + "tangerang",
                          "jak_sel": _url + "jakarta%20selatan",
                          "jak_bar": _url + "jakarta%20barat",
                          "jak_ut": _url + "jakarta%20utara",
                          "jak_tim": _url + "jakarta%20timur",
                          "jak_pus": _url + "jakarta%20pusat"}
    property var init_url: devMode ? "http://www.google.co.id" : links.jak_bar

    Stack.onStatusChanged:{
       if(Stack.status==Stack.Activating){
           notif_box.visible = false
           reload_url(init_url)
           if(init_url.indexOf("jakarta%20barat") > -1){
               jakbar_button.button_color = "#BF2F26"
           }else{
               jakbar_button.button_color = "white"
           }
           press = "0"
           selected_locker = "N/A"
        }
        if(Stack.status==Stack.Deactivating){
            reset_button()
            my_timer.stop()
        }
    }

    Rectangle{
        width:10
        height:10
        y:10
        color:"transparent"
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                countShow_test.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    function select_locker(){
        webview.experimental.evaluateJavaScript(
        "(function(){return document.getElementById('selected_locker').innerText})()",
        function(result){if(result.length>3||result!="") selected_locker=result});
    }

    function reload_url(__url){
        abc.counter = timer_value
        my_timer.restart()
        loadingGif.open();
        webview.url = Qt.resolvedUrl(__url);
        init_url = __url
        console.log("webview url : ", webview.url)
    }

    function reset_button(){
        bandung_button.button_color = "white"
        bekasi_button.button_color = "white"
        bogor_button.button_color = "white"
        cibinong_button.button_color = "white"
        depok_button.button_color = "white"
        jakbar_button.button_color = "white"
        jakpus_button.button_color = "white"
        jaksel_button.button_color = "white"
        jaktim_button.button_color = "white"
        jakut_button.button_color = "white"
        tangerang_button.button_color = "white"
    }

    ScrollView {
        id: scroll_view
        width: 1024
        height: 768
        focus: true
        WebView {
            id: webview
            anchors.fill: parent
            focus: true
            experimental.userAgent:"Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36  (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36"
            onLoadingChanged: {
                if(loadRequest.status==WebView.LoadSucceededStatus){
                    readyStatus = true
                    loadingGif.close()
                    status_loading = "finished";
                } else if(loadRequest.status==WebView.LoadFailedStatus){
                    status_loading = "failed";
                }
                if(devMode){
                    webview.experimental.evaluateJavaScript("var inputs = document.getElementsByTagName('INPUT'); var element = inputs[2]; element.value='Hello World'")
                }
            }
        }
    }

    Rectangle{
        id: locker_city_buttons
        x: -3
        focus: true
        visible: true
        enabled: !notif_box.visible
        color: "transparent"
        anchors.right: parent.right
        anchors.rightMargin: -6
        y: 117
        width: 141
        height: 547

        Column{
            x: -6
            y: 0
            spacing: 2
            //1st Button
            LockerCityButton{
                id: bandung_button
                button_text: qsTr("Bandung")
                //enabled: (button_color=="#BF2F26") ? false : true
                visible: false
                enabled: false
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    bandung_button.button_color = "#BF2F26"
                    reload_url(links.bandung)
                    }
                }
            }
            //2nd Button
            LockerCityButton{
                id: bekasi_button
                button_text: qsTr("Bekasi")
                enabled: (button_color=="#BF2F26") ? false : true
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    bekasi_button.button_color = "#BF2F26"
                    reload_url(links.bekasi)
                    }
                }
            }
            //3rd Button
            LockerCityButton{
                id: bogor_button
                button_text: qsTr("Bogor")
                enabled: (button_color=="#BF2F26") ? false : true
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    bogor_button.button_color = "#BF2F26"
                    reload_url(links.bogor)
                    }
                }
            }
            //4th Button
            LockerCityButton{
                id: cibinong_button
                button_text: qsTr("Cibinong")
                enabled: (button_color=="#BF2F26") ? false : true
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    cibinong_button.button_color = "#BF2F26"
                    reload_url(links.cibinong)
                    }
                }
            }
            //5th Button
            LockerCityButton{
                id: depok_button
                button_text: qsTr("Depok")
                enabled: (button_color=="#BF2F26") ? false : true
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    depok_button.button_color = "#BF2F26"
                    reload_url(links.depok)
                    }
                }
            }
            //6th Button
            LockerCityButton{
                id: jakbar_button
                button_text: qsTr("Jakarta Barat")
                enabled: (button_color=="#BF2F26") ? false : true
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    jakbar_button.button_color = "#BF2F26"
                    reload_url(links.jak_bar)
                    }
                }
            }
            //7th Button
            LockerCityButton{
                id: jakpus_button
                button_text: qsTr("Jakarta Pusat")
                enabled: (button_color=="#BF2F26") ? false : true
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    jakpus_button.button_color = "#BF2F26"
                    reload_url(links.jak_pus)
                    }
                }
            }
            //8th Button
            LockerCityButton{
                id: jaksel_button
                button_text: qsTr("Jakarta Selatan")
                enabled: (button_color=="#BF2F26") ? false : true
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    jaksel_button.button_color = "#BF2F26"
                    reload_url(links.jak_sel)
                    }
                }
            }
            //9th Button
            LockerCityButton{
                id: jaktim_button
                button_text: qsTr("Jakarta Timur")
                enabled: (button_color=="#BF2F26") ? false : true
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    jaktim_button.button_color = "#BF2F26"
                    reload_url(links.jak_tim)
                    }
                }
            }
            //10th Button
            LockerCityButton{
                id: jakut_button
                button_text: qsTr("Jakarta Utara")
                enabled: (button_color=="#BF2F26") ? false : true
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    jakut_button.button_color = "#BF2F26"
                    reload_url(links.jak_ut)
                    }
                }
            }
            //11th Button
            LockerCityButton{
                id: tangerang_button
                button_text: qsTr("Tangerang")
                enabled: (button_color=="#BF2F26") ? false : true
                MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_button()
                    tangerang_button.button_color = "#BF2F26"
                    reload_url(links.tangerang)
                    }
                }
            }
        }
    }

    Rectangle{
        id: rec_main_title
        x: 112
        width: 500
        height: 50
        color: "#BF2F26"
        radius: 20
        anchors.top: parent.top
        anchors.topMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        opacity: 1
        visible: readyStatus

        Text {
            id: notif_select_qty
            anchors.fill: rec_main_title
            color: "#FFFFFF"
            font.family:"Microsoft YaHei"
            text: qsTr("Please Click The Locker Point")
            font.pixelSize: 25
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            textFormat: Text.AutoText
            opacity: 1
        }
    }

    Rectangle{
        id: rec_zoom_text
        x: 60
        y: 10
        width: 110
        height: 40
        radius: 0
        color: "white"
        Text{
            anchors.fill: rec_zoom_text
            width: 100
            text: qsTr("Zoom Here")
            font.bold: true
            font.pixelSize: 15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "#BF2F26"
        }
    }

    BackButton{
        id:back_button
        x:20
        y:background.height/2
        show_text:qsTr("Back")

        MouseArea {
            id: ms_back
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
//                my_stack_view.pop()
                my_stack_view.push(customer_input_details, {products: products, customers: customers, usageOf:usageOf})
            }
        }
    }

    OverTimeButton{
        id: check_button
        x: 373
        y: 675
        show_text: qsTr("Select This Locker")
        show_x:15
        text_color: "white"
        bg_color: "#BF2F26"
        show_source: ""
        global_vis: readyStatus

        MouseArea {
            anchors.fill: check_button
            onClicked: {
                if (press!="0") return
                press = "1"
                abc.counter = timer_value
                my_timer.restart()
                select_locker()
                notif_box.visible = true
                select_locker_timer.start()
            }
        }
    }

    Rectangle{
        id: rec_top_button
        x: 413
        y: -10
        width: 300
        height: 80
        color: "transparent"
        anchors.horizontalCenter: parent.horizontalCenter
        visible: readyStatus
        Rectangle{
            id: map_mode
            radius: 10
            width: 150
            height: 70
            color: "darkred"
            Text {
                id: map_mode_text
                anchors.fill: map_mode
                text: qsTr("Map Mode")
                font.bold: true
                wrapMode: Text.WordWrap
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family: "Microsoft YaHei"
                font.pixelSize: 20
                color: "white"
            }
        }
        Rectangle{
            id: list_mode
            y: 0
            radius: 10
            anchors.left: map_mode.right
            anchors.leftMargin: 0
            width: 150
            height: 70
            color: "white"
            Text {
                id: list_mode_text
                anchors.fill: list_mode
                text: qsTr("List Mode")
                font.bold: true
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.family: "Microsoft YaHei"
                font.pixelSize: 20
                color: "darkred"
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(customer_select_locker, {products: products, customers: customers, usageOf: usageOf})
                }
            }
        }
    }

    LoadingView {id: loadingGif}

    Rectangle{
        id: overlay_base
        visible: notif_box.visible
        anchors.fill: parent
        color: "#472f2f"
        opacity: 0.7
        QtObject{
            id:select_locker_count
            property int counter
            Component.onCompleted:{
                select_locker_count.counter = 1
            }
        }
        Timer{
            id:select_locker_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                select_locker_count.counter -= 1
                if(select_locker_count.counter < 0){
                    select_locker_timer.stop()
                    select_locker()
                }
            }
        }
    }

    Rectangle{
        id:notif_box
        x: 183
        y: 294
        width: 681
        height: 400
//        visible: true
        color: "white"
        radius: 15
        anchors.verticalCenter: parent.verticalCenter
        opacity: 1
        Text{
            id: txt_notif_oops
            x: 69
            y: 100
            width: 544
            height: 50
            text: qsTr("Locker Confirmation")
            anchors.verticalCenterOffset: -150
            anchors.verticalCenter: parent.verticalCenter
            font.italic: true
            textFormat: Text.PlainText
            color: "#BF2F26"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 35
            font.family: "Microsoft YaHei"
            font.bold: true
        }
        Text{
            id: txt_notif
            x: 109
            y: 241
            width: 544
            height: 187
            text: qsTr("You have chose below locker as destination address : ") + "\n\n" + selected_locker
            anchors.verticalCenterOffset: -11
            anchors.verticalCenter: parent.verticalCenter
            font.italic: true
            textFormat: Text.PlainText
            color: "#BF2F26"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignTop
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 30
            font.family: "Microsoft YaHei"
            font.bold: true
        }
        Image{
            id: img_arrow
            width: 60
            height: 98
            anchors.verticalCenterOffset: -12
            anchors.verticalCenter: parent.verticalCenter
            source: "img/otherImages/marker-locker.png"
            x:30
            y:275
        }
        OverTimeButton{
            id:cancel_button
            x:69
            y:316
            width: 250
            height: 64
            show_text: qsTr("Cancel")
            show_x:15
            bg_color: "#BF2F26"
            show_source: ""
            text_color: "white"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    abc.counter = timer_value
                    my_timer.restart()
                    press = "0"
                    loadingGif.close()
                    selected_locker = "N/A"
                    notif_box.visible = false
                }
            }
        }        
        OverTimeButton{
            id:confirm_button
            x:363
            y:316
            width: 250
            height: 64
            enabled: (selected_locker!="N/A") ? true : false
            show_text: qsTr("Confirm")
            show_x:15
            bg_color: (selected_locker!="N/A") ? "#BF2F26" : "gray"
            show_source: ""
            text_color: (selected_locker!="N/A") ? "white" : "black"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(selected_locker!="N/A"){
                        console.log("selected_locker : ", selected_locker);
                        my_stack_view.push(grocery_express_info, {products:products,
                                               selected_locker: selected_locker,
                                               customers: customers});
                    }
                }
            }
        }
    }

    Text{
        id:countShow_test
        x:10
        y:738
        color:"#FFFF00"
        font.family:"Microsoft YaHei"
        font.pixelSize:16
        visible: false
    }
}

