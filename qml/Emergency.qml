import QtQuick 2.4
import QtQuick.Controls 1.2
import Qt.labs.folderlistmodel 1.0
import QtMultimedia 5.0

Rectangle{
    id:parent_root
    color: "black"
    property var img_path: '/advertisement/emergency/'
    property url img_path_: '..' + img_path
    property int index: 0
    property variant media_files: []
    property string mode: 'staticVideo' // ["staticVideo", "mediaPlayer", "liveView"]
    property var staticFile: root.staticFile
    property var screenSize: '32'

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log('ads mode : ' +  mode);
            init_play();
        }
        if(Stack.status==Stack.Deactivating){
            player.stop();
        }
    }

    function init_play(){
        if (mode=='staticVideo'){
            media_files = [staticFile];
            media_mode.setIndex(0);
            console.log("media_files :", media_files)
        }
    }

    Component.onCompleted: {
//        if(screenSize!='15') init_play()
    }

    Rectangle {
        id: media_mode
        visible: true
        anchors.fill: parent
        color: "black"

        function setIndex(i){
            index = i;
            index %= media_files.length;
            player.source = img_path_ + media_files[index];
            player.play()
        }

        function next(){
            setIndex(index + 1);
        }

        function previous(){
            setIndex(index - 1);
        }

        Connections {
            target: player
            onStopped: {
                if (player.status == MediaPlayer.EndOfMedia) {
                    if (index==media_files.length-1){ //Looping start from beginning
                        media_mode.setIndex(0);
                    } else{
                        media_mode.next();
                    }
                }
            }
        }

        MediaPlayer {
            id: player
        }

        VideoOutput {
            anchors.fill: parent
            source: player
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(screenSize=='15'){
                    player.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
            onDoubleClicked: {
                if(screenSize=='15'){
                    player.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }
}

