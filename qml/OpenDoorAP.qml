import QtQuick 2.4
import QtQuick.Controls 1.2


BaseAP{
    id: baseAP
    property var press: '0'
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    property var phone_number: ''
    property int timer_value: 60
    property var summary: undefined
    property int reOpenDoor: 0
    property var locale: Qt.locale()
    property date currentDate: new Date()
    property string dateTimeString: "Tue 2013-09-17 10:56:06"
    property int count:2
    property int val



    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            phone_number = '';
            abc.counter = timer_value;
            my_timer.start();
            reOpenDoor = 0;
            if (summary!=undefined){
                parse_summary(summary);
            }
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
        }
    }

    Component.onCompleted: {

    }

    Component.onDestruction: {

    }

    function parse_summary(s){
        console.log('summary', s);
        var info = JSON.parse(s);
        door_no_.text = info.door_no;
        var dateBoard = "01/31/2018";
        var date = Qt.formatDateTime(info.date, "ddd, dd MMM yyyy hh:mm");
        take_time_.text =  date;
        console.log("info date zz: " + info.date);
        pin_code_.text = info.pin_code;
    }

    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }
    }

    Rectangle{
        id: timer_set
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Image {
        id: step_progress
        anchors.top: parent.top
        anchors.topMargin: 115
        anchors.horizontalCenter: parent.horizontalCenter
        source: "img/apservice/langkah/3.png"
    }

    Text {
        id: main_command_text
        width: 500
        height: 100
        color: "#323232"
        text: qsTr("Masukkan Barang")
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 200
        font.bold: true
        font.family: 'Microsoft YaHei'
        font.pixelSize: 50

    }

    Image {
        id: icon_confirm
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -10
        anchors.right: parent.right
        anchors.rightMargin: 10
        visible: false
        source: "img/apservice/img/kode_pin.png"
    }

    Image {
        id: notif_image
        x: 598
        y: 253
        scale: 0.8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -3
        anchors.right: parent.right
        anchors.rightMargin: -47
        source: "img/apservice/img/customer_take.PNG"
    }

    Column{
        id: details_text
        anchors.verticalCenterOffset: 10
        anchors.left: parent.left
        anchors.leftMargin: 44
        anchors.verticalCenter: parent.verticalCenter
        spacing: 60
        RowTextAP{
            labelText: qsTr('No Pintu')
            contentText: ''
            colorContent: '#323232'
            contentBold: true
            separatorPoint: 250
        }
        RowTextAP{
            labelText: qsTr('Batas Ambil')
            contentText: ''
            colorContent: '#009BE1'
            contentBold: true
            separatorPoint: 250
        }
    }

    Column{
        id: details2_text
        anchors.verticalCenterOffset: 60
        anchors.left: parent.left
        anchors.leftMargin: 55
        anchors.verticalCenter: parent.verticalCenter
        spacing: 60
        Text {
            id: door_no_
            text: '10'
            font.pixelSize: 50
            font.bold: true
            color: '#009BE1'
            font.family: 'Microsoft YaHei'
        }
        Text {
            id: take_time_
            text: '2018-08-09 10:00:00'
            font.pixelSize: 26
            font.bold: true
            //font.italic: true
            color: '#009BE1'
            font.family: 'Microsoft YaHei'

        }

    }
    

    // Rectangle{
    //     id:text_box
    //     width: 510
    //     height: 70
    //     color: "#4A4A4A"
    //     anchors.left: parent.left
    //     anchors.leftMargin: 55
    //     anchors.bottom: parent.bottom
    //     anchors.bottomMargin: 150

    //     Text{
    //         id: text_box_isi
    //         anchors.horizontalCenter: parent.horizontalCenter
    //         font.pixelSize: 16
    //         horizontalAlignment: Text.AlignHCenter
    //         color: '#ffffff'
    //         text: qsTr("Transaksi ini berlaku untuk satu (1) kali Penyimpanan\nbarang. Notifikasi KODE PIN dikirimkan ke NO HP yg\nterdaftar. Jangan lupa tutup pintu kembali. Anda\ndiberikan 3 kali kesempatan buka pintu selama 1 menit.")
    //     }

    //     // Column{
    //     //     id: colom_box
    //     //     anchors.verticalCenterOffset: 0
    //     //     anchors.left: parent.left
    //     //     anchors.leftMargin: 0
    //     //     anchors.verticalCenter: parent.verticalCenter
    //     //     spacing: 1
    //     //     Text{
    //     //         id: text_box_isi1
    //     //         anchors.horizontalCenter: parent.horizontalCenter
    //     //         font.pixelSize: 16
    //     //         horizontalAlignment: Text.AlignHCenter
    //     //         color: '#ffffff'
    //     //         font.bold: true
    //     //         text: qsTr(" Transaksi ini berlaku untuk satu (1) kali penyimpanan barang.")
    //     //     }
    //     //     Text{
    //     //         id: text_box_isi2
    //     //         anchors.horizontalCenter: parent.horizontalCenter
    //     //         font.pixelSize: 16
    //     //         horizontalAlignment: Text.AlignHCenter
    //     //         color: '#ffffff'
    //     //         text: qsTr("Notifikasi KODE PIN dikirimkan ke NO HP yg terdaftar.")

    //     //      }
    //     // }
    // }

    Text{
        id: pin_code_
        x: 708
        y: 448
        width: 400
        visible: false
        text: 'ABC123'
        font.letterSpacing: 10
        font.weight: Font.Bold
        fontSizeMode: Text.HorizontalFit
        style: Text.Raised
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: icon_confirm.horizontalCenter
        anchors.verticalCenterOffset: 90
        anchors.verticalCenter: parent.verticalCenter
        font.bold: true
        font.pixelSize: 70
        color: '#009BE1'
        font.family: 'Microsoft YaHei'
    }

    NotifButtonAP{
        id: ok_button
        x: 365
        y: 644
        buttonColor: 'BLUE'
        modeReverse: false
        buttonText: qsTr('SELESAI')
        MouseArea{
            anchors.fill: parent
            onClicked: {
                my_timer.stop();
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }));
            }
        }
    }

    NotifButtonAP{
        id: cancel_button
        x: 50
        y: 644
        buttonColor: (reOpenDoor>1) ? 'GRAY' : 'BLUE'
        modeReverse: true
        buttonText: qsTr('BUKA KEMBALI')
        enabled: (reOpenDoor>1) ? false : true
        MouseArea{
            anchors.fill: parent
            onClicked: {
                reOpenDoor += 1;
                val = count - reOpenDoor;
                availableKlik.text = val
                slot_handler.start_open_mouth_again();
                console.log('reopen_button is pressed')
            }
        }
    }

    // Put Main Component Here

    Rectangle{
        id: klik_avail
        x: 70
        y: 725
        color: "transparent"

        Row {
            id: avail_doors_view
            x: 0
            y: 0   
            spacing: 5

            Text {
                id: mouth_title
                text: qsTr("Sisa buka kembali:  ")
                font.family:"Microsoft YaHei"
                font.bold: false
                color:"black"
                font.pixelSize: 20
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            Row {
                spacing: 5
                Text {
                    id: availableKlik
                    text: qsTr("2")
                    font.family:"Microsoft YaHei"
                    font.bold: false
                    color:"black"
                    font.pixelSize: 20
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                }
            }
          
        }
    }

    Text{
        id: perhatian
        x:50
        y:535
        color: 'black'
        text: qsTr("Transaksi ini berlaku untuk satu (1) kali penyimpanan\nbarang. Notifikasi KODE PIN dikirimkan ke NO HP yg\nterdaftar. Jangan lupa tutup pintu kembali. Anda\ndiberikan 3 kali kesempatan buka pintu selama 1 menit.")
        anchors.left: parent.left
        anchors.leftMargin: 50
        font.pixelSize: 17
        font.family: 'Microsoft YaHei'
    }


    Notification{
        id: notif_text
        contentNotif: qsTr('Terima Kasih telah menggunakan Loker PopBox.')
        successMode: true
    }


}
