import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:full_keyboard
    width:268
    height:358
    color:"transparent"
    property bool functionPad: true
    signal letter_button_clicked(string str)
    signal function_button_clicked(string str)


    NumButton{
        x:0
        y:0
        show_text:"1"
    }
    NumButton{
        x:90
        y:0
        show_text:"2"
    }
    NumButton{
        x:180
        y:0
        show_text:"3"
    }
    NumButton{
        x:0
        y:90
        show_text:"4"
    }
    NumButton{
        x:90
        y:90
        show_text:"5"
    }
    NumButton{
        x:180
        y:90
        show_text:"6"
    }
    NumButton{
        x:0
        y:180
        show_text:"7"
    }
    NumButton{
        x:90
        y:180
        show_text:"8"
    }
    NumButton{
        x:180
        y:180
        show_text:"9"
    }
    NumButton{
        x:90
        y:270
        show_text:"0"
    }
    NumboardFunctionButtonreturn{
        x:0
        y:270
        visible: functionPad
        color: "#5a5a5a"
        border.width: 0
        slot_text:qsTr("return")
    }
    NumboardFunctionButtondelete{
        x:180
        y:270
        visible: functionPad
        color: "#ffc125"
        border.width: 0
        slot_text:qsTr("DEL")
       //show_image:"img/button/DeleteButton.png" -> Disable Background Change
    }
}
