import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: grocery_express_info
    width: 1024
    height: 768
    property int timer_value: 300
    property int show_timer_value: 5
    property int show_timer_value_nf: 5
    property var press:"0"
    property int trial_count: 0
    property var cust_name: "-"
    property var cust_phone: "-"
    property var cust_email: "-"
    property var prod_name: "-"
    property var prod_sku: "-"
    property var prod_qty: "-"
    property var selected_locker: "-"
    property var amount: "0"
    property var new_amount:"0"
    property var payment_channel: undefined
    property var customers
    property var products
    property var summary_data
    property var show_provider:"-"
    property var phone_no:"-"
    property var service:"-"
    property var denom:"0"
    property var data_product
    property var product_id:"-"
    property var product_type:"-"
    property var prod_type_temp:"-"
    property var usageOf: undefined
    property var invoice_no: undefined
    property var popsafe_param: undefined
    property var lockerSize: ""
    property bool triggerOpen: false

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log("products : " + products)
            console.log("customers : " + customers)
            console.log("selected locker : " + selected_locker)
            console.log("usageOf : " + usageOf)
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
            if (payment_channel=="MANDIRI-EMONEY"){
                prepare_text.text = qsTr("Please prepare your pre-paid card and ensure the balance is sufficient.")
            }
            trial_count = 0
            triggerOpen = false
            parse_data_all(products, customers)
            time_payment.counter = show_timer_value
            time_nf.counter = show_timer_value_nf
            ok_button.enabled = true
            nonfinished_notif.close()
            payment_page.close()
            wrong_card_notif.close()
            if (usageOf=='sepulsa'){
                if (payment_channel == undefined) {
                    parseDataProduct(data_product)
                } else {
                    reparseDataSepulsa(data_product)
                }
            }
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            show_timer_payment.stop()
            show_timer_nf.stop()
        }
    }

    Component.onCompleted: {
        root.start_global_payment_emoney_result.connect(handle_text);
        root.create_transaction_result.connect(pretransaction_result);
        root.add_payment_result.connect(assign_payment_result);
        root.choose_mouth_result.connect(handle_text_mouth);
        root.start_check_member_result.connect(get_member);
        root.customer_take_express_result.connect(process_result)
    }

    Component.onDestruction: {
        root.start_global_payment_emoney_result.disconnect(handle_text);
        root.create_transaction_result.disconnect(pretransaction_result);
        root.add_payment_result.disconnect(assign_payment_result);
        root.choose_mouth_result.disconnect(handle_text_mouth);
        root.start_check_member_result.disconnect(get_member);
        root.customer_take_express_result.disconnect(process_result)
    }

    function process_result(text){
        console.log('process_result : ' + text)
        switch(text){
            case "Success" : my_stack_view.push(customer_take_express_opendoor_view);
                break;
            case "Overdue" : my_stack_view.push(customer_take_express_overtime_view);
                break;
            case "Overdue||Popsafe" :
                my_stack_view.push(customer_take_express_overtime_view, {isPopDeposit: true});
                break;
            case "Error" : my_stack_view.push(customer_take_express_error_view);
                break;
            default : my_stack_view.push(customer_take_express_error_view);
        }
    }

    function get_member(result){
        console.log("get_member : ", result)
        var c = JSON.parse(result)
        cust_name = c.name
        cust_email = c.email
        cust_phone = c.phone
        customers = result
    }

    function reparseDataSepulsa(d){
        show_provider =  d.show_provider
        amount = d.amount
        product_type = d.product_type
        service = d.service
        phone_no = d.phone_no
        product_id = d.product_id
    }

    function handle_text_mouth(mouth){
        console.log("handle_text_mouth : ", mouth)
        if(mouth == 'Success' && triggerOpen == false){
            triggerOpen = true
            my_stack_view.push(send_door_open_page,{isPopDeposit: true, type:"none",store_type:"store",change_press:3,popsafe_param:popsafe_param})
        }
    }

    function assign_payment_result(text){
        console.log('assign_payment_result', text)
        if (text!='NO_DATA'||text!='ERROR'){
            // TODO Check response to define assign_payment_result
        }
    }

    function pretransaction_result(text){
        console.log('pretransaction_result', text)
        if (text=='NO_DATA'||text=='ERROR'||text==''||text==undefined){
            return
        } else {
            // TODO Check response to define transaction_reference
            var o = JSON.parse(text).data
            invoice_no = o[0].references;
            if (payment_channel=="MANDIRI-EMONEY"){
                add_payment_channel(usageOf, invoice_no, payment_channel);
            }
        }
    }

    function add_payment_channel(u, t, m){
        switch (u) {
        case 'ppob-starter-pack':
            var param = JSON.stringify({'transaction': t, 'payment': m})
            slot_handler.start_add_payment(param)
            break;
        default:
            break;
        }
    }

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    }

    function handle_text(result){
        console.log("status_payment_signal : " + result )
        if(result == "UNFINISHED"){
            trial_count += 1
            payment_page.close()
            nonfinished_notif.open()
            show_timer_payment.stop()
            show_timer_nf.restart()
            ok_button.enabled = false
            trial_count_text.text = trial_count
            time_nf.counter = show_timer_value_nf
            if(trial_count >= 4){
                //slot_handler.start_reset_partial_transaction()
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
            return
        }
        if(result == "WRONG-CARD"){
            wrong_card_notif.open()
            payment_page.close()
            ok_button.enabled = false
            return
        }
        if(result == "ERROR" || result == "FAILED"){
            my_stack_view.push(other_payment_failed_page)
            return
        }

        if (payment_channel != undefined){
            switch(usageOf){
            case "sepulsa":
                slot_handler.start_push_data_settlement(phone_no+'-'+product_id+'-'+product_type)
                my_stack_view.push(sepulsa_payment_success_view, {
                                       payment_result:result, amount:amount, service:service, phone_no:phone_no,
                                       product_id: product_id, product_type:prod_type_temp});
                break;
            case "popsafe-order":
                var popsafe = JSON.parse(popsafe_param)
                var r = JSON.parse(result)
                popsafe.transactionRecord = r.raw
                popsafe.paymentMethod = payment_channel
                popsafe_param = JSON.stringify(popsafe)
                console.log("popsafe_param : ", payment_channel, popsafe_param)
                if (popsafe.transactionRecord != ""){
                    slot_handler.start_video_capture("popsafe-order-"+popsafe.lockerSize)
                    slot_handler.start_push_data_settlement(phone_no+'-popsafe_locker-'+popsafe.lockerSize)
                    slot_handler.start_choose_mouth_size(lockerSize,"customer_store_express")
                }
                break;
            case "popsafe-extend":
                var popsafe_extend = JSON.parse(popsafe_param)
                var rr = JSON.parse(result)
                popsafe_extend.transactionRecord = rr.raw
                popsafe_extend.paymentMethod = payment_channel
                popsafe_param = JSON.stringify(popsafe_extend)
                console.log("popsafe_extend_param : ", payment_channel, popsafe_param)
                if (popsafe_extend.transactionRecord != ""){
                    var p = JSON.parse(products)
                    var c = JSON.parse(customers)
                    var param = JSON.stringify({"expressNumber": p.sku, "userSession": c.sessionId, "trxRemarks": payment_channel+"|"+rr.raw+"|"+amount})
                    slot_handler.start_extend_express(param)
                    slot_handler.start_push_data_settlement(phone_no+'-popsafe_extend-'+p.sku)
                    slot_handler.start_video_capture("popsafe-extend-"+p.sku+'-'+p.pin)
                    slot_handler.customer_take_express(p.pin)
                }
                break;
            default:
                // For default case, the emoney settlement on the grocery_payment_success view
                summary_data = {"product": products, "customer": customers, "locker": selected_locker}
                my_stack_view.push(grocery_payment_success, {payment_result:result, new_amount: new_amount,
                                       summary_data:JSON.stringify(summary_data), usageOf: usageOf,
                                       invoice_order: invoice_no, amount: amount, payment_type: payment_channel})
                break;
            }

        }

    }

    function parse_data_all(objA, objB){
        var product = JSON.parse(objA)
        prod_sku = product.sku
        prod_name = product.name
        prod_qty = product.qty
        amount = product.total_amount
        new_amount = insert_flg(amount)
        check_proddetails(prod_sku,prod_name)
        var customer = JSON.parse(objB)
        cust_name = customer.name
        cust_email = customer.email
        cust_phone = customer.phone
        if (usageOf=="popsafe-order" || usageOf=="popsafe-extend") {
            lockerSize = product.lockerSize
            if (payment_channel==undefined){
                slot_handler.start_check_member(customer.phone)
            } else {
                cust_name = customer.name
                cust_email = customer.email
                cust_phone = customer.phone
            }
            //TODO Define Popsafe Param
            popsafe_param = JSON.stringify({
                "takeUserPhoneNumber": cust_phone,
                "lockerSize": product.lockerSize,
                "transactionRecord": "",
                "paymentMethod": "",
                "paymentAmount": amount
            })
            product_label.text = qsTr("Description")
            delivery_label.text = qsTr("Location")
            amount_label.text = qsTr("Price")
            qty_label.text = qsTr("Duration")
            qty_text.text = qsTr("24 Hours")
        }

        create_pretransaction(usageOf)
    }

    function create_pretransaction(u){
        console.log('create_pretransaction : ', u)
        switch (u) {
        case 'ppob-starter-pack':
            var sim_number = JSON.parse(products).number
            // call slot_function for starter pack create transaction
            var param = JSON.stringify({ 'phone': sim_number, 'customer_phone': cust_phone,
                                           'customer_name': cust_name, 'customer_email': cust_email,
                                           'product_id': [prod_sku], 'addon_id': []
                                         })
            slot_handler.start_create_transaction('starter-pack', param)
            break;
        default:
            break;
        }

    }

    function check_proddetails(x,y){
        var z = x + " - " + y
        if(z.length > 30){
            second_row.y = 400
        }else{
            second_row.y = 360
        }
    }

    function parseDataProduct(d){
        console.log("parse_sepulsa_data_product : " + JSON.stringify(d))
        for(var x in d){
            if(d[x].product_id == product_id){
                denom = d[x].nominal
                //service = d[x].label.replace(show_provider + " ",'')
                service = d[x].label
                if(d[x].is_promo == true){
                    service = service + " (" + d[x].promo_name.replace('.000', 'K') + ")"
                    amount = d[x].price
                }else{
                    amount = denom
                }
                prod_type_temp = d[x].type
                if(prod_type_temp == "mobile"){
                    if(d[x].is_packet_data == true){
                        product_type = "Paket Data"
                    }
                    if(d[x].is_packet_data == false){
                        product_type = "TopUp Pulsa"
                    }
                }else if(prod_type_temp == "electricity"){
                    product_type = "PLN Pra-Bayar"
                }else if(prod_type_temp == "bpjs_kesehatan"){
                    product_type = "BPJS Kesehatan"
                }else if(prod_type_temp == "game"){
                    product_type = "Voucher Game"
                }else{
                    product_type = "Layanan Lainnya"
                }
            }
        }

        data_product = {
            "show_provider": show_provider,
            "amount": amount,
            "product_type": product_type,
            "service": service,
            "phone_no": phone_no,
            "product_id": product_id
        }

    }

    Rectangle{
        x: 10
        y: 10
        width: 20
        height: 20
        color: "transparent"

        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                main_timer_text.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
        Text {
            id:main_timer_text
            visible:false
            width: 20
            height: 20
            font.family:"Microsoft YaHei"
            color:"#fff000"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            textFormat: Text.PlainText
            font.pointSize:10
            wrapMode: Text.WordWrap
        }
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("return")
        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
//                my_stack_view.push(customer_input_details, {products: products, customers: customers, usageOf:"popshop"})
            }
        }
    }

    FullWidthReminderText{
        id:title_page
        x: 0
        y:140
        remind_text:qsTr("Order Details")
        remind_text_size:"35"
    }

    Item{
        id:first_row
        visible: (usageOf=='sepulsa') ? false : true
        x:185
        y:195
        width: 750
        height: 150
        Column{
            spacing:10
            Text {
                id: name_label
                text: qsTr("Name")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
            Text {
                id: phone_label
                text: qsTr("Phone Number")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
            Text {
                id: email_label
                text: qsTr("Email")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
            Text {
                id: product_label
                text: qsTr("Product Name")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
        }
        Item{
            anchors.left: parent.left
            anchors.leftMargin: 215
            Column{
                spacing:10
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
            }
        }

        Item{
            y: 0
            anchors.left: parent.left
            anchors.leftMargin: 230
            Column{
                spacing:10
                Text {
                    id: name_text
                    text: cust_name
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                    font.capitalization: Font.Capitalize
                }
                Text {
                    id: phone_text
                    text: cust_phone
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: email_text
                    text: cust_email.toLowerCase()
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: product_text
                    text: prod_sku + " - " + prod_name
                    wrapMode: Text.WordWrap
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 24
                    width: 490
                    font.capitalization: Font.Capitalize
                }
            }
        }
    }

    Item{
        id:second_row
        visible: (usageOf=='sepulsa') ? false : true
        x:185
        y:360
        width: 750
        height: 150
        Column{
            spacing:10
            Text {
                id: qty_label
                text: qsTr("Quantity")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
            Text {
                id: delivery_label
                text: qsTr("Delivery Address")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
            Text {
                id: amount_label
                text: qsTr("Total Amount")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 25
            }
        }
        Item{
            anchors.left: parent.left
            anchors.leftMargin: 215
            Column{
                spacing:10
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
            }
        }

        Item{
            anchors.left: parent.left
            anchors.leftMargin: 230
            Column{
                spacing:10
                Text {
                    id: qty_text
                    width: 51
                    text: prod_qty + " " + qsTr("pcs")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
                Text {
                    id: locker_text
                    text: "PopBox @ " + selected_locker
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                    font.capitalization: Font.Capitalize
                }
                Text {
                    id: amount_text
                    text: new_amount
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 25
                }
            }
        }
    }

    Row{
        id: details_row_sepulsa
        visible: (usageOf=='sepulsa') ? true : false
        x:212
        y:200
        width: 600
        height: 300
        spacing:25
        Column{
            spacing:20
            Text {
                text: qsTr("Operator")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Phone Number")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Product Type")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Total Amount")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Product Name")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
        }
        Row{
            width: 400
            spacing:10
            Column{
                spacing:20
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
            }
            Column{
                spacing:20
                Text {
                    id: show_provider_txt
                    text: show_provider
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                    font.capitalization: Font.Capitalize
                }
                Text {
                    id: phone_no_txt
                    text: phone_no
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    id: product_type_txt
                    text: product_type
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                    font.capitalization: Font.Capitalize
                }
                Text {
                    id: amount_txt
                    text: insert_flg(amount)
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    id: service_txt
                    width: 500
                    height: 80
                    text: service
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                    font.capitalization: Font.Capitalize
                    wrapMode: Text.WordWrap
                }
            }
        }
    }

    Text {
        id: prepare_text
        x: 0
        y: 509
        width: 1024
        height: 104
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        font.pixelSize: 25
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text : qsTr("Please re-check your order before continuing.")
    }

    Text {
        id: confirm_text
        x: 0
        y: 709
        width: 1024
        height: 32
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        font.pixelSize: 24
        text: qsTr("By clicking confirm button you agree to the Terms and Conditions.")
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    DoorButton{
        id:ok_button
        y:613
        anchors.horizontalCenter: parent.horizontalCenter
        x:524
        show_text: (payment_channel!=undefined) ? qsTr("Agree") : qsTr("Confirm")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                ok_button.enabled = false
                if(payment_channel=="MANDIRI-EMONEY"){
                    payment_page.open()
                    show_timer_payment.restart()
                }else{
                    my_stack_view.push(select_payment_channel, {products: products,
                                                                customers: customers,
                                                                selected_locker: selected_locker,
                                                                product_class: usageOf,
                                                                popsafe_param: popsafe_param,
                                                                sepulsa_product: data_product})
                }
            }
        }
    }

    LoadingView{
        id:loading
    }

    PaymentWindow{
        id:payment_page
        //visible:true

        Text {
            id: text_payment_window
            x:334
            y:257
            width: 561
            height: 140
            text: qsTr("Please place your card to the reader")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:25
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_payment
                property int counter
                Component.onCompleted:{
                    time_payment.counter = show_timer_value
                }
            }
            Row{
                x:548
                y:399
                spacing:5
                Text {
                    id:show_time_payment_text
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    textFormat: Text.PlainText
                    font.pointSize:25
                    wrapMode: Text.WordWrap
                }
                Text {
                    id:show_seconds_left_text
                    color: "#ab312e"
                    text: qsTr("seconds left")
                    font.family:"Microsoft YaHei"
                    font.pixelSize: 25
                }
            }

            Timer{
                id:show_timer_payment
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    show_time_payment_text.text = time_payment.counter
                    time_payment.counter -= 1
                    if(time_payment.counter == 3){
                        slot_handler.start_global_payment_emoney(amount)
                        //my_stack_view.push(on_develop_view)
                    }
                    if(time_payment.counter < 0){
                        show_timer_payment.stop()
                        show_time_payment_text.visible = false
                        show_seconds_left_text.visible = false
                        ok_button.enabled = true
                        abc.counter = timer_value
                    }
                }
            }
        }
    }

    PaymentWindow{
        id:nonfinished_notif
        //visible:true

        Text {
            id: text_nonfisnihed_1
            x:130
            y:218
            width: 765
            height: 50
            text: qsTr("Transaction Unfinished")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:25
            wrapMode: Text.WordWrap
        }

        Text {
            id: text_nonfisnihed_2
            x:340
            y:285
            width: 555
            height: 88
            text: qsTr("Please put back your previous prepaid card.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:20
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_nf
                property int counter
                Component.onCompleted:{
                    time_nf.counter = show_timer_value_nf
                }
            }
            Row{
                x:563
                y:391
                spacing:5
                Text {
                    id:show_time_nf
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    textFormat: Text.PlainText
                    font.pointSize:25
                    wrapMode: Text.WordWrap
                }
                Text {
                    id: second_left_nf
                    color: "#ab312e"
                    text: qsTr("seconds left")
                    font.family:"Microsoft YaHei"
                    font.pixelSize: 25
                }
            }
            Row{
                x:862
                y:520
                width: 30
                height: 30
                spacing:5
                Text {
                    id:trial_count_text
                    width: 30
                    height: 30
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    textFormat: Text.PlainText
                    font.pointSize:18
                    wrapMode: Text.WordWrap
                }
            }

            Timer{
                id:show_timer_nf
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    show_time_nf.text = time_nf.counter
                    time_nf.counter -= 1
                    if(time_nf.counter == 3){
                        slot_handler.start_global_payment_emoney(amount)
                        //my_stack_view.push(on_develop_view)
                    }
                    if(time_nf.counter < 0){
                        show_timer_nf.stop()
                        show_time_nf.visible = false
                        second_left_nf.visible = false
                    }
                }
            }
        }
    }

    HideWindow{
        id:wrong_card_notif
        //visible:true

        Text {
            id: text_wc_1
            x: 93
            y:464
            width: 850
            height: 50
            text: qsTr("Transaction Failed")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:28
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            id: text_wc_2
            x: 93
            y:520
            width: 850
            height: 50
            text: qsTr("Please use Mandiri e-Money prepaid card.")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:25
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: img_wrong_card_notif
            x: 410
            y: 235
            width: 215
            height: 215
            fillMode: Image.PreserveAspectFit
            source: "img/otherservice/no_credit.png"
        }

        OverTimeButton{
            id:wrong_card_notif_back
            x:378
            y:589
            show_text:qsTr("Cancel")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
                    //slot_handler.start_reset_partial_transaction()
                }
            }
        }
    }

}
