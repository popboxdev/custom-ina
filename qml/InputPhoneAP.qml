import QtQuick 2.4
import QtQuick.Controls 1.2
import "door_price.js" as DOORS


BaseAP{
    id: baseAP
    property var press: '0'
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    property variant check_number: ['0814','0815', '0816', '0855', '0856', '0857', '0858',
        '0811', '0812', '0813', '0821', '0822', '0823', '0851', '0852', '0853',
        '0817', '0818', '0819', '0859', '0877', '0878', '0879',
        '0831', '0832', '0838', '9991', '9992', '9993', '9994', '9995', '9988',
        '0881', '0882', '0883', '0884', '0885', '0886', '0887', '0888', '0889',
        '0895', '0896', '0897', '0898', '0899']
    property var phone_number: ''
    property int timer_value: 30
    property int count: 0
    property var usageOf: undefined
    property bool useSamePrice: true
    property var doorSize: 'L'

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            phone_number = '';
            abc.counter = timer_value;
            my_timer.start();
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
        }
    }

    Component.onCompleted: {
    }

    Component.onDestruction: {

    }

    function compile_popsafe_data(no){    
        console.log(" DATA COMPILE POPSAFE: " + no)
        var price = DOORS.get_price('DEFAULT');
        if (!useSamePrice) price = DOORS.get_price(no)
        var customers = JSON.stringify({ "name": phone_number, "phone": phone_number, "email": phone_number})
        var products = JSON.stringify({"sku": "APXID", "name": "Locker APS Order|" + no, "qty": "1", "total_amount": price, "lockerSize": no})
        // my_stack_view.push(grocery_express_info, {customers: customers, products: products, usageOf: usageOf, selected_locker: selected_locker})
        my_stack_view.push(select_payment_ap, {customers: customers, products: products,phone_number: phone_number, product_class: usageOf})
    }

    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    Rectangle{
        id: timer_set
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Image {
        id: step_progress
        width: 500
        height: 51
        anchors.top: parent.top
        anchors.topMargin: 115
        anchors.horizontalCenter: parent.horizontalCenter
        source: "img/apservice/langkah/1.png"
    }

    Text {
        id: main_command_text
        width: 500
        height: 100
        color: "#323232"
        text: qsTr("Masukkan No HP")
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 180
        font.bold: true
        font.family: 'Microsoft YaHei'
        font.pixelSize: 50

    }

    Text {
        id: main_command_text_note
        width: 500
        height: 100
        color: "gray"
        text: qsTr("Notifikasi akan dikirimkan ke nomor ini, pastikan nomor benar")
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 325
        font.family: 'Microsoft YaHei'
        font.pixelSize: 19
        font.bold: true
    }

    Rectangle{
        id: phone_number_input
        width:400
        height:60
        color:"transparent"
        radius: 10
        anchors.top: parent.top
        anchors.topMargin: 255
        anchors.left: parent.left
        anchors.leftMargin: 50
        border.color: 'gray'
        border.width: 2
        Image {
            id: icon_phone
            scale: 1.2
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.verticalCenter: parent.verticalCenter
            source: (phone_number=='')  ? 'img/apservice/icon/hp_grey.png' : 'img/apservice/icon/hp_biru.png'
        }

        TextEdit{
            id:input_phone
            text:phone_number
            anchors.topMargin: 7
            anchors.leftMargin: 60
            anchors.fill: parent
            horizontalAlignment: Text.AlignLeft
            font.family:"Microsoft YaHei"
            color:"#323232"
            font.pixelSize:35
            font.bold: true

        }
    }

    Rectangle{
        id: base_ground_keyboard
        color: 'black'
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        opacity: 0.65
        width: parent.width
        height: 400
    }

    NumKeyboard{
        id:number_keyboard
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        functionPad: false
        enabled: !main_rec_dimm.visible

        Component.onCompleted: {
            number_keyboard.letter_button_clicked.connect(show_validate_code);
            // number_keyboard.function_button_clicked.connect(function_button_action);
        }

        NumButton{
            id: reset_button
            x: 0
            y: 270
            defaultColor: 'orange'
            img_source: ''
            show_text: qsTr('HAPUS')
            textColor: 'white'
            textSize: 20
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (count >= 1 || count <= 15){
                        if (count >=14)
                            count = count-1
                        count--;
                    }
                    if (count <=0){ 
                        count=0;
                    }
                    phone_number = phone_number.substring(0,phone_number.length-1);
                }
            }
        }

        NumButton{
            id: find_button
            x: 180
            y: 270
            defaultColor: 'green'
            img_source: ''
            show_text: qsTr('SELESAI')
            textColor: 'white'
            textSize: 20
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (press!='0') return
                    press = '1'
                    if(phone_number != ""){
                        console.log('Phone Number : ', phone_number);
                        if(check_number.indexOf(phone_number.substring(0,4)) > -1 && phone_number.length > 9){
                            dimm_act_confirm.visible=true;
                            number_keyboard.enabled = false;
                            back_button.enabled = false;
                            //compile_popsafe_data(doorSize)
                            count = 0;
                        }else{
                            main_rec_dimm.visible=true; 
                        }
                    }else{
                        main_rec_dimm.visible=true;
                    }
                }
            }
        }

        // function function_button_action(str){
        // }

        function show_validate_code(str){
            if (str != "" && count < 15){
                count++;
            }
            if (count >= 15){
                str="";
            }
            press = '0';
            phone_number += str;
            abc.counter = timer_value;
            my_timer.restart()
        }
    }

    // Notification{
    //     id: notif_text
    //     contentNotif: qsTr('Pastikan Nomor Telepon Anda adalah Benar.')
    //     successMode: false;

    // }
    Rectangle{
        id:main_rec_dimm
        visible:false
        width: 1024
        height: 768
        color: "#AA4A4A4A"

        Rectangle{
            id:sub_rec_dimm
            width: 650
            height: 500
            color: "white"
            radius: 22
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            Text{
                id:subject_text
                //x: 200
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.horizontalCenterOffset: 0
                //width: 300
                //text: prod_trans_id
                text: qsTr("Nomor Salah")
                anchors.top: parent.top
                anchors.topMargin: 10
                font.bold: true
                color:"#009BE1"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                font.pixelSize:43
            }

            Text{
                id:body_text
                x: 93
                width: 850
                height: 50
                text: qsTr("Format Nomor Salah,\nPastikan Nomor Anda Benar")
                anchors.horizontalCenterOffset: 0
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 160
                font.family:"Microsoft YaHei"
                color:"#4A4A4A"
                textFormat: Text.PlainText
                font.pixelSize: 26
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            Image {
                id: img_notif
                x: 410
                width: 215
                height: 215
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 220
                fillMode: Image.PreserveAspectFit
                source: "img/apservice/icon/wrongphone.png"
            }

            NotifButtonAP{
                id: button_notif
                anchors.top: parent.top
                anchors.topMargin: 400
                anchors.horizontalCenter: parent.horizontalCenter
                buttonColor: 'BLUE'
                modeReverse: false
                buttonText: qsTr('OK')

                MouseArea {
                    // visible: !isExtending
                    anchors.fill: parent
                    onClicked: main_rec_dimm.visible=false;
                }
            }
        }
    }

    Rectangle{
        id:dimm_act_confirm
        visible:false
        width: 1024
        height: 768
        color: "#AA4A4A4A"

        Rectangle{
            id: sub_dimm_act
            width: 600
            height: 460
            //border.color: "black"
            //color: "transparent"
            color:"white"
            radius: 22
            anchors.top: parent.top
            anchors.topMargin: 150
            anchors.left: parent.left
            anchors.leftMargin: 218

            Text {
                width: 500
                height: 100
                color: "#009BE1"
                text: qsTr("Periksa Kembali")
                anchors.left: parent.left
                anchors.leftMargin: 120
                anchors.top: parent.top
                anchors.topMargin: 50
                font.family: 'Microsoft YaHei'
                font.pixelSize: 45
                font.bold: true
            }

            Text {
                color: "grey"
                text: qsTr("Notifikasi akan dikirimkan ke nomor ini,")
                anchors.left: parent.left
                anchors.leftMargin: 70
                anchors.top: parent.top
                anchors.topMargin: 150
                font.family: 'Microsoft YaHei'
                font.pixelSize: 25
                font.bold: false
            }

            Text {
                color: "grey"
                text: qsTr("pastikan nomor benar")
                anchors.left: parent.left
                anchors.leftMargin: 170
                anchors.top: parent.top
                anchors.topMargin: 180
                font.family: 'Microsoft YaHei'
                font.pixelSize: 25
                font.bold: false
            }

            Text {
                color: "black"
                text: qsTr(phone_number)
                anchors.horizontalCenter: parent.horizontalCenter

                // anchors.left: parent.left
                // anchors.leftMargin:60
                anchors.top: parent.top
                anchors.topMargin: 230
                font.family: 'Microsoft YaHei'
                font.pixelSize: 60
                font.bold: true
            }

            Rectangle {
                id: btn_batal
                anchors.left: parent.left
                anchors.leftMargin: 80
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 110
                visible:true
                Image {
                    id: btl_logo
                    width: 200
                    height: 60
                    source: "img/courier08/08ground_field.png"

                    Text{
                        anchors.horizontalCenter : parent.horizontalCenter
                        anchors.verticalCenter : parent.verticalCenter
                        font.family: 'Microsoft YaHei'
                        font.pixelSize: 20
                        color: "#009BE1"
                        font.bold: true
                        text: qsTr("KEMBALI")
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            //press = '0';
                            count = 0;
                            dimm_act_confirm.visible = false;
                            number_keyboard.enabled = true;
                            back_button.enabled = true;
                            phone_number = "";
                        }
                    }   
                }
            } 

            Rectangle {
                id: btn_setuju
                anchors.right: parent.right
                anchors.rightMargin: 280
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 110
                //visible: true
                Image {
                    width: 200
                    height: 60
                    source: "img/courier08/08ground.1.png"

                    Text{
                        anchors.horizontalCenter : parent.horizontalCenter
                        anchors.verticalCenter : parent.verticalCenter
                        font.family: 'Microsoft YaHei'
                        font.pixelSize: 20
                        color: "#ffffff"
                        font.bold: true
                        text: qsTr("LANJUT")
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            //if (press!='0') return
                            //press = '1'
                            dimm_act_confirm.visible=false;
                            number_keyboard.enabled = true;
                            back_button.enabled = true;
                            compile_popsafe_data(doorSize)
                        }
                    }
                }
            }   
        }
    }    
}
