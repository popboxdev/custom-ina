import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:88
    height:88
    //radius: 22
    property var img_source:""
    property var show_text:""
    property var defaultColor: 'white'
    property var textColor: 'black'
    property int textSize: 24
    color: defaultColor

    Image{
        id:num_button
        width:88
        height:88
        source:img_source
    }

    Text{
        text:show_text
        color:textColor
        font.family:"Microsoft YaHei"
        font.pixelSize:textSize
        anchors.centerIn: parent;
        font.bold: true
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            full_keyboard.letter_button_clicked(show_text)
        }
        onEntered:{
            parent.color = "silver"
        }
        onExited:{
            parent.color = defaultColor
        }
    }

}
