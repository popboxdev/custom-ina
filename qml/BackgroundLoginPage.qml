import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    //width: 1024
    //height: 768
    id: baseAP
    //property var press: '0'
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    // property var secret_user:"POPDEV"
    // property var secret_code:"P0PD3V"
    property var press:"0"
    property int timer_value: 1000 //old = 60
    property variant pref_custom : ["AP1", "EVT", "KUR"]// "KNK", "GRA", "PIC", "TYK", "LOT", "VCS"]
    // property variant pref_dev : ["SPR"]

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log('IDENTITAS KURIR : ' + identity)
            courier_login_username.bool = true
            courier_login_psw.bool = false
            card_id.forceActiveFocus()
            if(courier_login_username.show_text != "" || courier_login_psw.show_text != ""){
                courier_login_username.show_text = ""
                courier_login_psw.show_text = ""
                ch=1
            }
            courier_login_button.show_source = "img/courier08/08ground_button.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    property var identity:""
    property var ch:1    

    Rectangle{        
        id:main_page
        Component.onCompleted: {
            root.user_login_result.connect(handle_result)
        }

        Component.onDestruction: {
            root.user_login_result.disconnect(handle_result)
        }

        function handle_result(result){
            main_page.enabled = true
            waiting.close()            
            if(result == "Success"){
                 if(identity == "LOGISTICS_COMPANY_USER"){
                    // if(prefixUse == courier_login_username.show_text.substring(0,3)){
                    //     my_stack_view.push(courier_service_view, {c_access:"limited",pref_login_user:courier_login_username.show_text.substring(0,3)})
                    // }else{
                    //     my_stack_view.push(courier_psw_error_view)
                    // }
                     console.log('PREFERENCES : ' + pref_custom.indexOf(courier_login_username.show_text.substring(0,3)))
                     console.log('USERNAME : ' + courier_login_username.show_text)
                     if(pref_custom.indexOf(courier_login_username.show_text.substring(0,3)) > -1){
                        //  my_stack_view.push(courier_service_view, {c_access:"limited",pref_login_user:courier_login_username.show_text.substring(0,3)})
                        my_stack_view.push(courier_service_view, {c_access:"limited",pref_login_user:courier_login_username.show_text.substring(0,3)})
                     }else{
                        //  my_stack_view.push(courier_service_view, {c_access:"full",pref_login_user:courier_login_username.show_text.substring(0,3)})
                        my_stack_view.push(courier_psw_error_view)
                     }
                }
                if(identity == "OPERATOR_USER"){
                    my_stack_view.push(manager_service_view)
                }
            }
            if(result == "Failure"){
                my_stack_view.push(courier_psw_error_view)
            }
            if(result == "NoPermission"){
                my_stack_view.push(courier_psw_error_view)
            }
            if(result == "NetworkError"){
                main_page.enabled = false
                network_error.open()
            }
        }

        CourierInput{
            id:courier_login_username
            x:150
            y:270
            show_image:"img/courier08/08user_blue.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    ch=1
                    courier_login_username.bool = true
                    courier_login_psw.bool = false
                }
            }
        }

        CourierInputPsw{
            id:courier_login_psw
            x:455
            y:270
            show_image:"img/courier08/08pass_blue.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    ch=2
                    courier_login_username.bool = false
                    courier_login_psw.bool = true
                }
            }
        }

        CourierLoginButton{
            id:courier_login_button
            x:760
            y:270
            MouseArea{
                anchors.fill:parent
                onClicked:{
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    // if(courier_login_username.show_text == secret_user && courier_login_psw.show_text == secret_code){
                    //     slot_handler.start_video_capture("usingSecretCode_" + courier_login_username.show_text)
                    //     slot_handler.start_user_bypass()
                    //     //slot_handler.start_post_capture_file()
                    //     main_page.enabled = false
                    //     secret_code_check.open()
                    // }else{
                    slot_handler.start_video_capture(identity + "_" + courier_login_username.show_text)
                    //slot_handler.start_post_capture_file()
                    slot_handler.background_login(courier_login_username.show_text,courier_login_psw.show_text,identity)
                    main_page.enabled = false
                    waiting.open()
                    // }
                }
                onEntered:{
                    courier_login_button.show_source = "img/bottondown/login_down.png"
                }
                onExited:{
                    courier_login_button.show_source = "img/courier08/08ground.png"
                }
            }
        }

        FullKeyboard{
            id:touch_keyboard
            x:29
            y:360
            property var count:0
            property var validate_c
            Component.onCompleted: {
                touch_keyboard.letter_button_clicked.connect(input_text)
                touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
            }

            function on_function_button_clicked(str){
                abc.counter = timer_value
                if(str == "ok"){

                    if(ch == 1){
                        if(courier_login_username.show_text != "" && courier_login_psw.show_text != ""){
                            if(press!="0"){
                                return
                            }
                            press = "1"
                            abc.counter = timer_value
                            my_timer.stop()
                            slot_handler.start_video_capture(courier_login_username.show_text)
                            //slot_handler.start_post_capture_file()
                            slot_handler.background_login(courier_login_username.show_text,courier_login_psw.show_text,identity)
                            main_page.enabled = false
                            waiting.open()
                        }else{
                            ch = 2
                            courier_login_username.bool = false
                            courier_login_psw.bool = true
                        }
                    }else if(ch == 2){
                        if(courier_login_username.show_text != "" && courier_login_psw.show_text != ""){
                            if(press!="0"){
                                return
                            }
                            press = "1"
                            abc.counter = timer_value
                            my_timer.stop()
                            // if(courier_login_username.show_text == secret_user && courier_login_psw.show_text == secret_code){
                            //     slot_handler.start_video_capture("usingSecretCode_" + courier_login_username.show_text)
                            //     slot_handler.start_user_bypass()
                            //     //slot_handler.start_post_capture_file()
                            //     main_page.enabled = false
                            //     secret_code_check.open()
                            // }else{
                            slot_handler.start_video_capture(courier_login_username.show_text)
                            //slot_handler.start_post_capture_file()
                            slot_handler.background_login(courier_login_username.show_text,courier_login_psw.show_text,identity)
                            main_page.enabled = false
                            waiting.open()
                        // }
                        }else{
                            ch = 1
                            courier_login_username.bool = true
                            courier_login_psw.bool = false
                        }
                    }
                }
            }

            function input_text(str){
                abc.counter = timer_value
                my_timer.restart()

                if(ch==1){
                    if (str == "" && courier_login_username.show_text.length > 0){
                        courier_login_username.show_text.length--
                        courier_login_username.show_text=courier_login_username.show_text.substring(0,courier_login_username.show_text.length-1 )
                    }
                    if (str != "" && courier_login_username.show_text.length< 14){
                        courier_login_username.show_text.length++
                    }
                    if (courier_login_username.show_text.length>=14){
                        str=""
                        courier_login_username.show_text.length=14
                    }else{
                        courier_login_username.show_text += str
                    }
                }

                if(ch==2){
                    if (str == "" && courier_login_psw.show_text.length > 0){
                        courier_login_psw.show_text.length--
                        courier_login_psw.show_text=courier_login_psw.show_text.substring(0, courier_login_psw.show_text.length-1)
                    }
                    if (str != "" && courier_login_psw.show_text.length < 14){
                        courier_login_psw.show_text.length++
                    }
                    if (courier_login_psw.show_text.length>=14){
                        str=""
                        courier_login_psw.show_text.length=14
                    }else{
                        courier_login_psw.show_text += str
                    }
                }
            }
        }
    }

    HideWindow{
        id:waiting
        //visible: true

        Image {
            id: img_time_waiting
            x: 437
            y: 400
            width: 150
            height: 200
            source: "img/otherImages/loading.png"
        }

        Text {
            x: 0
            y:300
            width: 1024
            height: 60
            text: qsTr("Please wait")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }
    }

    HideWindow{
        id:network_error
        //visible: true

        Image {
            id: img_network_error
            x: 423
            y: 408
            width: 180
            height: 180
            source: "img/otherImages/network-reconnect.png"
        }

        Text {
            id: text_network_error
            x: 0
            y:219
            width: 750
            height: 180
            text: qsTr("Network error, Please retry later")
            wrapMode: Text.WordWrap
            anchors.horizontalCenterOffset: 513
            anchors.horizontalCenter: parent.horizontalCenter
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:network_error_back_button
            x:373
            y:607
            show_text:qsTr("back")
            // show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(courier_login_username.show_text != "" || courier_login_psw.show_text != ""){
                        //courier_login_username.show_text = ""
                        courier_login_psw.show_text = ""
                        ch=2
                        courier_login_username.bool = false
                        courier_login_psw.bool = true
                    }
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    main_page.enabled = true
                    network_error.close()
                }
                onEntered:{
                    network_error_back_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    network_error_back_button.show_source = "img/button/7.png"
                }
            }
        }
    }

    // HideWindow{
    //     id:secret_code_check
    //     //visible: true

    //     Image {
    //         id: img_lock_secret
    //         x: 434
    //         y: 214
    //         width: 200
    //         height: 200
    //         source: "img/otherImages/lock_opened.png"
    //     }

    //     Text {
    //         x: 92
    //         y:386
    //         width: 848
    //         height: 200
    //         text: qsTr("Hi, secret code is enterred!\n Select Function Below?")
    //         font.family:"Microsoft YaHei"
    //         color:"#ffffff"
    //         textFormat: Text.PlainText
    //         font.pointSize:40
    //         horizontalAlignment: Text.AlignHCenter
    //         verticalAlignment: Text.AlignVCenter
    //     }

    //     OverTimeButton{
    //         id:secret_sure_button
    //         x:550
    //         y:576
    //         show_text:qsTr("Quit GUI")
    //         // show_x:15

    //         MouseArea {
    //             anchors.fill: parent
    //             onClicked: {
    //                 //my_stack_view.push(manager_service_view)
    //                 slot_handler.start_explorer()
    //                 Qt.quit()
    //             }
    //             onEntered:{
    //                 secret_sure_button.show_source = "img/bottondown/down1.png"
    //             }
    //             onExited:{
    //                 secret_sure_button.show_source = "img/button/7.png"
    //             }
    //         }
    //     }

    //     OverTimeButton{
    //         id:secret_admin_button
    //         x:200
    //         y:576
    //         show_text:qsTr("Access Doors")
    //         // show_x:15

    //         MouseArea {
    //             anchors.fill: parent
    //             onClicked: {
    //                 my_stack_view.push(manager_cabinet_view) //,{press_time:1})
    //             }
    //             onEntered:{
    //                 secret_sure_button.show_source = "img/bottondown/down1.png"
    //             }
    //             onExited:{
    //                 secret_sure_button.show_source = "img/button/7.png"
    //             }
    //         }
    //     }

    //     BackButton{
    //         id:secret_back_button
    //         x:130
    //         y:230
    //         show_text:qsTr("return")

    //         MouseArea {
    //             anchors.fill: parent
    //             onClicked: {
    //                 press = "0"
    //                 courier_login_username.bool = true
    //                 courier_login_psw.bool = false
    //                 courier_login_username.show_text=""
    //                 courier_login_psw.show_text=""
    //                 main_page.enabled = true
    //                 courier_login_button.enabled = true
    //                 secret_code_check.close()
    //             }
    //         }
    //     }

    //     Rectangle {
    //         id: camera_button
    //         x: 853
    //         y: 214
    //         width: 80
    //         height: 60
    //         color: "#00000000"
    //         anchors.topMargin: 0
    //         anchors.top: img_lock_secret.top
    //         Image {
    //             id: camera_image_button
    //             anchors.fill: parent
    //             fillMode: Image.PreserveAspectFit
    //             source: "img/item/camera.png"
    //         }
    //         MouseArea {
    //             anchors.fill: parent
    //             onClicked: {
    //                 my_stack_view.push(camera_capture_view)

    //             }
    //             onEntered:{
    //                 camera_image_button.source = "img/item/camera_red.png"
    //             }
    //             onExited:{
    //                 camera_image_button.source = "img/item/camera.png"
    //             }
    //         }
    //     }

    //     Rectangle {
    //         id: input_email
    //         x: 848
    //         y: 314
    //         width: 80
    //         height: 60
    //         color: "#00000000"
    //         anchors.topMargin: 70
    //         anchors.top: img_lock_secret.top
    //         Image {
    //             id: input_email_button
    //             anchors.fill: parent
    //             fillMode: Image.PreserveAspectFit
    //             source: "img/item/email.png"
    //         }
    //         MouseArea {
    //             anchors.fill: parent
    //             onClicked: {
    //                 my_stack_view.push(customer_input_email_view)

    //             }
    //             onEntered:{
    //                 input_email_button.source = "img/item/email-red.png"
    //             }
    //             onExited:{
    //                 input_email_button.source = "img/item/email.png"
    //             }
    //         }
    //     }

    //     Rectangle {
    //         id: test_settle_button
    //         x: 750
    //         y: 254
    //         width: 80
    //         height: 60
    //         color: "#00000000"
    //         anchors.topMargin: 0
    //         anchors.top: img_lock_secret.top
    //         Image {
    //             id: test_settle_button_image_button
    //             anchors.fill: parent
    //             fillMode: Image.PreserveAspectFit
    //             source: "img/courier08/08user.png"
    //         }

    //         MouseArea {
    //             anchors.fill: parent
    //             onClicked: {
    //                 console.log('test settle order')
    //                 slot_handler.start_push_data_settlement("TEST_SETTLEMENT")
    //             }
    //         }
    //     }

    // }

    Row {
        x: 154
        y: 271
        Text {
            id: title
            text: qsTr("CARDID:")
            font.pixelSize: 30
            visible: false
        }

        TextInput {
            id: card_id
            width: 80
            height: 20
            text: qsTr("")
            autoScroll: false
            focus: true
            font.pixelSize: 30
            visible: false
            onEditingFinished: {
                if(card_id.text==""){
                    return
                }
                else{
                    slot_handler.start_get_card_info(card_id.text, identity)
                    card_id.text = ""
                }
            }
        }
    }
}
