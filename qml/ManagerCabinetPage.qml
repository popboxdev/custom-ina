import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3

Background{
    width: 1024
    height: 768
    property var userAccess: "undefined"
    property var show_box_number:""
    property var show_size:""
    property var show_image_status:"img/manage22/fulllocker.png"
    property var show_image_size:"img/manage22/c.png"
    property int timer_value: 420
    property var press : "0"
    property var press_time:1
    property var box_id
    property var box_size
    property var box_status
    property var box_number
    property var json_express: new Array()
    property var id_express
    property var id_express_temp:""
    property var show_box_id_express:""
    property var show_id_express:""
    property var show_box_number_temp
    property var show_size_temp
    property var show_image_status_temp
    property var show_image_size_temp
    property var show_box_status
    property var box_id_list
    property var set_box_window_show_id:""
    property var set_box_window_show_last_store_time:""
    property var set_box_window_show_boxsize:""
    property var set_box_window_show_boxnum:""
    property var set_box_window_show_box_status:""
    property var set_box_window_show_box_id: ""
    property var setbox_status
    property var cabinet_data
    property var cabinet_num: 25
    property var page_num
    property var set_status_time:1
    property int size:10

    property var overdue_data: ""
    property var overdue_num
    property var overdue_id
    property bool isOverdue : false
    property bool enable_status : false
    property bool grid_act : false

    property var courierUsername: undefined
    property var courierName: undefined

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Timer {
        id: timer_startup
        interval: 1000
        repeat: false
        running: true
        onTriggered:
        {
            slot_handler.start_load_mouth_list(1)
            slot_handler.start_load_manager_mouth_count()
            slot_handler.start_operator_load_overdue_express_list(1)
            slot_handler.start_load_courier_overdue_express_count()
            press = '0';
        }
    }

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            page.enabled = false
            pick_up_Button2.enabled = false
            pick_up_Button1.enabled = false
            up_button.enabled = false
            down_button.enabled = false
            waiting.open()
            slot_handler.start_load_mouth_list(1)
            slot_handler.start_load_manager_mouth_count()
            slot_handler.start_operator_load_overdue_express_list(1)
            slot_handler.start_load_courier_overdue_express_count()
            down_button.enabled = false
            down_button.visible = false
            up_button.visible = false
            up_button.enabled = false
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            timer_startup.start();
            console.log("USER Access : ", userAccess)
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            timer_startup.start();
            slot_handler.stop_user_bypass()
        }
    }

    Item  {
        id: page
        x:66
        y:280
        width:750
        height:300

        GridView{
            id:gridview
            anchors.fill: parent
            delegate: griddel
            model:gridModel
            cellWidth: 180
            cellHeight: 60
            flow:Grid.TopToBottom
            interactive: false
            contentY: listview.contentY
        }

        ListView {
            id: listview
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
            anchors.fill: parent
            model: listModel
            delegate: delegate
            spacing:0
            interactive:false
        }

        Component {
            id: griddel

            Rectangle {
                color: "transparent"

                Text{
                    id:text_show_box_status
                    font.family:"Microsoft YaHei"
                    text:show_box_status
                    visible:false
                    anchors.horizontalCenterOffset: 810
                    color:"#ffffff"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }

                Text{
                    id:text_show_box_size
                    font.family:"Microsoft YaHei"
                    text:show_box_size
                    visible:false
                    anchors.horizontalCenterOffset: 810
                    color:"#ffffff"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }

                Image {
                    id: image1
                    x: 0
                    y: 0
                    width: 150
                    height: 50
                    source: show_image_status
                    enabled: enable_status

                    Text {
                        id: text1
                        x: 0
                        y: 0
                        width: 110
                        height: 50
                        text:show_box_number
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 12
                    }

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            abc.counter = timer_value
                            my_timer.restart()
                            set_box_window_show_id=show_box_number
                            //set_box_window_show_last_store_time = "2015-05-01"
                            set_box_window_show_boxsize=text_show_box_size.text
                            set_box_window_show_boxnum = show_box_number
                            set_box_window_show_box_status = text_show_box_status.text
                            set_box_window_show_box_id = show_box_id
                            show_box_id_express = show_id_express
                            setbox_status = show_box_status
                            box_id_list = show_box_id
                            page.enabled = false
                            pick_up_Button2.enabled = false
                            pick_up_Button1.enabled = false
                            up_button.enabled = false
                            down_button.enabled = false
                            set_box_window.open()
                        }
                    }
                }

                Image {
                    id: image2
                    x: 100
                    y: 0
                    width: 50
                    height: 50
                    source: show_image_size

                    Text {
                        id: text2
                        x: 0
                        y: 0
                        width: 50
                        height: 50
                        text:show_size
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 12
                    }
                }
            }
        }

        ListModel {
            id: gridModel
        }

        Component{
            id:delegate
            Rectangle{
                width: 150
                height: 70
                color: "transparent"
            }
        }

        ListModel{
            id:listModel
        }
    }

    Image {
        x: 466
        y: 203
        width: 500
        height: 50
        source: "img/manage22/statusmap.png"
    }

    Text {
        x: 785
        y: 174
        width: 300
        height: 50
        text: qsTr("Terlambat")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize: 12
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Text {
        x: 485
        y: 174
        width: 300
        height: 50
        text: qsTr("Not Use")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize: 12
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Text {
        x: 585
        y: 174
        width: 300
        height: 50
        text: qsTr("Available")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize: 12
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Text {
        x: 685
        y: 174
        width: 300
        height: 50
        text: qsTr("Used")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize: 12
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Text {
        x: 485
        y: 202
        width: 300
        height: 50
        text: qsTr("XS")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize: 12
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Text {
        x: 585
        y: 202
        width: 300
        height: 50
        text: qsTr("S")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize: 12
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Text {
        x: 685
        y: 202
        width: 300
        height: 50
        text: qsTr("M")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize: 12
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Text {
        x: 785
        y: 202
        width: 300
        height: 50
        text: qsTr("L")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize: 12
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Text {
        x: 885
        y: 202
        width: 300
        height: 50
        text: qsTr("XL")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize: 12
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Text {
        x: 76
        y: 200
        width: 300
        height: 50
        text: qsTr("From the ark set")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize: 30
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Image {
        x: 66
        y: 256
        width: 900
        source: "img/courier19/stripe.png"
    }

    PickUpButton {
        id:pick_up_Button2
        x: 65
        y: 638
        show_text: qsTr("exit")

        MouseArea {
            anchors.rightMargin: -4
            anchors.bottomMargin: -2
            anchors.leftMargin: 4
            anchors.topMargin: 2
            anchors.fill: parent
            onClicked:{
                my_stack_view.pop()
                slot_handler.stop_user_bypass()
            }
            onEntered:{
                pick_up_Button2.show_source = "img/bottondown/down1.png"
            }
            onExited:{
                pick_up_Button2.show_source = "img/button/7.png"
            }
        }
    }

    PickUpButton {
        id:pick_up_Button1
        x: 685
        y: 638
        show_text: qsTr("A key to unlock")
        visible: false

        MouseArea {
            anchors.leftMargin: 4
            anchors.bottomMargin: -2
            anchors.rightMargin: -4
            anchors.fill: parent
            anchors.topMargin: 2
            onClicked:{
                page.enabled = false
                pick_up_Button2.enabled = false
                pick_up_Button1.enabled = false
                up_button.enabled = false
                down_button.enabled = false
                one_key_open_window.open()
            }
            onEntered:{
                pick_up_Button1.show_source = "img/bottondown/down1.png"
            }
            onExited:{
                pick_up_Button1.show_source = "img/button/7.png"
            }
        }
    }

    Rectangle{
        id:up_button
        y:585
        x:60
        width:40
        height:40
        color:"transparent"

        Image{
            width:40
            height:40
            source:"img/05/back_red.png"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                abc.counter = timer_value
                my_timer.restart()
                gridModel.clear()
                listModel.clear()
                cabinet_data = ""
                down_button.visible = true
                down_button.enabled = true
                press_time = press_time-1
                slot_handler.start_load_mouth_list(press_time)
                if(press_time == 1){
                    up_button.visible = false
                    up_button.enabled = false
                }
            }
        }
    }

    Rectangle{
        id:down_button
        y:585
        x:660
        width:40
        height:40
        color:"transparent"
        anchors.right: parent.right
        anchors.rightMargin: 80

        Image{
            width:40
            height:40
            source:"img/05/ok_red.png"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                abc.counter = timer_value
                my_timer.restart()
                gridModel.clear()
                listModel.clear()
                cabinet_data = ""
                up_button.visible = true
                up_button.enabled = true
                press_time = press_time+1
                slot_handler.start_load_mouth_list(press_time)
                if(press_time == page_num){
                    down_button.visible = false
                    down_button.enabled = false
                }
            }
        }
    }

    HideWindow{
        id:one_key_open_window
        //visible: true

        Image {
            id: img_one_key_open_window
            x: 437
            y: 362
            width: 200
            height: 200
            source: "img/otherImages/lock_opened.png"
        }

        Text {
            y:266
            width: 1024
            height: 60
            text: qsTr("Is a key to unlock")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton4{
            id:yes_button1
            x:240
            y:598
            show_text:qsTr("Yes")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.start_manager_open_all_mouth()
                    page.enabled = true
                    pick_up_Button2.enabled = true
                    pick_up_Button1.enabled = true
                    up_button.enabled = true
                    down_button.enabled = true
                    one_key_open_window.close()
                    waiting.open()
                    page.enabled = false
                    pick_up_Button2.enabled = false
                    pick_up_Button1.enabled = false
                    up_button.enabled = false
                    down_button.enabled = false
                }
                onEntered:{
                    yes_button1.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    yes_button1.show_source = "img/button/7.png"
                }
            }
        }

        OverTimeButton4{
            id:no
            x:540
            y:598
            show_text:qsTr("No")
            show_x:205

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    page.enabled = true
                    pick_up_Button2.enabled = true
                    pick_up_Button1.enabled = true
                    up_button.enabled = true
                    down_button.enabled = true
                    one_key_open_window.close()
                }
                onEntered:{
                    no.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    no.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:waiting
        //visible: true

        Image {
            id: img_time_waiting
            x: 437
            y: 362
            width: 150
            height: 200
            source: "img/otherImages/loading.png"
        }

        Text {
            y:266
            width: 1024
            height: 60
            text: qsTr("Please wait")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }
    }

    HideWindow{
        id:set_ok
        //visible: true

        Image {
            id: img_set_ok
            x: 437
            y: 362
            width: 150
            height: 200
            source: "img/otherImages/checklist.png"
        }

        Text {
            y:266
            width: 1024
            height: 60
            text: qsTr("Success")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton{
            id:ok_back_button2
            x:373
            y:598
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    box_id_list = ""
                    page.enabled = true
                    pick_up_Button2.enabled = true
                    pick_up_Button1.enabled = true
                    up_button.enabled = true
                    down_button.enabled = true
                    set_ok.close()
                }
                onEntered:{
                    ok_back_button2.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    ok_back_button2.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:coming_soon
        //visible: true

        Image {
            id: img_coming_soon
            x: 437
            y: 362
            width: 150
            height: 200
            source: "img/otherImages/checklist.png"
        }

        Text {
            y:266
            width: 1024
            height: 60
            text: qsTr("Coming Soon")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton{
            id:ok_back1
            x:373
            y:598
            show_text:qsTr("back")
            show_x:15


            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    box_id_list = ""
                    page.enabled = true
                    pick_up_Button2.enabled = true
                    pick_up_Button1.enabled = true
                    up_button.enabled = true
                    down_button.enabled = true
                    coming_soon.close()
                }
                onEntered:{
                    ok_back1.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    ok_back1.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:set_failed
        //visible: true

        Image {
            id: img_set_failed
            x: 437
            y: 362
            width: 200
            height: 200
            source: "img/otherImages/x-mark.png"
        }

        Text {
            y:266
            width: 1024
            height: 60
            text: qsTr("Can not set the box")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton{
            id:failed_back_button
            x:373
            y:598
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    box_id_list = ""
                    page.enabled = true
                    pick_up_Button2.enabled = true
                    pick_up_Button1.enabled = true
                    up_button.enabled = true
                    down_button.enabled = true
                    set_failed.close()
                }
                onEntered:{
                    failed_back_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    failed_back_button.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:set_box_window

        Text {
            id: text1
            x: 166
            y: 288
            width:120
            height: 50
            color:"#FFFFFF"
            font.family:"Microsoft YaHei"
            text: qsTr("ID:")
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WrapAnywhere
            font.pixelSize: 40
        }

        Text {
            font.family:"Microsoft YaHei"
            text:set_box_window_show_box_id
            visible:false
            anchors.horizontalCenterOffset: 810
            color:"#FFFFFF"
            font.pixelSize:17
            anchors.centerIn: parent;
        }

        Text {
            id: text2
            x: 281
            y: 288
            width: 30
            height: 50
            color:"#FFFFFF"
            font.family:"Microsoft YaHei"       
            text: set_box_window_show_id
            font.pixelSize:40
        }

        Text {
            id: text_exp
            x: 281
            y: 200
            width: 30
            height: 50
            color:"#FFFFFF"
            font.family:"Microsoft YaHei"       
            text: show_box_id_express
            font.pixelSize:10
        }

        Image {
            x: 480
            y: 288
            width: 6
            height: 210
            source: "img/manager24/xian.png"
        }

        Text {
            id: text6
            x: 574
            y: 288
            width: 100
            height: 50
            color:"#FFFFFF"
            font.family:"Microsoft YaHei"
            text: qsTr("The door is type")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 30
        }

        Image {
            x: 770
            y: 285
            width: 143
            height: 58
            source: "img/18/02.png"

            Text {
                id: boxsize
                x: 0
                y: 0
                width: 143
                height: 58
                color: "#FFFFFF"
                text: set_box_window_show_boxsize
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                font.pixelSize: 45
            }
        }

        Text {
            id: text7
            x: 574
            y: 371
            width: 100
            height: 50
            color:"#FFFFFF"
            font.family:"Microsoft YaHei"
            text: qsTr("The door is number")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 30
        }

        Image {
            x: 770
            y: 370
            width: 143
            height: 58
            source: "img/18/02.png"

            Text {
                id: boxnum
                x: 0
                y: 0
                width: 143
                height: 58
                color: "#FFFFFF"
                text: set_box_window_show_boxnum
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                font.pixelSize: 45
            }
        }

        Text {
            id: text8
            x: 574
            y: 448
            width: 143
            height: 50
            color:"#FFFFFF"
            font.family:"Microsoft YaHei"
            text: qsTr("set enabled")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 30
        }

        Text {
            id: unenable_set
            visible: false
            x: 700
            y: 514
            width: 143
            height: 50
            color:"#FFFFFF"
            font.family:"Microsoft YaHei"
            text: qsTr("unenable to set the box")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 30
        }

        Image {
            x: 770
            y: 453
            width: 140
            height: 50
            source: "img/18/02.png"

            Text {
                id: text3
                x: 0
                y: 0
                width: 143
                height: 58
                color: "#32adb1"
                text: set_box_window_show_box_status
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                font.pixelSize: 30
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(set_status_time%2 == 1){
                            set_status_time = set_status_time + 1
                            if(set_box_window_show_box_status == "USED"){
                                set_to_enable.visible = false
                                set_to_locked.visible = false
                                unenable_set.visible = true
                            }
                            if(set_box_window_show_box_status != "USED"){
                                set_to_enable.visible = true
                                set_to_locked.visible = true
                                set_mouth_status_select.open()
                            }
                        }
                        else{
                            set_status_time = set_status_time + 1
                            set_mouth_status_select.close()
                        }
                    }
                }
            }
        }


        HideWindowButton1{
            id:manager_service_button
            x: 146
            y: 580
            show_icon:"img/manage25/serviceinfo.png"
            show_text:qsTr("unlocking")
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_manager_open_mouth(box_id_list)
                    box_id_list = ""
                    page.enabled = true
                    pick_up_Button2.enabled = true
                    pick_up_Button1.enabled = true
                    up_button.enabled = true
                    down_button.enabled = true
                    set_box_window.close()
                    unenable_set.visible = false
                    waiting.open()
                    page.enabled = false
                    pick_up_Button2.enabled = false
                    pick_up_Button1.enabled = false
                    up_button.enabled = false
                    down_button.enabled = false
                    set_mouth_status_select.close()
                }

                onEntered:{
                    manager_service_button.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    manager_service_button.show_source = "img/button/7.png"
                }
            }
        }

        HideWindowButton1{
            id:manager_service_button1
            x:335
            y: 580
            show_icon:"img/manage25/doormanage.png"
            show_text:qsTr("Empty the box")
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (show_box_id_express!=""){
                        json_express = [show_box_id_express]
                        console.log("DATA JSON : " + JSON.stringify(json_express))
                        
                        var op_taken = JSON.stringify({
                            "express_id": json_express[0],
                            "op_username": courierUsername,
                            "op_name": courierName
                        })
                        console.log("KOSONGKAN_BOX: " + op_taken)
                        
                        slot_handler.operator_taken(op_taken)
                        // slot_handler.start_courier_take_reject_express(JSON.stringify(json_express))   
                        // slot_handler.start_courier_take_send_express(JSON.stringify(show_box_id_express))  
                    }else{
                        console.log("DATA JSON : KOSONG") //+ JSON.stringify(show_box_id_express))
                    }

                    slot_handler.start_manager_set_mouth(set_box_window_show_box_id,setbox_status)
                    gridModel.clear()
                    listModel.clear()
                    cabinet_data = ""
                    slot_handler.start_load_mouth_list(1)
                    slot_handler.start_load_manager_mouth_count()
                    set_mouth_status_select.close()
                    // slot_handler.start_courier_take_reject_express(JSON.stringify(json_express)) 
                    set_status_time = 1
                    press_time = 1
                    set_box_window.close()
                    unenable_set.visible = false
                    page.enabled = true
                    pick_up_Button2.enabled = true
                    pick_up_Button1.enabled = true
                    up_button.enabled = false
                    up_button.visible = false
                    down_button.enabled = true

                    // abc.counter = timer_value
                    // my_timer.restart()
                    // set_mouth_status_select.close()
                    // set_status_time = 1
                    // set_box_window.close()
                    // unenable_set.visible = false
                    // page.enabled = true
                    // pick_up_Button2.enabled = true
                    // pick_up_Button1.enabled = true
                    // up_button.enabled = true
                    // down_button.enabled = true
                    // coming_soon.open()
                    // json_express = show_box_id_express
                }
                onEntered:{
                    manager_service_button1.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    manager_service_button1.show_source = "img/button/7.png"
                }
            }
        }

        HideWindowButton1{
            id:manager_service_button2
            x: 524
            y: 580
            show_icon:"img/manage25/error.png"
            show_text:qsTr("sure")
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_manager_set_mouth(set_box_window_show_box_id,setbox_status)
                    gridModel.clear()
                    listModel.clear()
                    cabinet_data = ""
                    slot_handler.start_load_mouth_list(1)
                    slot_handler.start_load_manager_mouth_count()
                    set_mouth_status_select.close()
                    set_status_time = 1
                    press_time = 1
                    set_box_window.close()
                    unenable_set.visible = false
                    page.enabled = true
                    pick_up_Button2.enabled = true
                    pick_up_Button1.enabled = true
                    up_button.enabled = false
                    up_button.visible = false
                    down_button.enabled = true
                }
                onEntered:{
                    manager_service_button2.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    manager_service_button2.show_source = "img/button/7.png"
                }
            }
        }


        HideWindowButton1{
            id:manager_service_button3
            x: 714
            y: 580
            show_icon:"img/manage25/overduetime.png"
            show_text:qsTr(" return")
            MouseArea {
                anchors.fill: parent
                onClicked:{
                    abc.counter = timer_value
                    my_timer.restart()
                    set_mouth_status_select.close()
                    set_status_time = 1
                    set_box_window.close()
                    unenable_set.visible = false
                    page.enabled = true
                    pick_up_Button2.enabled = true
                    pick_up_Button1.enabled = true
                    up_button.enabled = true
                    down_button.enabled = true
                }
                onEntered:{
                    manager_service_button3.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    manager_service_button3.show_source = "img/button/7.png"
                }
            }
        }

    }

    HideSelectWindow{
        id:set_mouth_status_select

        Row {
            x: 650
            y: 512
            spacing: 5

            HideSelectButton{
                id:set_to_enable
                visible: false
                show_icon:"img/manage25/overduetime.png"
                show_text:qsTr(" ENABLE")
                MouseArea {
                    anchors.fill: parent
                    onClicked:{
                        setbox_status = "ENABLE"
                        set_box_window_show_box_status = "ENABLE"
                        set_mouth_status_select.close()
                        set_status_time = 1
                    }
                    onEntered:{
                        set_to_enable.show_source = "img/bottondown/down1.png"
                    }
                    onExited:{
                        set_to_enable.show_source = "img/button/7.png"
                    }
                }
            }

            HideSelectButton{
                id:set_to_locked
                visible: false
                show_icon:"img/manage25/overduetime.png"
                show_text:qsTr(" LOCKED")
                MouseArea {
                    anchors.fill: parent
                    onClicked:{
                        setbox_status = "LOCKED"
                        set_box_window_show_box_status = "LOCKED"
                        set_mouth_status_select.close()
                        set_status_time = 1
                    }
                    onEntered:{
                        set_to_locked.show_source = "img/bottondown/down1.png"
                    }
                    onExited:{
                        set_to_locked.show_source = "img/button/7.png"
                    }
                }
            }
        }
    }

    Component.onCompleted:{
        root.overdue_express_list_result.connect(overdue_express_list)
        root.overdue_express_count_result.connect(overdue_count)

        root.manager_mouth_count_result.connect(mouth_count)
        root.mouth_list_result.connect(show_mouth_list)
        // root.overdue_list_result.connect(show_overdue_list)
        root.load_mouth_list_result.connect(load_list_result)
        root.manager_set_mouth_result.connect(log_text)
        root.manager_open_mouth_by_id_result.connect(log_text)
        root.manager_open_all_mouth_result.connect(log_text)
    }

    Component.onDestruction:{
        root.overdue_express_list_result.disconnect(overdue_express_list)
        root.overdue_express_count_result.disconnect(overdue_count)

        root.manager_mouth_count_result.disconnect(mouth_count)
        root.mouth_list_result.disconnect(show_mouth_list)
        // root.overdue_list_result.disconnect(show_overdue_list)
        root.load_mouth_list_result.disconnect(load_list_result)
        root.manager_set_mouth_result.disconnect(log_text)
        root.manager_open_mouth_by_id_result.disconnect(log_text)
        root.manager_open_all_mouth_result.disconnect(log_text)
    }

    function overdue_express_list(text){
        overdue_data = text
        // combine_param(overdue_data)
        var obj = JSON.parse(overdue_data)
        for(var i in obj){
            overdue_id = obj[i].id
            // console.log("OVERDUE LIST : ", i + "OVERDUE ID : ", overdue_id)
        }
        // show_overdue_express(text)
        if(grid_act==false){
            gridModel.clear()
            listModel.clear()
            grid_act=true
        }
    }

    function overdue_count(text){
        overdue_num = text
        console.log("OVERDUE NUMBER : ", text)
        count_page(text)
        // if(text<=5){
        //     down_button.enabled = false
        //     down_button.visible = false
        // }
        // else{
        //     down_button.enabled = true
        //     down_button.visible = true
        // }
    }

    function load_list_result(text){
        console.log("load_list_result : ", text)
        waiting.close()
        page.enabled = true
        pick_up_Button2.enabled = true
        pick_up_Button1.enabled = true
        up_button.enabled = true
        down_button.enabled = true
    }

    function show_mouth_list(text){
        // console.log("mouth_list_result : ", text)
        cabinet_data = text
        show_cabinet_data(cabinet_data)
    }

    // function show_overdue_list(text){
    //     console.log("show_overdue_list : ", text)
    //     overdue_data = text
    //     show_overdue_data(overdue_data)
    // }

    function count_page(cabinet_num){
        if(cabinet_num/25 > Math.floor(cabinet_num/25)){
            page_num = Math.floor(cabinet_num/25)+1
        }
        else
            page_num = cabinet_num/25
    }

    function mouth_count(text){
        // console.log("manager_mouth_count_result : ", text)
        cabinet_num = text
        count_page(text)
        if(text<=25){
            down_button.enabled = false
            down_button.visible = false
        }
        else{
            down_button.enabled = true
            down_button.visible = true
        }
    }

    // function show_overdue_data(overdue_data){
    //     var obj = JSON.parse(overdue_data)
    // }

    function show_cabinet_data(cabinet_data){
        var obj = JSON.parse(cabinet_data)
        for(var i in obj){
            id_express_temp = ""
            box_id = obj[i].id
            id_express = obj[i].express_id
            box_status = obj[i].status
            if(overdue_data!=""){
                    var obj2 = JSON.parse(overdue_data)
                    for(var x in obj2){
                        overdue_id = obj2[x].id
                        // console.log("OVERDUE LIST : ", overdue_id)
                        if (id_express==overdue_id){
                            isOverdue = true
                            console.log("OVERDUE LIST : ", overdue_id)
                        }
                    }
            }
            // console.log("JUMLAH OVERDUE : ", isOverdue)
            if(box_status == "ENABLE"){
                if((userAccess.indexOf("SPR") > -1) || (userAccess.indexOf("OPS") > -1)){
                    enable_status = true
                    show_image_status_temp="img/manage22/freelocker.png"
                }else{
                    enable_status = false
                    show_image_status_temp = "img/manage22/freelocker_inactive.png"
                }
            }
            else if(box_status == "USED"){
                if(isOverdue == true){
                    show_image_status_temp="img/manage22/overduelocker.png"
                }else{
                    show_image_status_temp="img/manage22/fulllocker.png"
                }
                if(userAccess.indexOf("OPS") > -1){
                    enable_status = false
                    show_image_status_temp = "img/manage22/freelocker_inactive.png"
                }else{
                    enable_status = true
                }
                id_express_temp = id_express
            }
            else{
                show_image_status_temp="img/manage22/badlocker.png"
            }
            // console.log("STATUS ENABLE ", enable_status)
            isOverdue = false
            box_number = obj[i].number
            show_box_number_temp=box_number
            box_size = obj[i].name
            if(box_size=="MINI"){
                show_image_size_temp="img/manage22/a.png"
                show_size_temp="XS"
            }
            else if(box_size=="S"){
                show_image_size_temp="img/manage22/b.png"
                show_size_temp="S"
            }
            else if(box_size=="M"){
                show_image_size_temp="img/manage22/c.png"
                show_size_temp="M"
            }
            else if(box_size=="L"){
                show_image_size_temp="img/manage22/d.png"
                show_size_temp="L"
            }
            else{
                show_image_size_temp="img/manage22/e.png"
                show_size_temp="XL"
            }
            gridModel.append({"show_image_status":show_image_status_temp , "show_image_size": show_image_size_temp , "show_box_number": show_box_number_temp , "show_size": show_size_temp , "show_box_id":box_id , "show_box_size":box_size , "show_box_status":box_status, "show_id_express":id_express_temp, "enable_status":enable_status});
            listModel.append({});
        }
    }

    function log_text(text){
        console.log("log_text : ", text)
        page.enabled = true
        pick_up_Button2.enabled = true
        pick_up_Button1.enabled = true
        up_button.enabled = true
        down_button.enabled = true
        waiting.close()
        box_id_list = ""
        if(text == 'Success'){
            page.enabled = false
            pick_up_Button2.enabled = false
            pick_up_Button1.enabled = false
            up_button.enabled = false
            down_button.enabled = false
            set_ok.open()
        }
        else{
            page.enabled = false
            pick_up_Button2.enabled = false
            pick_up_Button1.enabled = false
            up_button.enabled = false
            down_button.enabled = false
            set_failed.open()
        }
    }

    // function combine_param(cabinet_data, overdue_data){
    //     console.log("KABINET DATA : ", cabinet_data)
    //     console.log("KABINET DATA : ", overdue_data)
    // }
}
