import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: other_search_fail_page

    property int timer_value: 30
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }


    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


    FullWidthReminderText{
       id:text
       y:280
       remind_text:qsTr("Sorry we're unable to verify the order number")
       remind_text_size:"35"
    }

    FullWidthReminderText{
       y:330
       remind_text:qsTr("Please contact Customer Service at")
       remind_text_size:"35"
    }

    FullWidthReminderText{
       y:380
       remind_text:qsTr("021 290 22537 for the assistance")
       remind_text_size:"35"
    }

    DoorButton{
        id:back_button
        y:604
        x:370
        show_text:qsTr("Back")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                back_button.show_image = "img/door/2.png"
            }
            onExited:{
                back_button.show_image = "img/door/1.png"
            }
        }
    }


}
