import QtQuick 2.4
import QtQuick.Controls 1.2
import Qt.labs.folderlistmodel 1.0
import QtMultimedia 5.0

Rectangle{
    id:parent_root
    color: "black"
    property var img_path: "/advertisement/video/"
    property url img_path_: ".." + img_path
    property var qml_pic
    property string pic_source: ""
    property int num_pic
    property string mode // ["staticVideo", "mediaPlayer", "liveView"]
//    property var list_pic: img_files
    property variant media_files: []
//    property int index: root.media_idx

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log('ads mode : ' +  mode)
            if(mode=="mediaPlayer" && media_files.length == 0){
                slot_handler.start_get_file_dir(img_path);
            }
        }
        if(Stack.status==Stack.Deactivating){
            player.stop()
            while (media_files.length > 0) {
                media_files.pop();
            }
        }
    }

    Component.onCompleted: {
        root.start_get_file_dir_result.connect(get_result)
        root.start_get_detection_result.connect(get_detection)
    }

    Component.onDestruction: {
        root.start_get_file_dir_result.disconnect(get_result)
        root.start_get_detection_result.disconnect(get_detection)
    }

    function get_detection(result){
        console.log("get_detection : ", JSON.stringify(result))
        if (result=="FAILED" || result=="ERROR"){
            return
        } else {
            var handle = JSON.parse(result)
            var detections = handle.result_face + handle.result_motion
            if (detections > 0){
                player.stop()
                while (media_files.length > 0) {
                    media_files.pop();
                }
                my_stack_view.pop()
                root.reference = "people_count"
            }
        }
    }


    function get_result(result){
        if (result == "ERROR" || result == ""){
            console.log("No Media Files!")
        } else {
            var files = JSON.parse(result)
            media_files = files.output
            console.log("Media Files (" + media_files.length + ") : " + media_files)
            if (media_files.length > 0){
                media_mode.setIndex(root.media_idx);
            } else{
                console.log("Cannot Play Media!")
            }
        }
    }


    // Play Multiple Videos
    Rectangle {
        id: media_mode
        visible: (mode=="mediaPlayer") ? true : false
        width: 1024
        height: 768
        color: "black"

        function setIndex(i){
            root.media_idx = i;
            root.media_idx %= media_files.length;
            player.source = img_path_ + media_files[root.media_idx];
            slot_handler.start_post_tvclog(media_files[root.media_idx])
            console.log('Playing Media [', root.media_idx, '-', media_files[root.media_idx], ']')
            player.play()
        }

        function next(){
            setIndex(root.media_idx + 1);
        }

        function previous(){
            setIndex(root.media_idx - 1);
        }

        Connections {
            target: player
            onStopped: {
                if (player.status == MediaPlayer.EndOfMedia) {
                    if (root.media_idx==media_files.length-1){ //Looping start from beginning
                        media_mode.setIndex(0);
                    } else{
                        media_mode.next();
                    }
                }
            }
        }

        MediaPlayer {
            id: player
        }

        VideoOutput {
            anchors.fill: parent
            source: player
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                player.stop()
                while (media_files.length > 0) {
                    media_files.pop();
                }
                slot_handler.start_post_activity("ReleaseTVC")
                my_stack_view.pop()
            }
            onDoubleClicked: {
                player.stop()
                while (media_files.length > 0) {
                    media_files.pop();
                }
                slot_handler.start_post_activity("ReleaseTVC")
                my_stack_view.pop();
            }
        }

    }

    // Play Static Single Video
    /*Video {
        id: video
        visible: (mode=="staticVideo") ? true : false
        anchors.fill: parent
        source: img_path + "sample_1.mp4"
        focus: true

        MouseArea{
            anchors.fill: video
            onClicked: {
                if (video.playbackState == MediaPlayer.PlayingState) {
                    video.pause()
                } else {
                    video.play()
                }
            }
        }
    }*/

    //ViewFinder from Camera
   /* Item {
        visible: (mode=="liveView") ? true : false
        width: 1023
        height: 768

        Camera {
            id: camera
            imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash
            exposure {
                exposureCompensation: -1.0;
                exposureMode: Camera.ExposurePortrait;
            }

            imageCapture {
                onImageCaptured: {
                    // Show the preview in an Image
                    photoPreview.source = preview;
                    photoPreview.visible = true
                }
            }
        }

        VideoOutput {
            id: vo
            source: camera
            focus : visible // to receive focus and capture key events when visible
            anchors.fill: parent

            MouseArea {
                id: voMouse
                visible: vo.focus
                anchors.fill: parent;
                onClicked: {
                    camera.imageCapture.capture();
                    text.text = "Captured...";
                }
            }
        }

        Image {
            id: photoPreview
            fillMode: Image.PreserveAspectFit
            MouseArea{
                id: previewMouse
                visible: photoPreview.visible
                anchors.fill: photoPreview
                onClicked: {
                    if(photoPreview.visible == true){
                        photoPreview.visible = false;
                        vo.focus = true;
                        text.text = "Live View...";
                    }
                }
            }
        }

        Text{
            id: text
            x: 25
            y: 25
            color: "yellow"
            font.pointSize: 20
            font.italic: true
            font.family: "Verdana"
            text: "Live View..."
        }

    }*/


}

