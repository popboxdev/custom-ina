import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:187
    height:91
    color:"#3b8f23"
    radius: 22
    property var show_text:qsTr("OK")
    property var show_source:""

    Image{
        width:187
        height:91
        source:show_source
    }

    Text{
        x: 25
        y: 31
        font.family:"Microsoft YaHei"
        text:show_text
        font.bold: true
        color:"#ffffff"
        font.pixelSize:24
        anchors.centerIn: parent;
    }

}

