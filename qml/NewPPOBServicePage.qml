import QtQuick 2.4
import QtQuick.Controls 1.2
import "button_payment.js" as BUTTON

Background{
    id:ppob_service_page
    property int timer_value: 60
    property var press: "0"
    property var emoney_reader: undefined
    property var data_buttons: BUTTON.button_list

    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_get_cod_status();
            abc.counter = timer_value;
            my_timer.restart();
            press = "0";
            all_rec.color = 'white';
            all_text.color = 'darkred';
            parse_button();
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_cod_status_result.connect(cod_result);
    }

    Component.onDestruction: {
        root.start_get_cod_status_result.disconnect(cod_result)
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }
    }

    function cod_result(result){
        console.log("cod_result is ", result)
        if(result==""){
            emoney_reader = "disabled"
        }else{
            emoney_reader = result
        }
    }

    function get_image(id, name){
        switch (id){
        case 9:
            return "img/otherservice/cop_white.png";
        case 16:
            return "img/button/popsend_white.png";
        default:
            return "img/ppob/"+ name +".png";
        }
    }

    function parse_button(tab){
        buttonItem_listModel.clear()
        gridview.model = undefined;
        var buttons = data_buttons;
//        console.log('buttons', JSON.stringify(buttons));
        for (var i in buttons){
            var button_name = buttons[i].name;
            var button_id = buttons[i].id;
            var button_parent = buttons[i].parent;
            var button_tab = buttons[i].tab;
            if (buttons[i].status==1 && buttons[i].parent=='0'){
                if (tab==undefined){
                    buttonItem_listModel.append({'_name_' : button_name,
                                                 '_id_' : button_id,
                                                 '_parent_id_': button_parent,
                                                 '_tab_': button_tab
                                                });
                }
                if (buttons[i].tab==tab){
                    buttonItem_listModel.append({'_name_' : button_name,
                                                 '_id_' : button_id,
                                                 '_parent_id_': button_parent,
                                                 '_tab_': button_tab
                                                });
                }
            }
        }
        gridview.model = buttonItem_listModel;
        if (tab==undefined) tab = 'ALL';
        console.log('Total Buttons : ' + buttonItem_listModel.count + ' in ' + tab);
    }

    function reset_button(){
        all_rec.color = 'transparent';
        purchase_rec.color = 'transparent';
        payment_rec.color = 'transparent';
        topup_rec.color = 'transparent';
        ticket_rec.color = 'transparent';
        all_text.color = 'white';
        purchase_text.color = 'white';
        payment_text.color = 'white';
        topup_text.color = 'white';
        ticket_text.color = 'white';
        press = '0';
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


    Row{
        x: 0
        y: 165
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 50
        Rectangle{
            id: all_rec
            color: 'white'
            width: 130
            height: 30
            Text {
                id: all_text
                anchors.fill: parent
                text: qsTr("All")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: 'darkred'
                font.bold: false
                font.pixelSize: 20
                font.family:"Microsoft YaHei"

            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    reset_button();
                    parse_button();
                    all_rec.color = 'white';
                    all_text.color = 'darkred';
                }
            }
        }
        Rectangle{
            id: purchase_rec
            color: 'transparent'
            width: 130
            height: 30
            Text {
                id: purchase_text
                anchors.fill: parent
                text: qsTr("Purchase")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: 'white'
                font.bold: false
                font.pixelSize: 20
                font.family:"Microsoft YaHei"

            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    reset_button();
                    parse_button('Purchase');
                    purchase_rec.color = 'white';
                    purchase_text.color = 'darkred';
                }
            }
        }
        Rectangle{
            id: payment_rec
            color: 'transparent'
            width: 130
            height: 30
            Text {
                id: payment_text
                anchors.fill: parent
                text: qsTr("Payment")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: 'white'
                font.bold: false
                font.pixelSize: 20
                font.family:"Microsoft YaHei"

            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    reset_button();
                    parse_button('Payment');
                    payment_rec.color = 'white';
                    payment_text.color = 'darkred';
                }
            }
        }
        Rectangle{
            id: topup_rec
            color: 'transparent'
            width: 130
            height: 30
            Text {
                id: topup_text
                anchors.fill: parent
                text: qsTr("Top Up")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: 'white'
                font.bold: false
                font.pixelSize: 20
                font.family:"Microsoft YaHei"

            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    reset_button();
                    parse_button('Top Up');
                    topup_rec.color = 'white';
                    topup_text.color = 'darkred';
                }
            }
        }
        Rectangle{
            id: ticket_rec
            color: 'transparent'
            width: 130
            height: 30
            Text {
                id: ticket_text
                anchors.fill: parent
                text: qsTr("Ticket")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: 'white'
                font.bold: false
                font.pixelSize: 20
                font.family:"Microsoft YaHei"

            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    reset_button();
                    parse_button('Ticket');
                    ticket_rec.color = 'white';
                    ticket_text.color = 'darkred';
                }
            }
        }
    }

    Item{
        id: button_item_view
        x: 8
        y: 201
        width: 1008
        height: 559
        focus: true

        GridView{
            id: gridview
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.topMargin: 0
            anchors.leftMargin: 31
            anchors.fill: parent
            focus: true
            clip: true
            flickDeceleration: 750
            maximumFlickVelocity: 1500
            layoutDirection: Qt.LeftToRight
            flow: GridView.FlowLeftToRight
            boundsBehavior: Flickable.StopAtBounds
            snapMode: GridView.SnapToRow
            anchors.margins: 20
            delegate: delegate_item_view
            //model: groceryItem_listModel
            cellWidth: 225
            cellHeight: 215
        }

        ListModel {
            id: buttonItem_listModel
            dynamicRoles: true
        }

        Component{
            id: delegate_item_view
            PPOBServiceButton {
                width: 245
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                show_text: _name_
                show_image: get_image(_id_, _name_)

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "0"){
                            return
                        }
                        press = "1"
                        my_timer.stop()
//                        console.log('define process :', _id_, _tab_)
                        define_process(_id_, _name_)
                    }
                }
            }
        }
    }

    function define_process(id, tab){
        switch(id){
        case 1:
            my_stack_view.push(grocery_product_list_new, {emoney:emoney_reader});
            break;
        case 2: case 9:
            my_stack_view.push(other_input_memory_page);
            break;
        case 3: case 4: case 5: case 20:
            my_stack_view.push(ppob_view_section, {_parent_id_: id,
                                                   _tab_: tab,
                                                   data_buttons:
                                                   data_buttons,
                                                   emoney_reader: emoney_reader}
                                                   );
            break;
        case 15:
            my_stack_view.push(sepulsa_input_phone_view);
            break;
        case 16:
            my_stack_view.push(courier_input_phone_view, {usageOf:"popsend_topup"});
            break;
        default:
            my_stack_view.push(on_develop_view);
            break;
        }
    }
}
