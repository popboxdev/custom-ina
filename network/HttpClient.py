import logging
import requests
import Configurator
import json

NOT_INTERNET_ = {
    'statusCode': -1,
    'statusMessage': 'Not Internet'}

__author__ = 'wahyudi@popbox.asia'
url = Configurator.get_value('ClientInfo', 'ServerAddress')
order_no = Configurator.get_value('ClientInfo', 'OrderNo')
box_token = Configurator.get_value('ClientInfo', 'Token')
user_token = ''
_DISK_SN_ = ''
_LOG_ = logging.getLogger()


def set_user_token(token):
    global user_token
    user_token = token


def clean_user_token():
    global user_token
    user_token = ''


def get_header():
    header = {
        'OrderNo': order_no,
        'BoxToken': box_token,
        'DiskSerialNumber': _DISK_SN_}
    if user_token != '':
        header['UserToken'] = user_token
    return header


def get_message(url_param, msg=None):
    header = get_header()
    # _LOG_.info('header' + str(header) + 'url:' + str(url_param) + '; json: ' + str(msg))

    try:
        r = requests.get(url + url_param, headers=header, json=msg, timeout=50)
    except requests.RequestException:
        _LOG_.warning((NOT_INTERNET_, -1))
        return NOT_INTERNET_, -1

    try:
        r_json = r.json()
    except ValueError:
        _LOG_.warning(('ValueError', r.status_code))
        return NOT_INTERNET_, r.status_code

    _LOG_.debug(('<URL> : ' + str(url + url_param) + ", <STAT> : " + str(r.status_code) + ", <RESP> : " +
                 str(json.dumps(r_json))))
    return r_json, r.status_code


def post_message(url_param, msg=None):
    header = get_header()
    _LOG_.info('header' + str(header) + 'url:' + str(url_param) + '; json: ' + str(msg))

    try:
        r = requests.post(url + url_param, headers=header, json=msg, timeout=50)
    except requests.RequestException:
        _LOG_.debug((NOT_INTERNET_, -1))
        return NOT_INTERNET_, -1

    try:
        r_json = r.json()
    except ValueError:
        _LOG_.warning(('ValueError', r.status_code))
        return NOT_INTERNET_, r.status_code

    if 'Log' not in url_param:
        _LOG_.debug(('<URL> : ' + str(url + url_param) + ", <POST> : " + str(json.dumps(msg)) + ", <RESP> : " +
                     str(json.dumps(r_json))))

    _LOG_.info('POST MESSAGE STATUS : ' + ', JSON : ' + str(r_json) + ', Status Code : ' + str(r.status_code))
    return r_json, r.status_code


def pakpobox_get_message(url_param, msg=None):
    # logger.info('url:' + str(url_param) + '; json: ' + str(msg))

    try:
        r = requests.post(url_param, json=msg, timeout=50)
    except requests.RequestException:
        _LOG_.debug((NOT_INTERNET_, -1))
        return NOT_INTERNET_, -1
    try:
        r_json = r.json()
    except ValueError:
        _LOG_.warning(('ValueError', r.status_code))
        return NOT_INTERNET_, r.status_code

    _LOG_.debug(('<URL> : ' + str(url_param) + ", <STAT> : " + str(r.status_code) + ", <RESP> : " +
                 str(json.dumps(r_json))))
    return r_json, r.status_code
