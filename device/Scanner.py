import logging
from PyQt5.QtCore import QObject, pyqtSignal
import ClientTools
import zlib
import serial
import base64

scanner = None
_LOG_ = logging.getLogger()
__author__ = 'wahyudi@popbox.asia'


class ScannerSignalHandler(QObject):
    barcode_result = pyqtSignal(str)


_SCANNER_ = ScannerSignalHandler()


def get_scanner():
    global scanner
    if scanner is None:
        init_scanner()
    return scanner


def init_scanner():
    global scanner
    try:
        scanner = serial.Serial(**ClientTools.get_port_value('Scanner', 115200, 'COM3', 30))
    except Exception as e:
        _LOG_.debug(('init_scanner ERROR :', e))


def start_stop_scanner():
    global start
    try:
        if start:
            start = False
        get_scanner().write((255, 85, 13))
        get_scanner().flushInput()
    except Exception as e:
        _LOG_.debug(('start_stop_scanner ERROR :', e))
 

start = False


def start_get_text_info(zip_flag=False):
    global start
    if not start:
        start = True
        ClientTools.get_global_pool().apply_async(get_text_info, (zip_flag,))


def get_text_info(zip_flag):
    global start
    try:
        get_scanner().flushInput()
        get_scanner().write((255, 84, 13))
        scanner_result = get_scanner().readline()
        if scanner_result == b'':
            return
        _LOG_.debug(('pre scanner_result is :', scanner_result))
        # _LOG_.debug(('zip_flag is : ', zip_flag))
        if zip_flag:
            scanner_result = base64.b64decode(scanner_result)
            scanner_result = zlib.decompress(scanner_result, 16 + zlib.MAX_WBITS)
        result = str(scanner_result, encoding='utf-8')
        result = result.replace(' ', '').strip().strip('\r\n').strip('\n')
        _SCANNER_.barcode_result.emit(result)
        _LOG_.debug(('scanner_result is :', result))
    except Exception as e:
        _LOG_.debug(('scanner get_text_info ERROR :', e))
        _SCANNER_.barcode_result.emit('ERROR')
    finally:
        start = False
