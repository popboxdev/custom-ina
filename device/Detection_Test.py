#!/usr/bin/env python

import sys
import numpy as np
import cv2
from glob import glob
import itertools as it
import matplotlib.pyplot as plt
from time import gmtime, strftime
from imutils.object_detection import non_max_suppression
from imutils import paths
import imutils

rstp = "rtsp://admin:popbox12345@192.168.5.88:554/Streaming/Channels/402"

try:
    cascPath = "face_weight.xml"
except Exception:
    cascPath = None

class HOG_Detection():
    def __init__(self, input_video, output, skip_every):
        if input_video is None:
            self.video_source = 0
        if input_video == 'rstp':
            self.video_source = rstp
        
        self.video_output = 'default.avi' if output is None else output+'.avi'
        self.skip_every = 5 if skip_every is None else 5

    def inside(self, r, q):
        rx, ry, rw, rh = r
        qx, qy, qw, qh = q
        return rx > qx and ry > qy and rx + rw < qx + qw and ry + rh < qy + qh

    def draw_detections(self, img, rects, color=(0, 255, 0), thickness=1):
        for x, y, w, h in rects:
            # the HOG detector returns slightly larger rectangles than the real objects.
            # so we slightly shrink the rectangles to get a nicer output.
            pad_w, pad_h = int(0.15*w), int(0.05*h)
            cv2.rectangle(img, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), color, thickness)

    def detect_video(self):
        hog = cv2.HOGDescriptor()
        hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector() )

        video_capture = cv2.VideoCapture(self.video_source)
        video_capture.set(6, 1)
        # fps=5
        # ret, frame = video_capture.read()
        # height, width, layers = frame.shape
        # fourcc = cv2.VideoWriter_fourcc(*'MJPG')
        # out = self.video_output
        # video = cv2.VideoWriter(out, fourcc, fps, (width, height), 1)
        # Skip frames count
        limit_modulus = 0
        count_skip = 0
        result = []

        while True:
            count_skip += 1
            limit_modulus += 1
            if count_skip < self.skip_every:
                continue
            count_skip = 0
            ret, frame = video_capture.read()
            if frame is None:
                break
            frame = imutils.resize(frame, width=min(400, frame.shape[1]))
            faces = None
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            current_time = strftime("%Y-%m-%d_%H:%M:%S", gmtime())
            #face detection
            if cascPath:
                faceCascade = cv2.CascadeClassifier(cascPath)
                faces = faceCascade.detectMultiScale(
                    gray,
                    scaleFactor=1.1,
                    minNeighbors=5,
                    minSize=(30, 30)
                )
                for (x, y, w, h) in faces:
                    f_x = x
                    f_y = y
                    f_w = w
                    f_h = h
                #draw rec & text face
                if len(faces) > 0:
                    self.draw_detections(img=frame, rects=faces, color=(0, 255, 255), thickness=2)
                    cv2.putText(frame, 'face', (int(f_x)+10, int(f_y)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
                #handling result face
                if len(faces) > 0 and limit_modulus % (self.skip_every+2) == 0:
                    face_found = "face detected [" + str(len(faces)) + "] : " + current_time
                    # face_found = "face detected [" + str(len(faces)) + "] : " + "modulus:" + str(limit_modulus)
                    result.append(face_found)
                    print(face_found)
            #motion detection
            motion, weights = hog.detectMultiScale(frame, winStride=(8, 8), padding=(16, 16), scale=1.05)
            for (x , y, w, h) in motion:
                m_x = x
                m_y = y
                m_w = w
                m_h = h
            # motion = np.array([[x, y, x + w, y + h] for (x, y, w, h) in motion])
            # motion = non_max_suppression(motion, probs=None, overlapThresh=0.65)
            # motion_filtered = []
            # for ri, r in enumerate(motion):
            # 	for qi, q in enumerate(motion):
            # 		if ri != qi and self.inside(r, q):
            # 			break
            # 		else:
            # 			motion_filtered.append(r)
            # self.draw_detections(img=frame, rects=motion_filtered, color=(0, 255, 0), thickness=3)
            #draw rec & text motion
            #handling result motion
            if len(motion) > 0:
                self.draw_detections(img=frame, rects=motion, color=(0, 255, 0),  thickness=2)
                cv2.putText(frame, 'motion', (int(m_x)+10, int(m_y)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            # if len(motion) > 0 and limit_modulus % self.skip_every+2 == 0:
                motion_found = "motion detected [" + str(len(motion)) + "] : " + current_time
                # motion_found = "motion detected [" + str(len(motion)) + "] : " + " modulus:" + str(limit_modulus)
                result.append(motion_found)
                print(motion_found)
            
            cv2.imshow('Face Detector', frame)
            # if cv2.waitKey(1) & 0xFF == ord('q') or len(result) >= (self.skip_every * 10):
            if cv2.waitKey(1) & 0xFF == ord('q') or limit_modulus > 1000:
                del frame
                del faces
                del motion
                del gray
                break
        cv2.destroyAllWindows()
        video_capture.release()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="input video, leave emtpy to use cam")
    parser.add_argument("--output", help="where to save video")
    parser.add_argument("--skip", help="frames to skip")
    args = parser.parse_args()

    x = HOG_Detection(args.input, args.output, args.skip)
    x.detect_video()
	

