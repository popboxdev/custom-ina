import logging
import threading
from PyQt5.QtCore import QObject, pyqtSignal
import time
import Configurator
import ClientTools
import codecs
import serial
lock_machine_port = None
cabinet_count = 16
door_count = 24
lock = threading.Lock()
_LOG_ = logging.getLogger()
lock_version = Configurator.get_or_set_value('LockMachine', 'version', default='1.0')
lock_type = Configurator.get_or_set_value('LockMachine', 'lockType', default='3')
buffer_range_count = ''
if lock_version == '1':
    lock_version = '1.0'


class LockMachineHandler(QObject):
    open_door_result = pyqtSignal(str)


def start():
    global lock_machine_port
    global lock_version
    global cabinet_count
    global buffer_range_count
    if Configurator.get_value('LockMachine', 'cabinet_count'):
        cabinet_count = Configurator.get_value('LockMachine', 'cabinet_count')
    try:
        lock_machine_port = serial.Serial(**ClientTools.get_port_value('LockMachine', 38400, 'COM2'))
        if lock_version == '1.0':
            buffer_range_count = 8
        elif lock_version == '2.0':
            buffer_range_count = 5
        elif lock_version == '3.0':
            buffer_range_count = 7
    except Exception as e:
        _LOG_.warning(e)


def start_open_door(cabinet_id, door_id, default=True):
    ClientTools.get_global_pool().apply_async(open_door, (cabinet_id, door_id, default))


def open_door(cabinet_id, door_id, default=True):
    _start_time = ClientTools.now()
    _LOG_.info(("DATA_LOCKMAACHINE",cabinet_id, door_id))
    try:
        try:
            lock.acquire()
            time.sleep(0.5)
            if lock_version == '1.0':
                get_port().write((
                 cabinet_id - 1 + 160, 3, door_id, (cabinet_id - 1 + 160 + 3 + door_id ^ 255) % 256))
            elif lock_version == '2.0':
                get_port().write((
                 cabinet_id - 1 + 160, 4, door_id - 1, 1,
                 (cabinet_id - 1 + 160 + 4 + door_id - 1 + 1 ^ 255) % 256))
            else:
                if lock_version == '3.0':
                    crc = (cabinet_id - 1 + 160 + door_id - 1 + 1 + 0 + 0 + 0 ^ 255) % 256
                    get_port().write((cabinet_id - 1 + 160, door_id - 1, 1, 0, 0, 0, crc, 0))
                _LOG_.info((cabinet_id, door_id))
                time.sleep(1)
                while True:
                    if ClientTools.now() - _start_time >= 3000:
                        return False
                    if lock_version == '1.0':
                        cabinet_status = get_cabinet_status(cabinet_id)
                        if cabinet_status is None:
                            _LOG_.error('cabinet_status is None')
                            return False
                        if cabinet_status[int(door_id) - 1] == 1:
                            return True
                    elif lock_version == '2.0':
                        cabinet_status = get_cabinet_status(cabinet_id, door_id)
                        if cabinet_status is None:
                            _LOG_.error('cabinet_status is None')
                            return False
                        if cabinet_status == 1:
                            return True
                    else:
                        if lock_version == '3.0':
                            cabinet_status = get_cabinet_status(cabinet_id, door_id)
                            if cabinet_status is None:
                                _LOG_.error('cabinet_status is None')
                                return False
                            if cabinet_status == 1:
                                return True
                        time.sleep(0.3)

        except Exception as e:
            _LOG_.warning(e)
            close_port()
            return default

    finally:
        lock.release()


def start_turn_off_lights(cabinet_id, door_number):
    ClientTools.get_global_pool().apply_async(turn_off_lights, (cabinet_id, door_number))


def turn_off_lights(cabinet_id, door_number):
    try:
        try:
            lock.acquire()
            time.sleep(0.1)
            if lock_version == '2.0':
                get_port().write((
                 cabinet_id - 1 + 160, 4, door_number - 1, 0,
                 (cabinet_id - 1 + 160 + 4 + door_number - 1 + 0 ^ 255) % 256))
                _LOG_.info(('turn_off_lights :', cabinet_id, door_number))
        except Exception as e:
            _LOG_.warning(e)
            close_port()

    finally:
        lock.release()


def start_turn_on_lights(cabinet_id, door_number):
    ClientTools.get_global_pool().apply_async(turn_on_lights, (cabinet_id, door_number))


def turn_on_lights(cabinet_id, door_number):
    try:
        try:
            lock.acquire()
            time.sleep(0.1)
            if lock_version == '2.0':
                get_port().write((
                 cabinet_id - 1 + 160, 4, door_number - 1, 14,
                 (cabinet_id - 1 + 160 + 4 + door_number - 1 + 14 ^ 255) % 256))
                _LOG_.info(('turn_on_lights :', cabinet_id, door_number))
        except Exception as e:
            _LOG_.warning(e)
            close_port()

    finally:
        lock.release()


def start_get_cabinet_status(cabinet_id):
    ClientTools.get_global_pool().apply_async(get_cabinet_status, (cabinet_id,))


def get_cabinet_status(cabinet_id, door_id=None):
    for x in range(0, 3):
        try:
            if lock_version == '1.0':
                get_port().write((
                 160 + cabinet_id - 1, 3, 170, (160 + cabinet_id - 1 + 3 + 170 ^ 255) % 256))
                buffer = list()
                for i in range(20):
                    receive_data = get_port().read(1)
                    if receive_data == b'':
                        break
                    buffer.append(receive_data)
                    if len(buffer) > 9:
                        buffer.pop(0)
                    if len(buffer) == 9 and check_data(buffer):
                        _LOG_.info(buffer)
                        __receive_data = buffer_com(buffer)
                        return convert_lock_status(__receive_data)

            if lock_version == '2.0':
                get_port().write((cabinet_id - 1 + 160, 4, door_id - 1, 16, (cabinet_id - 1 + 160 + 4 + door_id - 1 + 16 ^ 255) % 256))
                buffer = list()
                for i in range(20):
                    receive_data = get_port().read(1)
                    if receive_data == b'':
                        break
                    buffer.append(receive_data)
                    if len(buffer) > 5:
                        buffer.pop(0)
                    if len(buffer) == 5 and check_data(buffer):
                        _LOG_.info(buffer)
                        __receive_data = buffer_com(buffer)
                        return convert_lock_status(__receive_data)

            if lock_version == '3.0':
                crc = (cabinet_id - 1 + 160 + door_id - 1 + 0 + 1 + 0 + 0 ^ 255) % 256
                get_port().write((cabinet_id - 1 + 160, door_id - 1, 0, 1, 0, 0, crc, 0))
                buffer = list()
                for i in range(20):
                    receive_data = get_port().read(1)
                    if receive_data == b'':
                        break
                    buffer.append(receive_data)
                    if len(buffer) > buffer_range_count:
                        buffer.pop(0)
                    if len(buffer) == buffer_range_count and check_data(buffer):
                        _LOG_.info(buffer)
                        __receive_data = buffer_com(buffer)
                        return convert_lock_status(__receive_data)

        except (IOError, serial.SerialTimeoutException):
            close_port()


def start_detected_items(cabinet_id, door_number):
    ClientTools.get_global_pool().apply_async(get_items_status, (cabinet_id, door_number))


def get_items_status(cabinet_id, door_number, default=None):
    for x in range(0, 3):
        try:
            try:
                lock.acquire()
                time.sleep(0.1)
                if lock_version == '2.0':
                    get_port().write((
                     cabinet_id - 1 + 160, 4, door_number - 1, 16,
                     (cabinet_id - 1 + 160 + 4 + door_number - 1 + 16 ^ 255) % 256))
                    buffer = list()
                    for i in range(20):
                        receive_data = get_port().read(1)
                        if receive_data == b'':
                            break
                        buffer.append(receive_data)
                        if len(buffer) > 5:
                            buffer.pop(0)
                        if len(buffer) == 5 and check_data(buffer):
                            _LOG_.info(buffer)
                            __receive_data = buffer_com(buffer)
                            return convert_item_status(__receive_data)

            except (IOError, serial.SerialTimeoutException):
                close_port()

        finally:
            lock.release()

    return default


def start_get_box_item_status(cabinet_id, door_number):
    ClientTools.get_global_pool().apply_async(get_box_item_status, (cabinet_id, door_number))


def get_box_item_status(cabinet_id, door_number):
    for x in range(0, 3):
        try:
            time.sleep(0.1)
            if lock_version == '2.0':
                get_port().flushInput()
                get_port().write((
                 cabinet_id - 1 + 160, 4, door_number - 1, 16,
                 (cabinet_id - 1 + 160 + 4 + door_number - 1 + 16 ^ 255) % 256))
                buffer = list()
                for i in range(20):
                    receive_data = get_port().read(1)
                    if receive_data == b'':
                        break
                    buffer.append(receive_data)
                    if len(buffer) > 5:
                        buffer.pop(0)
                    if len(buffer) == 5 and check_data(buffer):
                        _LOG_.info(buffer)
                        __receive_data = buffer_com(buffer)
                        return convert_item_status(__receive_data), convert_lock_status(__receive_data)

        except (IOError, serial.SerialTimeoutException):
            close_port()

    return None, None


def buffer_com(data):
    com = b''
    for i in range(0, buffer_range_count):
        com += data[i]
    return com


def close_port():
    global lock_machine_port
    try:
        lock_machine_port.close()
    finally:
        lock_machine_port = None


def check_data(data):
    result = 0
    __check_data = buffer_com(data)
    if lock_version == '1.0':
        for i in range(0, 8):
            result += __check_data[i]

        _LOG_.debug(('check_data data is :', result))
        result ^= 255
        _LOG_.debug(('check_data result ^= :', result))
        return result & 255 == int(codecs.encode(data[8], 'hex'), 16)
    if lock_version == '2.0':
        for i in range(0, 4):
            result += __check_data[i]

        _LOG_.debug(('check_data data is :', result))
        result ^= 255
        _LOG_.debug(('check_data result ^= :', result))
        return result & 255 == __check_data[4]
    if lock_version == '3.0':
        __check_data = buffer_com(data)
        for i in range(0, buffer_range_count - 1):
            result += __check_data[i]

        _LOG_.debug(('check_data data is :', result))
        result ^= 255
        _LOG_.debug(('check_data result ^= :', result))
        return result & 255 == int(codecs.encode(data[buffer_range_count - 1], 'hex'), 16)
    return False


def convert_item_status(receive_data):
    if lock_version == '2.0':
        all_lock_status = receive_data[3]
        _LOG_.debug(('convert_item_status all_lock_status is :', all_lock_status))
        if all_lock_status & 32 != 0:
            return 'ERROR'
        return all_lock_status & 4 != 0


def convert_lock_status(receive_data):
    global lock_type
    if lock_version == '1.0':
        all_lock_status = (receive_data[4] * 256 + receive_data[3]) * 256 + receive_data[2]
        _LOG_.debug(('convert_lock_status all_lock_status is :', all_lock_status))
        return ClientTools.get_global_pool().map(lambda x: all_lock_status & 1 << x == 0, range(0, door_count))
    if lock_version == '2.0':
        all_lock_status = receive_data[3]
        _LOG_.debug(('convert_lock_status all_lock_status is :', all_lock_status))
        if all_lock_status & 32 != 0:
            return 'ERROR'
        if lock_type == '3':
            return all_lock_status & 1 == 0
        if lock_type == '4':
            return all_lock_status & 2 == 0
    if lock_version == '3.0':
        all_lock_status = receive_data[2]
        _LOG_.debug(('convert_lock_status all_lock_status is :', all_lock_status))
        if all_lock_status & 32 != 0:
            return 'ERROR'
        if lock_type == '3':
            return all_lock_status & 1 == 0
        if lock_type == '4':
            return all_lock_status & 2 == 0


def get_port():
    if lock_machine_port is None:
        start()
    return lock_machine_port
