import sys
import configparser
import os
import shutil
import logging
file_config = 'config.conf'
# FILE_NAME = sys.path[0] + '/config.conf'
FILE_NAME = sys.path[0] + '/config.conf'
FILE_NAMEBAK = sys.path[0] + '/config.bak'
__author__ = 'wahyudi@popbox.asia'
conf = None


def init():
    global conf
    conf = configparser.ConfigParser()
    conf.read(FILE_NAME)


def get_value(section, option):
    if conf is None:
        init()
    try:
        return conf.get(section, option)
    except configparser.NoOptionError:
        return None
    except configparser.NoSectionError:
        return None


def get_or_set_value(section, option, default=None):
    if conf is None:
        init()
    try:
        return conf.get(section, option)
    except configparser.NoOptionError:
        set_value(section, option, default)
        return conf.get(section, option)
    except configparser.NoSectionError:
        set_value(section, option, default)
        return conf.get(section, option)


def set_value(section, option, value):
    if conf is None:
        init()
    if section not in conf.sections():
        add_section(section)
    conf.set(section, option, value)
    save_file()
    return value


def add_section(section):
    conf.add_section(section)


def save_file():
    conf.write(open(FILE_NAME, 'w'))


def check_file():
    try:
        if os.path.isfile(FILE_NAME):
            if os.path.getsize(FILE_NAME) > 100:
                shutil.copy(FILE_NAME, FILE_NAMEBAK)
            else:
                shutil.copy(FILE_NAMEBAK, FILE_NAME)
        else:
            shutil.copy(FILE_NAMEBAK, file_config)
    except Exception as e:
        logging.warning("error checkfile Configurator", e)

check_file()
