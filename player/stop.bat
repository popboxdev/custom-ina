@echo off

:: BatchGotAdmin (Run as Admin code starts)

REM --> Check for permissions

>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin.

if '%errorlevel%' NEQ '0' (

echo Requesting administrative privileges to kill TVC Player...

goto UACPrompt

) else ( goto gotAdmin )

:UACPrompt

echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"

echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"

"%temp%\getadmin.vbs"

exit /B

:gotAdmin

if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )

pushd "%CD%"

CD /D "%~dp0"

:: BatchGotAdmin (Run as Admin code ends)
:: Codes below to kill Video Screensaver processes
taskkill /f /im dgservice.exe /im PopBox_VideoScreen.SCR /im POPBOX~1.SCR /im VideoScreenSaver.* /im VideoScreenSaver.exe /im VIDEOS~1.exe /im PopBoxTVCPlayer.exe /im PopBoxTVCPlayer.SCR /im POPBOX~1.exe /im PopBoxTVCPlayer15.scr /im PopBoxTVCPlayer.SCR /im PopBoxTVCPlayer.scr

